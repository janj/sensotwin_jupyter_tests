# SensoTwin Wind Turbine Lifetime Extrapolation

The following software package was developed by the Project [SensoTwin](https://material-digital.de/project/5) and contains both the UI and python package versions of the remaining lifetime extrapolation of wind turbine installations.

Workflow execution is based on the workflow framework [pyiron](https://pyiron.org) developed by the initiative [Plattform Material Digital](https://www.materialdigital.de/)

Visualisation is based on [JupyterLab](https://jupyter.org) and [PyVista](https://pyvista.org) and [Matplotlib](https://matplotlib.org/) for 3D/2D data respectively.

FEM simulation is achieved via [Calculix](https://www.calculix.de/).

### Installation

The system was developed and tested on Ubuntu 22.04, but contains no platform specific functionality. The used

1. Install Conda/Mamba package manager (Mamba preferred)
2. Clone Repository
3. Create execution environment via install/env.yml

    ```mamba env create -f install/env.yml```

### Execution

1. Activate execution environment

    ```mamba activate sensotwin```

2. Start JupyterLab

    ```jupyter lab```

3. Follow instructions at the top of the 5 numbered notebooks

### Notebook summary
1. Definition of material curing behaviour
2. Definition of component manufactoring defects
3. Configuration of operational parameters
4. Execution of simulation and visualization of results
5. Pure python example workflow with no UI for automation purposes
