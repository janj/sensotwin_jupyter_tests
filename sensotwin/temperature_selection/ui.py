import ipywidgets as widgets
from . import configuration
from . import style
from .. import global_style
from .. import store_connection
import matplotlib.pyplot as plt
import numpy as np

class TemperatureSelectionUIElements:
    """Contains all UI functionality of material curing process configuration UI (Step 1).

    In order to use, show the top level widget 'self.dashboard' in one cell of a notebook
    and the apply css styling calling 'self.apply_styling()' an another cell
    """
    def __init__(self):
        """Initialize UI objects and associated UI functionality."""
        self.dashboard = widgets.Tab().add_class("global_tab_container")
        self.dashboard.children = [
            self.init_data_source_box(),
            self.init_graph_input_box()]
        tab_titles = ['Input', 'Simulation']
        for i in range(len(tab_titles)):
            self.dashboard.set_title(i, tab_titles[i])

    def init_data_source_box(self) -> widgets.Box:
        """Initialize first tab of dashboard, choosing data source."""
        local_database_label = widgets.Label(value="Use local owlready2 database:").add_class("global_headline")
        use_local_data_button = widgets.Button(
            description='Use local database',
            disabled=False,
            tooltip='Use local database',
            icon='play',
        ).add_class("global_load_data_button").add_class("global_basic_button")
        use_local_data_button.on_click(self.load_local_data)
        
        remote_database_label = widgets.Label(value="Use remote Apache Jena Fuseki database:").add_class("global_headline")
        self.remote_database_input = widgets.Text(
            value=None,
            placeholder="insert SPARQL url here",
            description="SPARQL Endpoint:",
            disabled=False   
        ).add_class("global_url_input").add_class("global_basic_input")
        use_remote_data_button = widgets.Button(
            description='Use remote database',
            disabled=False,
            tooltip='Use remote database',
            icon='play',
        ).add_class("global_load_data_button").add_class("global_basic_button")
        use_remote_data_button.on_click(self.load_remote_data)
        
        remote_data_box = widgets.VBox([
            remote_database_label,
            self.remote_database_input,
            use_remote_data_button
        ])
        local_data_box = widgets.VBox([
            local_database_label,
            use_local_data_button
        ])
        data_source_box = widgets.VBox([
            local_data_box,
            remote_data_box
        ]).add_class("global_data_tab_container")

        return data_source_box

    def init_graph_input_box(self) -> widgets.Box:
        """Initialize second tab of dashboard, configuring material curing behaviour."""
        with plt.ioff():
            fig, self.ax = plt.subplots()
        fig.canvas.toolbar_visible = False
        fig.canvas.header_visible = False
        fig.canvas.footer_visible = False
        fig.canvas.resizable = False
        fig.set_facecolor((0.0, 0.0, 0.0, 0.0))
        self.ax.set_facecolor((0.0, 0.0, 0.0, 0.0))

        choose_timeline_label = widgets.Label(value="Choose temperature curve:").add_class("temp_sel_timeline_input_headline").add_class("global_headline")
        def on_selection_change(change):
            new_data_set = self.graph_data[change['new']]
            self.update_plot(new_data_set.get_time_data(),new_data_set.get_temperature_data())
        self.curve_selection = widgets.Dropdown(
            options=['Load data first'],
            value='Load data first',
            description='Name:',
            disabled=False
        ).add_class("temp_sel_basic_input").add_class("global_basic_input")
        self.curve_selection.observe(on_selection_change, names="value")

        fibre_volume_content_label = widgets.Label(value="Choose FVC:").add_class("temp_sel_timeline_input_headline").add_class("global_headline")
        self.fibre_volume_content_input = widgets.Dropdown(
            options=['Load data first'],
            value='Load data first',
            description='Fibre Volume Content (%):',
            disabled=False
        ).add_class("temp_sel_basic_input").add_class("global_basic_input")

        fvc_creation_label = widgets.Label(value="Create new fibre volume content:").add_class("temp_sel_timeline_input_headline").add_class("global_headline")
        self.creation_fvc_input = widgets.FloatText(
            value=0,
            disabled=False,
            description="Create new FVC entry (%)"
        ).add_class("temp_sel_basic_input").add_class("global_basic_input")
        save_fvc_button = widgets.Button(
            description='Save FVC',
            disabled=False,
            tooltip='Save FVC',
            icon='save',
        ).add_class("temp_sel_curve_creation_button").add_class("global_basic_button")
        save_fvc_button.on_click(self.save_new_fibre_volume_content)

        creation_label = widgets.Label(value="Create new temperature curve:").add_class("temp_sel_timeline_input_headline").add_class("global_headline")
        self.creation_time_input = widgets.Text(
            value="",
            placeholder="comma seperated values",
            description="Time values (h):",
            disabled=False   
        ).add_class("temp_sel_timeline_input").add_class("global_basic_input")
        self.creation_temperature_input = widgets.Text(
            value="",
            placeholder="comma seperated values",
            description="Temperature values (°C):",
            disabled=False   
        ).add_class("temp_sel_timeline_input").add_class("global_basic_input")
        self.creation_name_input = widgets.Text(
            value="",
            placeholder="enter name",
            description="Name:",
            disabled=False   
        ).add_class("temp_sel_timeline_input").add_class("global_basic_input")
        plot_curve_button = widgets.Button(
            description='Plot Curve',
            disabled=False,
            tooltip='Plot Curve',
            icon='play'
        ).add_class("temp_sel_curve_creation_button").add_class("global_basic_button")
        plot_curve_button.on_click(self.preview_new_timeline)
        save_curve_button = widgets.Button(
            description='Save Curve',
            disabled=False,
            tooltip='Save Curve',
            icon='save',
        ).add_class("temp_sel_curve_creation_button").add_class("global_basic_button")
        save_curve_button.on_click(self.save_new_timeline)

        input_set_selection_label = widgets.Label(value="Select Input Set:").add_class("global_headline")
        self.input_set_selection = widgets.Select(
            options=['Load data first'],
            value='Load data first',
            rows=10,
            description='Input sets:',
            disabled=False
        ).add_class("global_input_set_selection").add_class("global_basic_input")
        display_input_set_button = widgets.Button(
            description='Load Input Set',
            disabled=False,
            tooltip='Load Input Set',
            icon='play',
        ).add_class("global_basic_button")
        display_input_set_button.on_click(self.display_input_set)
        input_set_box = widgets.VBox([
            input_set_selection_label,
            self.input_set_selection,
            display_input_set_button
        ])

        save_input_set_button = widgets.Button(
            description='Save selected temperature graph and fibre volume content as new input set',
            disabled=False,
            tooltip='Save selected temperature graph and fibre volume content as new input set',
            icon='save',
        ).add_class("global_save_input_set_button").add_class("global_basic_button")
        save_input_set_button.on_click(self.save_new_input_set)
        curve_creation_button_box = widgets.HBox([
            plot_curve_button,
            save_curve_button
        ]).add_class("temp_sel_curve_creation_button_container")
        fvc_creation_button_box = widgets.HBox([
            save_fvc_button
        ]).add_class("temp_sel_curve_creation_button_container")
        temperature_selection_box = widgets.VBox([
            choose_timeline_label,
            self.curve_selection,
            creation_label,
            self.creation_time_input, 
            self.creation_temperature_input,
            self.creation_name_input,
            curve_creation_button_box
        ]).add_class("temp_sel_curve_selection_container")
        fvc_selection_box = widgets.VBox([
            fibre_volume_content_label,
            self.fibre_volume_content_input,
            fvc_creation_label,
            self.creation_fvc_input,
            fvc_creation_button_box
        ]).add_class("temp_sel_curve_creation_container")
        data_input_box = widgets.VBox([
            temperature_selection_box,
            fvc_selection_box
        ]).add_class("temp_sel_data_input_container")
        matplot_box = widgets.VBox([
            fig.canvas
        ]).add_class("temp_sel_matplot_box")
        
        graph_input_box = widgets.VBox([
            widgets.HBox([
                input_set_box,
                matplot_box,
                data_input_box
            ]).add_class("temp_sel_graph_tab_container"),
            save_input_set_button
        ])
        
        return graph_input_box

    def update_plot(self, time_data: list, temperature_data: list):
        """Update central plot with new data.

        Args:
            time_data: list of floats (hour)
            temperature_data: list of floats (°C)
        """
        self.ax.clear()
        self.ax.plot(np.array(time_data), np.array(temperature_data), label='_Temp?')
        self.ax.set_xlabel('Time (h)')
        self.ax.set_ylabel('Temperature (°C)')

    def load_local_data(self, ui_element=None):
        """Use locally stored data for UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.conn = store_connection.LocalStoreConnection("sensotwin_world")
        self.load_temperature_data()
        self.load_fvc_data()
        self.load_input_set_data()

    def load_remote_data(self, ui_element=None):
        """Use remotely stored data for UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.conn = store_connection.FusekiConnection(self.remote_database_input.value)
        self.load_temperature_data()
        self.load_fvc_data()
        self.load_input_set_data()

    def load_input_set_data(self):
        """Load data from previously set input source and populate UI."""
        self.input_set_data = configuration.MaterialHardeningInputDataSet.get_all_entries_from_store(self.conn)
        options = []
        for key, input_set in self.input_set_data.items():
            temperature_graph = self.graph_data[input_set.get_temperature_graph_id()]
            fibre_volume_content = self.fvc_data[input_set.get_fibre_volume_content_id()]
            label = input_set.generate_input_set_display_label()
            options.append((label, key))
        self.input_set_selection.options = options
        self.input_set_selection.value = list(self.input_set_data.keys())[0]

    def display_input_set(self, ui_element=None):
        """Display data for currently selected input set in UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        selected_set = self.input_set_data[self.input_set_selection.value]
        temperature_graph = self.graph_data[selected_set.get_temperature_graph_id()]
        fibre_volume_content = self.fvc_data[selected_set.get_fibre_volume_content_id()]
        self.load_temperature_data(pre_select=temperature_graph.get_id())
        self.load_fvc_data(pre_select=fibre_volume_content.get_id())

    def load_temperature_data(self, pre_select=None):
        """Refresh list for available temperature graphs.

        Args:
            pre_select: optional int id of a temperature set to be pre-selected after refresh
        """
        self.graph_data = configuration.TemperatureGraph.get_all_entries_from_store(self.conn)
        if pre_select:
            initial_dict_key = pre_select
        else:
            initial_dict_key = list(self.graph_data.keys())[0]
        time_data = self.graph_data[initial_dict_key].get_time_data()
        temperature_data = self.graph_data[initial_dict_key].get_temperature_data()
        self.curve_selection.options = [(value.get_name(), key) for key, value in self.graph_data.items()]
        self.curve_selection.value = initial_dict_key
        self.update_plot(time_data, temperature_data)

    def load_fvc_data(self, pre_select=None):
        """Refresh list for available fibre volume content values.

        Args:
            pre_select: optional int id of a fibre volume content to be pre-selected after refresh
        """
        self.fvc_data = configuration.FibreVolumeContent.get_all_entries_from_store(self.conn)
        if pre_select:
            initial_dict_key = pre_select
        else:
            initial_dict_key = list(self.fvc_data.keys())[0]
        self.fibre_volume_content_input.options = [(value.get_value(), key) for key, value in self.fvc_data.items()]
        self.fibre_volume_content_input.value = initial_dict_key

    def preview_new_timeline(self, ui_element=None):
        """Display new timeline currently defined in input fields without saving to store.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.update_plot(self.get_creation_time_values(), self.get_creation_temperature_values())

    def get_creation_time_values(self) -> list:
        """Parse current input of time value input."""
        time_string = self.creation_time_input.value
        time_values = list(map(float, time_string.split(",")))
        return time_values

    def get_creation_temperature_values(self) -> list:
        """Parse current input of temperature value input."""
        temperature_string = self.creation_temperature_input.value
        temperature_values = list(map(int, temperature_string.split(",")))
        return temperature_values

    def save_new_fibre_volume_content(self, ui_element=None):
        """Save currently defined value of fibre volume content to store as new entry.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        new_id = max(self.fvc_data.keys()) + 1
        new_fvc = configuration.FibreVolumeContent(
            uniq_id = new_id,
            value = self.creation_fvc_input.value,
            conn = self.conn)
        new_fvc.save_entry_to_store()
        self.load_fvc_data(pre_select=new_id)
    
    def save_new_timeline(self, ui_element=None):
        """Save currently defined values of temperature graph to store as new entry.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        new_id = max(self.graph_data.keys()) + 1
        new_graph = configuration.TemperatureGraph(
            uniq_id = new_id,
            name = self.creation_name_input.value,
            time_data = self.get_creation_time_values(),
            temperature_data = self.get_creation_temperature_values(),
            conn = self.conn)
        new_graph.save_entry_to_store()
        self.load_temperature_data(pre_select=new_id)

    def save_new_input_set(self, ui_element=None):
        """Save currently defined selections of temperature graph and fibre volume content to store as new entry.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        new_id = max(self.input_set_data.keys()) + 1
        new_set = configuration.MaterialHardeningInputDataSet(
            uniq_id = new_id,
            temperature_graph = self.graph_data[self.curve_selection.value],
            fibre_volume_content = self.fvc_data[self.fibre_volume_content_input.value],
            conn = self.conn)
        self.graph_data[self.curve_selection.value].save_entry_to_store()
        self.fvc_data[self.fibre_volume_content_input.value].save_entry_to_store()
        new_set.save_entry_to_store()
        self.load_input_set_data()

    def apply_styling(self):
        """Apply CSS hack to notebook for better styling than native Jupyter Widget styles."""
        css = """
            <style>
            {}
            {}
            </style>
        """.format(global_style.global_css, style.local_css)
        return widgets.HTML(css)