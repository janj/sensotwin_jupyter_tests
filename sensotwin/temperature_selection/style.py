

local_css = """
.temp_sel_graph_tab_container {
    display: flex !important;
    flex-direction: row !important;
    align-items: center !important;
    justify-content: space-around !important;
    width: 100%;
}
.temp_sel_matplot_box {
    width: 700px !important;
}
.temp_sel_data_input_container {
    width: auto !important;
}
.temp_sel_basic_input { 
    width: 500px !important;
}
.temp_sel_basic_input > input {
    
}
.temp_sel_basic_input > label {
    width: 200px !important;
}
.temp_sel_curve_selection_container {
    
}
.temp_sel_curve_creation_container {
    margin-top: 50px !important;
}
.temp_sel_timeline_input {
    width: 500px !important;
}
.temp_sel_timeline_input > label {
    width: 200px !important;
}
.temp_sel_curve_creation_button_container {
    display: flex !important;
    flex-direction: column !important;
    align-items: center !important;
    justify-content: flex-end !important;
    width: 100% !important;
}
.temp_sel_curve_creation_button {
    width: 300px !important;
}
"""