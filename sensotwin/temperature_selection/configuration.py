from .. import read_vtt, write_vtt
from ..store_connection import LocalStoreConnection, FusekiConnection, StoreConnection
from pyiron_base import HasHDF
import pandas as pd

class TemperatureGraph:
    """Describes temperature behaviour of material curing process."""
    def __init__(self, uniq_id: int=None, name: str=None, time_data: list=None, temperature_data: list=None, conn: StoreConnection=None):
        """Initialize object.

        Args:
            uniq_id: id unique to the chosen store for this object class
            name: non-unique name of object
            time_data: list of float x values for temperature graph (hours)
            temperature_data: list of float y values for temperature graph (°C)
            conn: Any subclass instance of StoreConnection
        """
        self._id = uniq_id
        self._name = name
        self._time_data = time_data
        self._temperature_data = temperature_data
        self._store_connection = conn

    def get_id(self) -> int:
        """Return id of object."""
        return self._id
    
    def get_name(self) -> str:
        """Return name of object."""
        return self._name
    
    def get_time_data(self) -> list:
        """Return time values of object."""
        return self._time_data

    def get_temperature_data(self) -> list:
        """Return temperature values of object."""
        return self._temperature_data

    @staticmethod
    def get_id_from_ontology_iri(label: str) -> int:
        """Extract id from full ontology instance iri."""
        return int(label.split("_")[-1])

    def generate_ontology_label(self) -> str:
        """Generate ontology instance iri from object."""
        return "__TemperatureGraph_{}".format(self._id)

    def save_entry_to_store(self):
        """Save object to store connection passed during initialization."""
        label = self.generate_ontology_label()
        query = """
            INSERT {{
                senso:__TimeData_{label} a senso:TimeData ;
                    co:value "{time}" .
                senso:__TemperatureData_{label} a senso:TemperatureData ;
                    co:value "{temp}" .
                senso:{label} a senso:TemperatureGraph ;
                    rdfs:label "{name}" ;
                    co:composedOf senso:__TimeData_{label} ;
                    co:composedOf senso:__TemperatureData_{label} .
            }} WHERE {{
                FILTER NOT EXISTS {{
                    [] rdfs:label "{name}" ;
                }}
            }}
            """.format(label=label, name=self._name, uniq_id=self._id, time=",".join(map(str,self._time_data)), temp=",".join(map(str,self._temperature_data)))
        self._store_connection.update_data(query)

    @staticmethod
    def get_all_entries_from_store(conn: StoreConnection) -> dict:
        """Get all instances of TemperatureGraph from store connection passed during initialization.

        Args:
            conn: Any subclass instance of StoreConnection
        Returns:
            dict of int -> TemperatureGraph
        """
        query = """
            SELECT ?x ?name ?time_value ?temp_value
            WHERE {
                ?x a senso:TemperatureGraph .
                ?x rdfs:label ?name .
                {
                    ?x co:composedOf ?time_component .
                    ?time_component rdf:type senso:TimeData .
                    ?time_component co:value ?time_value .
                }
                { 
                    ?x co:composedOf ?temp_component .
                    ?temp_component rdf:type senso:TemperatureData .
                    ?temp_component co:value ?temp_value .
                }
            }
            ORDER BY ?name
        """
        result = {}
        for x in conn.get_data(query):
            uniq_id = TemperatureGraph.get_id_from_ontology_iri(x[0])
            result[uniq_id] = TemperatureGraph(uniq_id=uniq_id, name=x[1], time_data=list(map(float, x[2].split(","))), 
                                            temperature_data=list(map(float, x[3].split(","))), conn=conn)
        return result

    def __str__(self):
        return "TemperatureGraph, ID: {}, Name: {}, Time (h): {}, Temperature (°C): {}, Source: {}".format(
            self._id, 
            self._name, 
            self._time_data, 
            self._temperature_data, 
            self._store_connection)

    def __repr__(self):
        return "TemperatureGraph(uniq_id={uniq_id}, name='{name}', time_data={time_data}, temperature_data={temperature_data}, conn={conn})".format(
            uniq_id = self._id, 
            name = self._name, 
            time_data = self._time_data, 
            temperature_data = self._temperature_data, 
            conn = repr(self._store_connection))


class FibreVolumeContent:
    """Describes average fibre volume content of material used for components."""
    def __init__(self, uniq_id: int=None, value: float=None, conn: StoreConnection=None):
        """Initialize object.

        Args:
            uniq_id: id unique to the chosen store for this object class.
            value: fibre volume content in %.
            conn: Any subclass instance of StoreConnection.
        """
        self._id = uniq_id
        self._value = value
        self._store_connection = conn

    def get_id(self) -> int:
        """Return id of object."""
        return self._id

    def get_value(self) -> float:
        """Return % value of object."""
        return self._value

    def generate_ontology_label(self) -> str:
        """Generate ontology instance iri from object."""
        return "__FibreVolumeContent_{}".format(self._id)

    @staticmethod
    def get_id_from_ontology_iri(label: str) -> int:
        """Extract id from full ontology instance iri."""
        return int(label.split("_")[-1])

    def save_entry_to_store(self):
        """Save object to store connection passed during initialization."""
        label = self.generate_ontology_label()
        query = """
            INSERT {{
                senso:{label} a senso:FibreVolumeContent ;
                    co:value {value} .
                }} WHERE {{
                    FILTER NOT EXISTS {{
                        [] co:value {value} ;
                    }}
            }}
            """.format(label=label, value=self._value)
        self._store_connection.update_data(query)

    @staticmethod
    def get_all_entries_from_store(conn: StoreConnection) -> dict:
        """Get all instances of FibreVolumeContent from store connection passed during initialization.

        Args:
            conn: Any subclass instance of StoreConnection
        Returns:
            dict of int -> FibreVolumeContent
        """
        query = """
            SELECT ?x ?value
            WHERE {
                ?x a senso:FibreVolumeContent .
                ?x co:value ?value .
            }
            ORDER BY ASC(?value)
        """
        result = {}
        for x in conn.get_data(query):
            uniq_id = FibreVolumeContent.get_id_from_ontology_iri(x[0])
            result[uniq_id] = FibreVolumeContent(uniq_id=uniq_id, value=float(x[1]), conn=conn)
        return result

    def __str__(self):
        return "FibreVolumeContent, ID: {}, Value (%): {}, Source: {}".format(self._id, self._value, self._store_connection)

    def __repr__(self):
        return "FibreVolumeContent(uniq_id={uniq_id}, value={value}, conn={conn})".format(
            uniq_id = self._id, 
            value = self._value, 
            conn = repr(self._store_connection))

class MaterialHardeningInputDataSet(HasHDF):
    """Input data set for future simulation containing material curing parameters.

    Contains temperature timeline for material curing process and average fibre volume
    content of used material; implements pyiron HasHDF interface for automatic HDF5
    serialization during job execution for storage
    """
    def __init__(self, uniq_id: int=None, temperature_graph: TemperatureGraph=None, fibre_volume_content: FibreVolumeContent=None, conn: StoreConnection=None):
        """Initialize object.
        
        Args:
            uniq_id: id unique to the chosen store for this object class
            temperature_graph: TemperatureGraph object
            fibre_volume_content: FibreVolumeContent object
            conn: Any subclass instance of StoreConnection
        """
        self._id = uniq_id
        self._temperature_graph = temperature_graph
        self._fibre_volume_content = fibre_volume_content
        self._store_connection = conn

    def get_id(self) -> int:
        """Return id of object."""
        return self._id

    def get_temperature_graph_id(self) -> int:
        """Return id of associated TemperateGraph object."""
        return self._temperature_graph.get_id()

    def get_fibre_volume_content_id(self) -> int:
        """Return id of associated FibreVolumeContent object."""
        return self._fibre_volume_content.get_id()

    def generate_input_set_display_label(self) -> str:
        """Return string display for object in list displays."""
        return "{}, {}%".format(self._temperature_graph.get_name(), self._fibre_volume_content.get_value())

    @staticmethod
    def get_id_from_ontology_iri(label: str)-> int:
        """Extract id from full ontology instance iri."""
        return int(label.split("_")[-1])

    def generate_ontology_label(self) -> str:
        """Generate ontology instance iri from object."""
        return "__CuringSimulationInputSet_{}".format(self._id)

    def save_entry_to_store(self):
        """Save object to store connection passed during initialization."""
        label = self.generate_ontology_label()
        temp_label = self._temperature_graph.generate_ontology_label()
        fvc_label = self._fibre_volume_content.generate_ontology_label()
        query = """
            INSERT {{
                senso:{label} a senso:CuringSimulationInputSet ;
                    co:composedOf       senso:{temp_label} ;
                    co:composedOf		senso:{fvc_label} .
            }} WHERE {{}}
            """.format(label=label, temp_label=temp_label, fvc_label=fvc_label)
        self._store_connection.update_data(query)

    @staticmethod
    def get_all_entries_from_store(conn: StoreConnection) -> dict:
        """Get all instances of MaterialHardeningInputSet from store connection passed during initialization.

        Args:
            conn: Any subclass instance of StoreConnection
        Returns:
            dict of int -> MaterialHardeningInputSet
        """
        temps = TemperatureGraph.get_all_entries_from_store(conn)
        fvcs = FibreVolumeContent.get_all_entries_from_store(conn)
        query = """
            SELECT ?x ?fvc_component ?temp_component
            WHERE {
                ?x a senso:CuringSimulationInputSet .
                {
                    ?x co:composedOf ?fvc_component .
                    ?fvc_component a senso:FibreVolumeContent .
                }
                {
                    ?x co:composedOf ?temp_component .
                    ?temp_component a senso:TemperatureGraph .
                }
            }
            """
        result = {}
        for x in conn.get_data(query):
            uniq_id = MaterialHardeningInputDataSet.get_id_from_ontology_iri(x[0])
            temp_id = TemperatureGraph.get_id_from_ontology_iri(x[2])
            fvc_id = FibreVolumeContent.get_id_from_ontology_iri(x[1])
            result[uniq_id] = MaterialHardeningInputDataSet(uniq_id=uniq_id, temperature_graph=temps[temp_id], fibre_volume_content=fvcs[fvc_id], conn=conn)
        return result

    def __str__(self):
        return "MaterialHardeningInputDataSet, ID: {}, Temperature ID: {}, FVC ID: {}, Source: {}".format(
            self._id, self._temperature_graph.get_id(), self._fibre_volume_content.get_id(), self._store_connection)

    def __repr__(self):
        return "MaterialHardeningInputDataSet(uniq_id={uniq_id}, temperature_graph={temperature_graph}, fibre_volume_content={fibre_volume_content}, conn={conn})".format(
            uniq_id = self._id, 
            temperature_graph = repr(self._temperature_graph),
            fibre_volume_content = repr(self._fibre_volume_content), 
            conn = repr(self._store_connection))

    def _to_hdf(self, hdf=None, group_name=None):
        with hdf.open("material_hardening_input_data") as hdf_store:
            hdf_store["_id"] = self._id
            hdf_store["_temperature_graph"] = repr(self._temperature_graph)
            hdf_store["_fibre_volume_content"] = repr(self._fibre_volume_content)
            hdf_store["_store_connection"] = repr(self._store_connection)

    def _from_hdf(self, hdf=None, version=None):
        pass

    def from_hdf_args(hdf):
        with hdf.open("material_hardening_input_data") as hdf_store:
            arg_dict = {
                "uniq_id": hdf_store["_id"], 
                "temperature_graph": eval(hdf_store["_temperature_graph"]),
                "fibre_volume_content": eval(hdf_store["_fibre_volume_content"]),
                "conn": eval(hdf_store["_store_connection"])
            }
        return arg_dict