from SPARQLWrapper import SPARQLWrapper, JSON, POST, GET
from abc import ABC, abstractmethod
from owlready2 import *
import os.path

PREDEFINED_ONTOLOGIES["https://w3id.org/pmd/co/2.0.7/"] = "https://materialdigital.github.io/core-ontology/ontology.rdf"

class StoreConnection(ABC):
    """Base class for store connections."""
    
    SPARQL_PREFIXES = [
        "xsd: <http://www.w3.org/2001/XMLSchema#>",
        "rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>",
        "owl: <http://www.w3.org/2002/07/owl#>",
        "rdfs: <http://www.w3.org/2000/01/rdf-schema#>",
        "senso: <http://w3id.org/sensotwin/applicationontology#>",
        "co: <https://w3id.org/pmd/co/>",
    ]
    
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def get_data(self):
        pass
        
    @abstractmethod
    def update_data(self):
        pass

    def _get_prefix_stub(self):
        """Generate PREFIX statement for all query from SPARQL_PREFIXES."""
        return "\n".join(["PREFIX " + prefix for prefix in self.SPARQL_PREFIXES])


class LocalStoreConnection(StoreConnection):
    """Connection for storing data on local hard drive."""

    _SUBFOLDER_NAME = "owlready_db"
    
    def __init__(self, world_name: str):
        """Establish connection to local sqlite3 file.

        Args:
            world_name: file name of sqlite3 without file ending
        """
        self._world_name = world_name
        if not os.path.isdir(self._SUBFOLDER_NAME):
            os.makedirs(self._SUBFOLDER_NAME)
        self._sensotwin_world = World()
        world_file = "{}/{}.sqlite3".format(self._SUBFOLDER_NAME, self._world_name)
        if os.path.isfile(world_file):
            self._sensotwin_world.set_backend(filename = world_file, exclusive = False)
            self._onto = self._sensotwin_world.get_ontology("http://w3id.org/sensotwin/applicationontology#").load()
        else:
            self._sensotwin_world.set_backend(filename = world_file, exclusive = False)
            #self._onto = self._sensotwin_world.get_ontology("https://upaehler.github.io/SensoTwin/v1.0.1/ontology.rdf").load()
            self._onto = self._sensotwin_world.get_ontology("http://127.0.0.1:1234/sensotwin_1.2.3.rdf").load()
        self._sensotwin_world.save()

    def get_data(self, query: str) -> list:
        """Execute SELECT query against local database file.

        Args:
            query: full SELECT statement without PREFIX statements
        
        Returns:
            list of retrieved data as specified in SELECT statement
        """
        result = self._sensotwin_world.sparql(self._get_prefix_stub() + query)
        self._sensotwin_world.save()
        parsed_result = []
        for row in result:
            new_entry = []
            for item in row:
                if hasattr(item, 'name'):
                    new_entry.append(item.name)
                else:
                    new_entry.append(str(item))
            parsed_result.append(new_entry)
        return parsed_result

    def update_data(self, query: str):
        """Execute INSERT query against local database file.

        Args:
            full INSERT statement without PREFIX statement
        """
        with self._onto:
            # error_on_undefined_entities=False is essential to enable multiple inserts in 1 query, if new objects depend on each other
            result = self._sensotwin_world.sparql(self._get_prefix_stub() + query, error_on_undefined_entities=False)
            self._sensotwin_world.save()
        return result

    def __str__(self):
        return "Local owlready2 database in '{}.sqlite3'".format(self._world_name)

    def __repr__(self):
        return "LocalStoreConnection('{}')".format(self._world_name)


class FusekiConnection(StoreConnection):
    """Connection for storing data in remote Apache Jena Fuseki2 triple store."""

    def __init__(self, url: str):
        """Initialize SPARQLWrapper object."""
        self._url = url
        self._wrapper = SPARQLWrapper(url)
        self._wrapper.setReturnFormat(JSON)

    def get_data(self, query: str):
        """Execute SELECT query against remote triple store.

        Args:
            query: full SELECT statement without PREFIX statements
        
        Returns:
            list of retrieved data as specified in SELECT statement
        """
        self._wrapper.setMethod(GET)
        self._wrapper.setQuery(self._get_prefix_stub() + query)
        raw_result = self._wrapper.queryAndConvert()["results"]["bindings"]
        parsed_result = []
        for row in raw_result:
            new_entry = []
            for item in row.values():
                if item['type'] == "uri":
                    new_entry.append(item["value"].split('#')[-1])
                else:
                    new_entry.append(item["value"])
            parsed_result.append(new_entry)
        return parsed_result

    def update_data(self, query: str) -> list:
        """Execute INSERT query against remote triple store.

        Args:
            full INSERT statement without PREFIX statement
        """
        self._wrapper.setMethod(POST)
        self._wrapper.setQuery(self._get_prefix_stub() + query)
        return self._wrapper.queryAndConvert()

    def __str__(self):
        return "Remote Jena Fuseki database at '{}'".format(self._url)

    def __repr__(self):
        return "FusekiConnection('{}')".format(self._url)