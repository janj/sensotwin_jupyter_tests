import copy
import math

_SUBSCRIPT = str.maketrans("0123456789", "₀₁₂₃₄₅₆₇₈₉")

def _print_repeating_series(x: dict) -> str:
    """Add subscript number to single element if applicable.

    Args:
        x: dict in the common analysis format as described in generate_composite_layup_description

    Returns:
        string description of repeating element
    """
    return "{}{}".format(_create_layup_description(x["val"]), str(int(x["count"])).translate(_SUBSCRIPT) if x["count"] > 1 else "")


def _print_symmetrical_series(x: dict) -> str:
    """Add symmetrical string identifier to single element.

    Args:
        x: dict in the common analysis format as described in generate_composite_layup_description

    Returns:
        string description of symmetrical element
    """
    return "{}{}".format(_create_layup_description(x["val"]), "s")


def _stringlist(list: list) -> str:
    """Generate string representation of number list without any separator"""
    return "".join(map(str, list))


def _check_if_number(x):
    """Check if passed argument can be cast to a number"""
    try:
        float(x)
        return True
    except (ValueError, TypeError):
        return False


def _search_for_symmetrical_series(data: list) -> list:
    """Search for any symmetrical occurrence of numbers.

    symmetrical search takes priority over other types of repetition and thus only has to handle dict objects
    unlike later steps in the process
    example of symmetrical numbers: [1, 2, 3, 4, 4, 3, 2, 1], the double middle value is necessary

    Args:
        data: list of dicts in the common analysis format as described in generate_composite_layup_description

    Returns:
        modified list of dicts with symmetrical dicts inserted
    """
    sequence = copy.deepcopy(data)
    current_pass_length = math.floor(len(sequence) / 2)
    while current_pass_length > 1:
        i = 0
        while i + 2 * current_pass_length <= len(sequence):
            first_slice = [x["val"] for x in sequence[i:i+current_pass_length]]
            second_slice = [x["val"] for x in sequence[i+current_pass_length:i+2*current_pass_length]]
            if first_slice == second_slice[::-1] and len(set(first_slice)) > 1:
                sequence[i]["val"] = _search_for_symmetrical_series(sequence[i:i+current_pass_length])
                sequence[i]["desc"] = "symmetrical"
                for _ in range(current_pass_length*2-1):
                    sequence.pop(i+1)
            i += 1
        current_pass_length -= 1
    return sequence


def _search_for_repeating_series(data: dict|list|int) -> dict|list|int:
    """Search for any repeating occurence of numbers.

    repeating series are a second level pattern and the search function thus has to correctly handle common
    analysis dicts, lists of common analysis dicts and raw numbers as input data
    example of repeating numbers: [1, 2, 3, 4, 1, 2, 3, 4]

    Args:
        data: data structure to recursively check for repeating angle sequences

    Returns:
        modified input data with series of repeating numbers minimised
    """
    sequence = copy.deepcopy(data)
    if isinstance(sequence, list):
        current_pass_length = math.floor(len(sequence) / 2)
        while current_pass_length > 0:
            i = 0
            while i + 2 * current_pass_length <= len(sequence):
                first_slice = [x["val"] for x in sequence[i:i+current_pass_length]]
                second_slice = [x["val"] for x in sequence[i+current_pass_length:i+2*current_pass_length]]
                if first_slice == second_slice:
                    # recursive algorithm starting from maximum length does not properly parse uneven number of repetitions
                    # extra check is performed in this case
                    uneven_correction = 0
                    if i + 3 * current_pass_length <= len(sequence):
                        third_slice = [x["val"] for x in sequence[i+2*current_pass_length:i+3*current_pass_length]]
                        if first_slice == third_slice:
                            uneven_correction = 1
                    sequence[i]["val"] = _search_for_repeating_series(sequence[i:i+current_pass_length])
                    # check for extra repeats of object in subobject and add them together with parent if necessary
                    # algorithm creates stacked repetitions of 2 when there is an even number of repetitions >= 4
                    additional_counts = 0
                    if isinstance(sequence[i]["val"], list) and len(sequence[i]["val"]) == 1 and sequence[i]["val"][0]["desc"] == "repeat":
                        additional_counts = sequence[i]["val"][0]["count"]
                        sequence[i]["val"] = copy.deepcopy(sequence[i]["val"][0]["val"])
                    sequence[i]["desc"] = "repeat"
                    sequence[i]["count"] += 1 + additional_counts + uneven_correction
                    for _ in range( (current_pass_length+uneven_correction) * 2 - 1):
                        sequence.pop(i+1)
                i += 1
            current_pass_length -= 1
        for i in range(len(sequence)):
            if (isinstance(sequence[i]["val"], list) or isinstance(sequence[i]["val"], dict)):
                sequence[i]["val"] = _search_for_repeating_series(sequence[i]["val"])
    elif isinstance(sequence, dict) and (isinstance(sequence["val"], list) or isinstance(sequence["val"], dict)):
        sequence["val"] = _search_for_repeating_series(sequence["val"])
    return sequence


def _search_for_inverted_series(data: dict|list|int) -> dict|list|int:
    """Search for any inverted occurence of numbers.

    inverted series are a second level pattern and the search function thus has to correctly handle common
    analysis dicts, lists of common analysis dicts and raw numbers as input data
    example of repeating numbers: [-1, 1] or [1, -1]

    Args:
        data: data structure to recursively check for inverted angle sequences

    Returns:
        modified input data with series of inverted numbers minimised
    """
    sequence = copy.deepcopy(data)
    if isinstance(sequence, list):
        current_position = 0
        while current_position < len(sequence):
            if current_position < len(sequence) - 1 and _check_if_number(sequence[current_position+1]["val"]) and sequence[current_position]["val"] == -sequence[current_position+1]["val"]:
                sequence[current_position]["val"] = "{}{}".format("±" if sequence[current_position]["val"] >= 0 else "∓", abs(sequence[current_position]["val"]))
                sequence.pop(current_position+1)
            elif isinstance(sequence[current_position]["val"], dict) or isinstance(sequence[current_position]["val"], list):
                sequence[current_position]["val"] = _search_for_inverted_series(sequence[current_position]["val"])
                current_position += 1
            else:
                current_position += 1
    elif isinstance(sequence, dict) and (isinstance(sequence["val"], list) or isinstance(sequence["val"], dict)):
        sequence["val"] = _search_for_inverted_series(sequence["val"])
    return sequence


def _reduction_chain(input):
    """Perform sequence reduction steps in specific order

    Args:
        input: list of dicts in the common analysis format as described in generate_composite_layup_description

    Returns:
        minimized sequence information
    """
    return \
        _search_for_repeating_series(
            _search_for_inverted_series(
                _search_for_repeating_series(
                    _search_for_symmetrical_series(
                        input
                    )
                )
            )
        )


def _reduce_sequence(sequence: dict) -> dict:
    """Perform reduction steps until no more minimisation is possible.

    Args:
        sequence: dict in the common analysis format as described in generate_composite_layup_description

    Returns:
        reduced input dict
    """
    old_sequence = copy.deepcopy(sequence)
    new_sequence = _reduction_chain(copy.deepcopy(sequence))
    while new_sequence != old_sequence:
        for i in range(len(new_sequence)):
            if isinstance(new_sequence[i]["val"],list):
                new_sequence[i] = _reduce_sequence(new_sequence[i])
        old_sequence = copy.deepcopy(new_sequence)
        new_sequence = _reduction_chain(copy.deepcopy(old_sequence))
    return copy.deepcopy(new_sequence)


def _create_layup_description(data: dict) -> str:
    """Create final string description from analysed series data.

    Args:
        data: stacked dicts in the common analysis format as described in generate_composite_layup_description

    Returns:
        string representation of analysed series data
    """
    result = "{}"
    if isinstance(data, list):
        if len(data) > 1:
            result = "[{}]"
        object_strings = []
        for i in range(len(data)):
            if data[i]["desc"] == "repeat":
                object_strings.append(_print_repeating_series(data[i]))
            elif data[i]["desc"] == "symmetrical":
                object_strings.append(_print_symmetrical_series(data[i]))
            else:
                object_strings.append(_create_layup_description(data[i]["val"]))
        object_string = "|".join(object_strings)
    elif isinstance(data, dict):
        if data["desc"] == "repeat":
            object_string = _print_repeating_series(data)
        elif data["desc"] == "symmetrical":
            object_string = _print_symmetrical_series(data)
        else:
            object_string = str(data["val"])
    else:
        object_string = data
    return result.format(object_string)


def generate_composite_layup_description(series: list) -> str:
    """Generate description string for given series of composite material alignment angles.

    the common analysis dict format of this algorithm:
    dict = {
        'desc': None|'repeat'|'symmetrical',
        'val': int|list(int)|dict,
        'count': int
    }

    Args:
        series: ordered list of integer angles of composite material layer alignments

    Returns:
        string representation of repeating angle pattern
    """
    total_string = _stringlist(series)
    total_repeats = 1
    repeating_sequence = None
    # find number of total repeats
    for i in range(1, len(series)+1):
        repeating_sequence = series[:i]
        current_search = _stringlist(series[:i])
        if len(total_string.replace(current_search, "")) == 0:
            total_repeats = len(total_string) / len(current_search)
            break
    # set top level object
    sequence_objects = [{
            "desc": None if total_repeats == 1 else "repeat",
            "val": [],
            "count": total_repeats
    }]
    for series in repeating_sequence:
        sequence_objects[0]["val"].append(
            {
                "desc": None,
                "val": series,
                "count": 1
            }
        )
    # recursively reduce repeating sequence
    sequence_objects[0]["val"] = _reduce_sequence(sequence_objects[0]["val"])
    return _create_layup_description(sequence_objects)