import ipywidgets as widgets
from . import configuration
from . import style
from .. import global_style
from .. import store_connection
import matplotlib.pyplot as plt
import matplotlib.transforms as trans
import matplotlib.patches as patches
import math
from traitlets.utils.bunch import Bunch


class StressSelectionUIElements:
    """Contains all UI functionality of operational parameter configuration UI (Step 3).

    In order to use, show the top level widget 'self.dashboard' in one cell of a notebook
    and the apply css styling calling 'self.apply_styling()' an another cell
    """
    WIND_PROFILE_FILE_PATH = "Inputs/wind_data.vtt"
    def __init__(self):
        """Initialize UI objects and associated UI functionality."""
        self.dashboard = widgets.Tab().add_class("global_tab_container")
        self.dashboard.children = [
            self.init_data_source_box(),
            self.init_graph_input_box()
        ]
        tab_titles = ['Input', 'Simulation']
        for i in range(len(tab_titles)):
            self.dashboard.set_title(i, tab_titles[i])

    def init_data_source_box(self) -> widgets.Box:
        """Initialize first tab of dashboard, choosing data source."""
        local_database_label = widgets.Label(value="Use local owlready2 database:").add_class("global_headline")
        use_local_data_button = widgets.Button(
            description='Use local database',
            disabled=False,
            tooltip='Use local database',
            icon='play',
        ).add_class("global_load_data_button").add_class("global_basic_button")
        use_local_data_button.on_click(self.load_local_data)
        
        remote_database_label = widgets.Label(value="Use remote Apache Jena Fuseki database:").add_class("global_headline")
        self.remote_database_input = widgets.Text(
            value=None,
            placeholder="insert SPARQL url here",
            description="SPARQL Endpoint:",
            disabled=False   
        ).add_class("global_url_input").add_class("global_basic_input")
        use_remote_data_button = widgets.Button(
            description='Use remote database',
            disabled=False,
            tooltip='Use remote database',
            icon='play',
        ).add_class("global_load_data_button").add_class("global_basic_button")
        use_remote_data_button.on_click(self.load_remote_data)
        
        local_data_box = widgets.VBox([
            local_database_label,
            use_local_data_button
        ])
        remote_data_box = widgets.VBox([
            remote_database_label,
            self.remote_database_input,
            use_remote_data_button
        ])
        data_source_box = widgets.VBox([
            local_data_box,
            remote_data_box
        ]).add_class("global_data_tab_container")

        return data_source_box

    def init_graph_input_box(self) -> widgets.Box:
        """Initialize second tab of dashboard, configuring operational parameters."""
        with plt.ioff():
            fig, (self.ax, self.pitch_ax) = plt.subplots(1, 2, width_ratios=[3, 1], figsize=(11.0, 6))
        fig.canvas.toolbar_visible = False
        fig.canvas.header_visible = False
        fig.canvas.footer_visible = False
        fig.canvas.resizable = False
        fig.set_facecolor((0.0, 0.0, 0.0, 0.0))
        self.ax.set_facecolor((0.0, 0.0, 0.0, 0.0))
        self.pitch_ax.set_facecolor((0.0, 0.0, 0.0, 0.0))

        manual_parameters_label = widgets.Label(value="Manual parameters:").add_class("global_headline")
        self.windspeed_slider = widgets.FloatSlider(
            description='Windspeed:',
            disabled=False,
            continuous_update=True,
            orientation='horizontal',
            readout=True,
            readout_format='.2f',
        ).add_class("global_basic_input").add_class("wind_sel_basic_input")
        self.windspeed_input = widgets.FloatText(
            description='Windspeed (m/s):',
            disabled=False,
        ).add_class("global_basic_input").add_class("wind_sel_basic_input")
        self.duration_input = widgets.FloatText(
            description='Duration (y):',
            disabled=False,
            value=10,
        ).add_class("global_basic_input").add_class("wind_sel_basic_input")
        self.windspeed_slider.observe(self.on_windspeed_slider_change, names="value")
        self.windspeed_input.observe(self.on_windspeed_input_change, names="value")
        self.duration_input.observe(self.on_duration_input_change, names="value")

        derived_parameters_label = widgets.Label(value="Derived parameters:").add_class("global_headline")
        self.pitch_angle_display = widgets.FloatText(
            value=None,
            description='Pitch angle (°):',
            disabled=True,
        ).add_class("global_basic_input").add_class("wind_sel_basic_input")
        self.rotation_speed_display = widgets.FloatText(
            value=None,
            description='Rotation speed (rpm):',
            disabled=True,
        ).add_class("global_basic_input").add_class("wind_sel_basic_input")
        self.rotation_total_display = widgets.IntText(
            value=None,
            description='Total rotations:',
            disabled=True,
        ).add_class("global_basic_input").add_class("wind_sel_basic_input")

        input_set_selection_label = widgets.Label(value="Select Input Set:").add_class("global_headline")
        self.input_set_selection = widgets.Select(
            options=['Load data first'],
            value='Load data first',
            rows=10,
            description='Input sets:',
            disabled=False
        ).add_class("global_input_set_selection").add_class("global_basic_input")
        display_input_set_button = widgets.Button(
            description='Load Input Set',
            disabled=False,
            tooltip='Load Input Set',
            icon='play',
        ).add_class("global_basic_button")
        display_input_set_button.on_click(self.display_input_set)

        selection_box = widgets.VBox([
            manual_parameters_label,
            self.windspeed_slider,
            self.windspeed_input,
            self.duration_input,
            derived_parameters_label, 
            self.pitch_angle_display,
            self.rotation_speed_display,
            self.rotation_total_display
        ]).add_class("wind_sel_selection_box")
        self.input_set_box = widgets.VBox([
            input_set_selection_label,
            self.input_set_selection,
            display_input_set_button,
            selection_box
        ])

        self.save_input_set_button = widgets.Button(
            description='Save selected wind speed and duration as new input set',
            disabled=False,
            tooltip='Save selected wind speed and duration as new input set',
            icon='save',
        ).add_class("global_save_input_set_button").add_class("global_basic_button")
        self.save_input_set_button.on_click(self.save_new_input_set)
        graph_input_box = widgets.VBox([
            widgets.HBox([
                self.input_set_box,
                fig.canvas
            ]).add_class("wind_sel_graph_tab_container"),
            self.save_input_set_button
        ])

        return graph_input_box

    def update_plot(self, x_value: float, y_value: float):
        """Update crosshair on 2D plot and rotate wind turbine display.
        
        Args:
            x_value: X value of crosshair, wind speed in m/s
            y_value: Y value of crosshair, pitch angle in °
        """
        if self.x_line:
            self.x_line.remove()
            self.y_line.remove()
        # set new placement lines on graph
        self.y_line = self.ax.axhline(y=y_value, color='r', linestyle='dotted')
        self.x_line = self.ax.axvline(x=x_value, color='r', linestyle='dotted')
        # rotate pitch display
        base = plt.gca().transData
        rot = trans.Affine2D().rotate_deg(-y_value)
        self.pitch_bar[0].set_transform(rot + base)

    def init_wind_display(self, ui_element=None):
        """Initialize 2D wind speed plot and plot for displaying pitch angle.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.wind_data = configuration.WindSpeedDataSet(self.WIND_PROFILE_FILE_PATH)
        x_data = []
        y_data = []
        for _, data in self.wind_data.get_data().iterrows():
            x_data.append(data["windspeed_mpers"])
            y_data.append(data["pitch_angle_deg"])
        # create windspeed to pitch diagram
        self.ax.plot(x_data, y_data)
        self.ax.set_xlabel('Wind speed (m/s)')
        self.ax.set_ylabel('Pitch (°)')
        # create pitch angle display diagram
        self.y_line = None
        self.x_line = None
        self.pitch_bar = None
        pitch_x, pitch_y = self.get_pitch_angle_graph_definition()
        self.pitch_bar = self.pitch_ax.plot(pitch_x, pitch_y, color='tab:blue')
        circle = patches.Circle((0,0), radius=1, facecolor=(0, 0, 0, 0), edgecolor="black", linestyle="solid",linewidth=2)
        self.pitch_ax.add_patch(circle)
        self.pitch_ax.set_aspect(1, adjustable='box')
        self.pitch_ax.axis('off')
        # adjust values last, it triggers change events and thus re-renders of the graph
        self.windspeed_slider.min = x_data[0]
        self.windspeed_slider.max = x_data[-1]
        self.windspeed_slider.step = round(x_data[1]-x_data[0], 5)
        initial_speed_value = x_data[math.floor(len(x_data) / 2)]
        self.windspeed_slider.value = initial_speed_value 

    def on_windspeed_slider_change(self, ui_element=None):
        """Update UI after windspeed slider has changed value.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.windspeed_changed(self.windspeed_slider.value)

    def on_windspeed_input_change(self, ui_element=None):
        """Update UI after windspeed number input has changed value.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.windspeed_changed(self.windspeed_input.value)

    def on_duration_input_change(self, change: Bunch):
        """Update UI after duration input has changed value."""
        self.update_total_rotations(change["new"], self.rotation_speed_display.value)

    def windspeed_changed(self, speed: float):
        """Update UI with data for new displayed wind speed."""
        rounded_speed = round(speed,5)
        self.windspeed_slider.value = rounded_speed
        self.windspeed_input.value = rounded_speed
        data = self.wind_data.get_single_entry(rounded_speed)
        self.rotation_speed_display.value = data["rpm"]
        self.pitch_angle_display.value = data["pitch_angle_deg"]
        self.update_total_rotations(self.duration_input.value, data["rpm"])
        self.update_plot(speed, data["pitch_angle_deg"])

    def update_total_rotations(self, duration: float, rpm: float):
        """Recalculate total rotations and update corresponding widget.
        
        Args:
            duration: Duration in years
            rpm: Rotations per minute
        """
        self.rotation_total_display.value = math.floor(duration * rpm * 60 * 24 * 365)

    def get_pitch_angle_graph_definition(self)-> tuple:
        """Return static point display used to draw wind turbine blade in matplot.
        
        Returns:
            list of floats as X data, list of floats a Y data
        """
        x_data = [0.98, 0.92, 0.86, 0.8, 0.65, 0.5, 0.35, 0.2, 0.05, -0.1, -0.16, -0.22, -0.28, -0.34,
                  -0.4, -0.46, -0.52, -0.58, -0.64, -0.7, -0.76, -0.81, -0.84, -0.87, -0.9, -0.93, -0.96,
                  -0.975, -0.984, -0.99, -0.996, -0.9975, -0.999, -0.9996, -0.9998, -0.9992, -0.998,
                  -0.9965, -0.992, -0.986, -0.98, -0.965, -0.94, -0.91, -0.88, -0.85, -0.82, -0.78, -0.72,
                  -0.66, -0.6, -0.54, -0.48, -0.42, -0.36, -0.3, -0.24, -0.18, -0.12, 0, 0.15, 0.3, 0.45,
                  0.6, 0.75, 0.84, 0.9, 0.96]
        y_data = [0.00676, 0.02272, 0.03808, 0.05346, 0.092, 0.12932, 0.16354, 0.1932, 0.2166, 0.23198,
                  0.23534, 0.23652, 0.23522, 0.23188, 0.22672, 0.21972, 0.2109, 0.20016, 0.18736, 0.17222,
                  0.15438, 0.13698, 0.12516, 0.11206, 0.0973, 0.0802, 0.05938, 0.0464, 0.0369, 0.02914,
                  0.01846, 0.01462, 0.00934, 0.00594, -0.00418, -0.00834, -0.01314, -0.0173, -0.02546,
                  -0.03234, -0.0375, -0.04704, -0.0582, -0.06812, -0.07608, -0.08276, -0.08854, -0.09516,
                  -0.10338, -0.10998, -0.11524, -0.11932, -0.12232, -0.12428, -0.12516, -0.12492, -0.12344,
                  -0.12028, -0.11524, -0.1017, -0.08064, -0.05716, -0.0336, -0.01224, 0.00372, 0.00878,
                  0.00922, 0.00608]
        return x_data, y_data

    def load_local_data(self, ui_element=None):
        """Use locally stored data for UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.conn = store_connection.LocalStoreConnection("sensotwin_world")
        self.load_input_set_data()
        self.init_wind_display()

    def load_remote_data(self, ui_element=None):
        """Use remotely stored data for UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.conn = store_connection.FusekiConnection(self.remote_database_input.value)
        self.load_input_set_data()
        self.init_wind_display()

    def save_new_input_set(self, ui_element=None):
        """Save currently set data for wind profile and duration of simulation.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        new_id = max(self.input_set_data.keys()) + 1
        new_set = configuration.StressStrainInputDataSet(
            uniq_id = new_id,
            wind_speed = self.windspeed_input.value,
            rotation_speed = self.rotation_speed_display.value,
            pitch_angle = self.pitch_angle_display.value,
            total_rotations = self.rotation_total_display.value,
            duration = self.duration_input.value,
            conn = self.conn)
        new_set.save_entry_to_store()
        self.load_input_set_data()

    def load_input_set_data(self):
        """Load data from previously set input source and populate UI."""
        self.input_set_data = configuration.StressStrainInputDataSet.get_all_entries_from_store(self.conn)
        options = []
        for key, input_set in self.input_set_data.items():
            label = input_set.generate_input_set_display_label()
            options.append((label, key))
        self.input_set_selection.options = options
        self.input_set_selection.value = list(self.input_set_data.keys())[0]

    def display_input_set(self, ui_element=None):
        """Display data for currently selected input set in UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        displayed_set = self.input_set_data[self.input_set_selection.value]

        self.windspeed_input.value = displayed_set.get_wind_speed()
        self.rotation_speed_display.value = displayed_set.get_rotation_speed()
        self.pitch_angle_display.value = displayed_set.get_pitch_angle()
        self.rotation_total_display.value = displayed_set.get_total_rotations()
        self.duration_input.value = displayed_set.get_duration()

        self.windspeed_changed(displayed_set.get_wind_speed())

    def apply_styling(self):
        css = """
            <style>
            {}
            {}
            </style>
        """.format(global_style.global_css, style.local_css)
        return widgets.HTML(css)

