from .. import read_vtt, write_vtt
from ..store_connection import LocalStoreConnection, FusekiConnection, StoreConnection
from pyiron_base import HasHDF
import pandas as pd
import datetime

class WindSpeedDataSet:
    """Data set containing information about wind speed and corresponding rotation speed and pitch angle of wind turbines.

    expects vtk table structure according to following definition:
    
    "windspeed_mpers": float (wind speed in m/s)
    "pitch_angle_deg": float (pitch angle of individual blades in °, 0 = vertical)
    "rpm": float (rotations per minute)
    """
    NECESSARY_KEYS = ["windspeed_mpers", "pitch_angle_deg", "rpm"]
    def __init__(self, vtt_path: str):
        """Initialise data set from file and check if necessary columns are present.

        Args:
            vtt_path: full or relative path of .vtt file containing cell information table
        """
        self._file_path = vtt_path
        self._dataframe = read_vtt.get_dataframe(self._file_path)
        self._dataframe["pitch_angle_deg"] = self._dataframe["pitch_angle_deg"].apply(lambda x: abs(x))
        self.check_data_source()

    def get_data(self):
        """Return all wind speed data"""
        return self._dataframe

    def get_single_entry(self, wind_speed: float) -> pd.DataFrame:
        """Return entry with chosen wind speed"""
        return self._dataframe.loc[self._dataframe.windspeed_mpers == wind_speed].iloc[0]

    def check_data_source(self):
        """Check if data source contains all necessary keys.
        
        Raises:
            KeyError: If any key as defined in NECESSARY_KEYS is not found
        """
        for key in self.NECESSARY_KEYS:
            if key not in self._dataframe:
                raise KeyError("'{}' not found in input data '{}'".format(key, self._file_path))

    def __str__(self):
        return "WindSpeedDataSet with {} entries from {} m/s to {} ms/s".format(len(self._dataframe),self._dataframe["windspeed_mpers"].min(), self._dataframe["windspeed_mpers"].max())

    def __repr__(self):
        return self.__str__()


class StressStrainInputDataSet(HasHDF):
    """Input data set for future simulation containing operational parameters.

    Contains relevant information of used wind profile like wind speed and rotations per minute in addition 
    to amount of the time simulation is supposed to run; implements pyiron HasHDF interface for automatic HDF5
    serialization during job execution for storage
    """
    def __init__(self, uniq_id: int=None, wind_speed: float=None, rotation_speed: float=None, pitch_angle: float=None, 
                 total_rotations: float=None, duration: float=None, conn: StoreConnection=None):
        """Initialize object.
        
        Args:
            uniq_id: id unique to the chosen store for this object class
            wind_speed: Wind speed in m/s
            rotation_speed: Rotation speed in rotations per minute
            pitch_angle: Pitch angle of wind turbine blades in ° (0 is vertical)
            total_rotations: Total number of rotations over the course of simulation (duration * rotation speed)
            duration: Total maximum duration of simulation in years
            conn: Any subclass instance of StoreConnection
        """
        self._id = uniq_id
        self._wind_speed = wind_speed
        self._rotation_speed = rotation_speed
        self._pitch_angle = pitch_angle
        self._total_rotations = total_rotations
        self._duration = duration
        self._store_connection = conn

    def get_id(self) -> int:
        """Return id of object."""
        return self._id

    def get_wind_speed(self) -> float:
        """Return wind speed in m/s of object."""
        return self._wind_speed

    def get_rotation_speed(self) -> float:
        """Return rotations per minute of object."""
        return self._rotation_speed

    def get_pitch_angle(self) -> float:
        """Return wind turbine blade pitch angle in ° of object."""
        return self._pitch_angle

    def get_total_rotations(self) -> float:
        """Return total number of rotations of object."""
        return self._total_rotations

    def get_duration(self) -> float:
        """Return maximum duration of simulation."""
        return self._duration

    def generate_input_set_display_label(self) -> str:
        """Return string display for object in list displays."""
        return "Wind: {} m/s, {} rot/min, {} years".format(self._wind_speed, self._rotation_speed, self._duration)

    @staticmethod
    def get_id_from_ontology_iri(label: str) -> str:
        """Extract id from full ontology instance iri."""
        return int(label.split("_")[-1])

    def generate_ontology_label(self) -> str:
        """Generate ontology instance iri from object."""
        return "__StressStrainInputDataSet_{}".format(self._id)

    def save_entry_to_store(self):
        """Save object to store connection passed during initialization."""
        label = self.generate_ontology_label()
        query = """
            INSERT {{
                senso:{label}_WindSpeed a senso:WindSpeed ;
                    co:value {wind_speed} .
                senso:{label}_WindRotationSpeed a senso:WindRotationSpeed ;
                    co:value {rotation_speed} .
                senso:{label}_WindPitchAngle a senso:WindPitchAngle ;
                    co:value {pitch_angle} .
                senso:{label}_WindTotalRotations a senso:WindTotalRotations ;
                    co:value {total_rotations} .
                senso:{label}_WindDuration a senso:WindDuration ;
                    co:value {duration} .
                  
                senso:{label} a senso:WindDataSimulationInputSet ;
                    co:composedOf senso:{label}_WindSpeed ;
                    co:composedOf senso:{label}_WindRotationSpeed ;
                    co:composedOf senso:{label}_WindPitchAngle ;
                    co:composedOf senso:{label}_WindTotalRotations ;
                    co:composedOf senso:{label}_WindDuration .
            }} WHERE {{ }}
            """.format(label=label, wind_speed=self._wind_speed, rotation_speed=self._rotation_speed, 
                       pitch_angle=self._pitch_angle, total_rotations=self._total_rotations, duration=self._duration)
        self._store_connection.update_data(query)

    @staticmethod
    def get_all_entries_from_store(conn: StoreConnection) -> dict:
        """Get all instances of StressStrainInputDataSet from store connection passed during initialization.

        Args:
            conn: Any subclass instance of StoreConnection
        Returns:
            dict of int -> StressStrainInputDataSet
        """
        query = """
            SELECT ?x ?windSpeed ?rotSpeed ?pitch ?totalRot ?duration
            WHERE {
                ?x a senso:WindDataSimulationInputSet .
                {
                    ?x co:composedOf ?speed_comp .
                    ?speed_comp a senso:WindSpeed .
                    ?speed_comp co:value ?windSpeed .
                }
                {
                    ?x co:composedOf ?rotspeed_comp .
                    ?rotspeed_comp a senso:WindRotationSpeed .
                    ?rotspeed_comp co:value ?rotSpeed .
                }
                {
                    ?x co:composedOf ?pitch_comp .
                    ?pitch_comp a senso:WindPitchAngle .
                    ?pitch_comp co:value ?pitch .
                }
                {
                    ?x co:composedOf ?rot_comp .
                    ?rot_comp a senso:WindTotalRotations .
                    ?rot_comp co:value ?totalRot .
                }
                {
                    ?x co:composedOf ?duration_comp .
                    ?duration_comp a senso:WindDuration .
                    ?duration_comp co:value ?duration .
                }
            }
            """
        result = {}
        for x in conn.get_data(query):
            uniq_id = StressStrainInputDataSet.get_id_from_ontology_iri(x[0])
            result[uniq_id] = StressStrainInputDataSet(uniq_id=uniq_id, wind_speed=float(x[1]), rotation_speed=float(x[2]), 
                                                      pitch_angle=float(x[3]), total_rotations=int(x[4]), duration=float(x[5]), conn=conn)
        return result

    def __str__(self):
        return "ID: {}, Wind Speed: {} m/s, Rotations: {}/min, Pitch Angle: {}°, Total Rotations: {}, Duration: {}y, Source: {}".format(
            self._id, self._wind_speed, self._rotation_speed, self._pitch_angle, self._total_rotations, self._duration, self._store_connection)

    def __repr__(self):
        return "StressStrainInputDataSet(uniq_id={uniq_id}, wind_speed={wind_speed}, rotation_speed={rotation_speed}, pitch_angle={pitch_angle}, total_rotations={total_rotations}, duration={duration}, conn={conn})".format(
            uniq_id = self._id,
            wind_speed = self._wind_speed,
            rotation_speed = self._rotation_speed,
            pitch_angle = self._pitch_angle,
            total_rotations = self._total_rotations,
            duration = self._duration,
            conn = repr(self._store_connection)
        )

    def _to_hdf(self, hdf=None, group_name=None):
        with hdf.open("stress_strain_input_data") as hdf_store:
            hdf_store["_id"] = self._id
            hdf_store["_wind_speed"] = self._wind_speed
            hdf_store["_rotation_speed"] = self._rotation_speed
            hdf_store["_pitch_angle"] = self._pitch_angle
            hdf_store["_total_rotations"] = self._total_rotations
            hdf_store["_duration"] = self._duration
            hdf_store["_store_connection"] = repr(self._store_connection)

    def _from_hdf(self, hdf=None, version=None):
        pass

    def from_hdf_args(hdf):
        with hdf.open("stress_strain_input_data") as hdf_store:
            arg_dict = {
                "uniq_id": hdf_store["_id"],
                "wind_speed": hdf_store["_wind_speed"],
                "rotation_speed": hdf_store["_rotation_speed"],
                "pitch_angle": hdf_store["_pitch_angle"],
                "total_rotations": hdf_store["_total_rotations"],
                "duration": hdf_store["_duration"],
                "conn": eval(hdf_store["_store_connection"])
            }
        return arg_dict
