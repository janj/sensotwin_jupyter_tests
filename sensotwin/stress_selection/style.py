

local_css = """
.wind_sel_graph_tab_container {
    display: flex !important;
    flex-direction: row !important;
    align-items: center !important;
    justify-content: space-around !important;
    width: 100% !important;
}
.wind_sel_basic_input {
    width: 400px !important;
}
.wind_sel_basic_input > label {
    width: 200px !important;
}
.wind_sel_graph_tab_container > .jupyter-matplotlib {
    flex-grow: 0 !important;
}
.wind_sel_selection_box {
    padding-top: 20px !important;
}
"""