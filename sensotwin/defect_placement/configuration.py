from collections import defaultdict, Counter
from .. import read_vtt, write_vtt
from ..store_connection import LocalStoreConnection, FusekiConnection, StoreConnection
from pyiron_base import HasHDF
import numpy as np
import pandas as pd
from abc import ABC, abstractmethod

class InputDefect(ABC):
    """Base class for all defect types, only used to search for available defect types via __subclasses__.
    
    Attributes:
        type_id: every subclass must have unique integer id, creating an uninterruped serial starting from 1 to total number of subclasses
    """
    type_id = 0
    _id = None
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def generate_display_label(self):
        pass

    @abstractmethod
    def generate_ontology_label(self, set_id, iter_id):
        pass

    @abstractmethod
    def generate_store_insert_stub(self):
        pass

    @staticmethod
    @abstractmethod
    def get_all_entries_from_store(conn):
        pass

    def get_cell_id(self) -> int:
        """Return cell position of defect."""
        return self._cell_id

    def get_layer_id(self) -> int:
        """Return layer position of defect."""
        return self._layer_id

    @staticmethod
    def get_id_from_ontology_iri(label) -> int:
        """Extract defect id from full ontology instance iri."""
        return int(label.split("_")[-1])

    @staticmethod
    def get_set_id_from_ontology_iri(label) -> int:
        """Extract input set id from full ontology instance iri."""
        return int(label.split("_")[-2])


class PlyWavinessDefect(InputDefect):
    """Ply waviness defect.

    Attributes:
        type_id: Integer unique among all subclasses of InputDefect belonging to one specific defect input set
        display_name: Human readable name of defect type for legends
        display_color: tuple of RGB color values scaled from 0 to 1
    """
    data = None
    type_id = 1
    display_name = "Ply Waviness"
    display_color = (200 / 256, 200 / 256, 0 / 256)

    def __init__(self, cell_id: int=None, layer_id: int=None, **param):
        """Initialize object.

        Args:
            cell_id: integer cell id from 3D mesh of wind turbine
            layer_id: integer layer id from 3D mesh of wind turbine
        """
        self._cell_id = cell_id
        self._layer_id = layer_id
        self._param = param["param"]

    def generate_display_label(self):
        """Return string display for object in list displays."""
        return "{}, Waviness: {}?".format(self.display_name, self._param)

    def generate_ontology_label(self, set_id: int, iter_id: int) -> str:
        """Generate ontology instance iri from object.

        Args:
            set_id: id of input set this defect belongs to
            iter_id: unique id of defect within this specific input set

        Returns:
            relative iri of ontology instance
        """
        return "__PlyWavinessDefect_{set_id}_{iter_id}".format(set_id=set_id, iter_id=iter_id)

    def generate_store_insert_stub(self, uniq_id: str) -> str:
        """Generate SPARQL stub for inserting this object in a multi INSERT query.

        Args:
            uniq_id: unique string identifier with a combination of input set and defect ids

        Returns:
            SPARQL insert statement without INSERT, PREFIXES or WHERE clause
        """
        return """
            senso:__PlyWavinessDefect_{uniq_id} a senso:PlyWavinessDefect ;
                co:value {param} ;
                senso:hasCellNumber {cell_id} ;
                senso:hasLayerNumber {layer_id} .
        """.format(uniq_id=uniq_id, cell_id=self._cell_id, layer_id=self._layer_id, param=self._param)

    @staticmethod
    def get_all_entries_from_store(conn: StoreConnection) -> dict:
        """Get all instances of PlyWavinessDefect from store connection passed during initialization.

        Args:
            conn: Any subclass instance of StoreConnection
        Returns:
            dict of int -> PlyWavinessDefect
        """
        query = """
            SELECT ?x ?param ?cell_number ?layer_number
            WHERE {
                ?x a senso:PlyWavinessDefect .
                ?x co:value ?param .
                ?x senso:hasCellNumber ?cell_number .
                ?x senso:hasLayerNumber ?layer_number .
            }
            """
        result = {}
        for x in conn.get_data(query):
            result[x[0]] = PlyWavinessDefect(cell_id=int(x[2]), layer_id=int(x[3]), param=float(x[1]))
        return result

    def __str__(self):
        return "{} (Type ID {}) in cell {}, layer {}, data TBA".format(self.display_name, self.type_id, self._cell_id, self._layer_id)

    def __repr__(self):
        return "PlyWavinessDefect(cell_id={cell_id}, layer_id={layer_id}, param={param})".format(cell_id = self._cell_id, layer_id = self._layer_id, param = self._param)


class PlyMisorientationDefect(InputDefect):
    """Defect for different fibre angle in layer than intended.

    Attributes:
        type_id: Integer unique among all subclasses of InputDefect belonging to one specific defect input set
        display_name: Human readable name of defect type for legends
        display_color: tuple of RGB color values scaled from 0 to 1
    """
    data = None
    type_id = 2
    display_name = "Ply Misorientation"
    display_color = (12 / 256, 238 / 256, 246 / 256)

    def __init__(self, cell_id: int=None, layer_id: int=None, **param):
        """Initialize object.

        Args:
            cell_id: integer cell id from 3D mesh of wind turbine
            layer_id: integer layer id from 3D mesh of wind turbine
        """
        self._cell_id = cell_id
        self._layer_id = layer_id
        self._param = param["param"]

    def generate_display_label(self) -> str:
        """Return string display for object in list displays."""
        return "{}, Offset: {}°".format(self.display_name, self._param)

    def generate_ontology_label(self, set_id: int, iter_id: int) -> str:
        """Generate ontology instance iri from object.

        Args:
            set_id: id of input set this defect belongs to
            iter_id: unique id of defect within this specific input set

        Returns:
            relative iri of ontology instance
        """
        return "__PlyMisorientationDefect_{set_id}_{iter_id}".format(set_id=set_id, iter_id=iter_id)

    def generate_store_insert_stub(self, uniq_id: str) -> str:
        """Generate SPARQL stub for inserting this object in a multi INSERT query.

        Args:
            uniq_id: unique string identifier with a combination of input set and defect ids

        Returns:
            SPARQL insert statement without INSERT, PREFIXES or WHERE clause
        """
        return """
            senso:__PlyMisorientationDefect_{uniq_id} a senso:PlyMisorientationDefect ;
                co:value {param} ;
                senso:hasCellNumber {cell_id} ;
                senso:hasLayerNumber {layer_id} .
        """.format(uniq_id=uniq_id, cell_id=self._cell_id, layer_id=self._layer_id, param=self._param)

    @staticmethod
    def get_all_entries_from_store(conn: StoreConnection) -> dict:
        """Get all instances of PlyMisorientationDefect from store connection passed during initialization.

        Args:
            conn: Any subclass instance of StoreConnection
        Returns:
            dict of int -> PlyMisorientationDefect
        """
        query = """
            SELECT ?x ?param ?cell_number ?layer_number
            WHERE {
                ?x a senso:PlyMisorientationDefect .
                ?x co:value ?param .
                ?x senso:hasCellNumber ?cell_number .
                ?x senso:hasLayerNumber ?layer_number .
            }
            """
        result = {}
        for x in conn.get_data(query):
            result[x[0]] = PlyMisorientationDefect(cell_id=int(x[2]), layer_id=int(x[3]), param=float(x[1]))
        return result

    def __str__(self):
        return "{} (Type ID {}) in cell {}, layer {}, data TBA".format(self.display_name, self.type_id, self._cell_id, self._layer_id)

    def __repr__(self):
        return "PlyMisorientationDefect(cell_id={cell_id}, layer_id={layer_id}, param={param})".format(cell_id = self._cell_id, layer_id = self._layer_id, param = self._param)


class MissingPlyDefect(InputDefect):
    """Defect for an entire layer missing.

    Attributes:
        type_id: Integer unique among all subclasses of InputDefect belonging to one specific defect input set
        display_name: Human readable name of defect type for legends
        display_color: tuple of RGB color values scaled from 0 to 1
    """
    data = None
    type_id = 3
    display_name = "Missing Ply"
    display_color = (25 / 256, 248 / 256, 5 / 256)

    def __init__(self, cell_id: int=None, layer_id: int=None, **param):
        """Initialize object.

        Args:
            cell_id: integer cell id from 3D mesh of wind turbine
            layer_id: integer layer id from 3D mesh of wind turbine
        """
        self._cell_id = cell_id
        self._layer_id = layer_id

    def generate_display_label(self) -> str:
        """Return string display for object in list displays."""
        return "{}, Ply missing".format(self.display_name)

    def generate_ontology_label(self, set_id, iter_id):
        """Generate ontology instance iri from object.

        Args:
            set_id: id of input set this defect belongs to
            iter_id: unique id of defect within this specific input set

        Returns:
            relative iri of ontology instance
        """
        return "__MissingPlyDefect_{set_id}_{iter_id}".format(set_id=set_id, iter_id=iter_id)

    def generate_store_insert_stub(self, uniq_id: str) -> str:
        """Generate SPARQL stub for inserting this object in a multi INSERT query.

        Args:
            uniq_id: unique string identifier with a combination of input set and defect ids

        Returns:
            SPARQL insert statement without INSERT, PREFIXES or WHERE clause
        """
        return """
            senso:__MissingPlyDefect_{uniq_id} a senso:MissingPlyDefect ;
                senso:hasCellNumber {cell_id} ;
                senso:hasLayerNumber {layer_id} .
        """.format(uniq_id=uniq_id, cell_id=self._cell_id, layer_id=self._layer_id)

    @staticmethod
    def get_all_entries_from_store(conn: StoreConnection) -> dict:
        """Get all instances of MissingPlyDefect from store connection passed during initialization.

        Args:
            conn: Any subclass instance of StoreConnection
        Returns:
            dict of int -> MissingPlyDefect
        """
        query = """
            SELECT ?x ?cell_number ?layer_number
            WHERE {
                ?x a senso:MissingPlyDefect .
                ?x senso:hasCellNumber ?cell_number .
                ?x senso:hasLayerNumber ?layer_number .
            }
            """
        result = {}
        for x in conn.get_data(query):
            result[x[0]] = MissingPlyDefect(cell_id=int(x[1]), layer_id=int(x[2]))
        return result

    def __str__(self):
        return "{} (Type ID {}) in cell {}, layer {}, data TBA".format(self.display_name, self.type_id, self._cell_id, self._layer_id)

    def __repr__(self):
        return "MissingPlyDefect(cell_id={cell_id}, layer_id={layer_id})".format(cell_id = self._cell_id, layer_id = self._layer_id)


class VaryingThicknessDefect(InputDefect):
    """Defect for a different layer thickness than intended.

    Attributes:
        type_id: Integer unique among all subclasses of InputDefect belonging to one specific defect input set
        display_name: Human readable name of defect type for legends
        display_color: tuple of RGB color values scaled from 0 to 1
    """
    data = None
    type_id = 4
    display_name = "Varying Thickness"
    display_color = (255 / 256, 130 / 256, 3 / 256)

    def __init__(self, cell_id=None, layer_id=None, **param):
        self._cell_id = cell_id
        self._layer_id = layer_id
        self._param = param["param"]

    def generate_display_label(self) -> str:
        """Return string display for object in list displays."""
        return "{}, Variation: {}?".format(self.display_name, self._param)

    def generate_ontology_label(self, set_id: int, iter_id: int) -> str:
        """Generate ontology instance iri from object.

        Args:
            set_id: id of input set this defect belongs to
            iter_id: unique id of defect within this specific input set

        Returns:
            relative iri of ontology instance
        """
        return "__VaryingThicknessDefect__{set_id}_{iter_id}".format(set_id=set_id, iter_id=iter_id)

    def generate_store_insert_stub(self, uniq_id: str) -> str:
        """Generate SPARQL stub for inserting this object in a multi INSERT query.

        Args:
            uniq_id: unique string identifier with a combination of input set and defect ids

        Returns:
            SPARQL insert statement without INSERT, PREFIXES or WHERE clause
        """
        return """
            senso:__VaryingThicknessDefect_{uniq_id} a senso:VaryingThicknessDefect ;
                co:value {param} ;
                senso:hasCellNumber {cell_id} ;
                senso:hasLayerNumber {layer_id} .
        """.format(uniq_id=uniq_id, cell_id=self._cell_id, layer_id=self._layer_id, param=self._param)

    @staticmethod
    def get_all_entries_from_store(conn: StoreConnection) -> dict:
        """Get all instances of VaryingThicknessDefect from store connection passed during initialization.

        Args:
            conn: Any subclass instance of StoreConnection
        Returns:
            dict of int -> VaryingThicknessDefect
        """
        query = """
            SELECT ?x ?param ?cell_number ?layer_number
            WHERE {
                ?x a senso:VaryingThicknessDefect .
                ?x co:value ?param .
                ?x senso:hasCellNumber ?cell_number .
                ?x senso:hasLayerNumber ?layer_number .
            }
            """
        result = {}
        for x in conn.get_data(query):
            result[x[0]] = VaryingThicknessDefect(cell_id=int(x[2]), layer_id=int(x[3]), param=float(x[1]))
        return result

    def __str__(self):
        return "{} (Type ID {}) in cell {}, layer {}, data TBA".format(self.display_name, self.type_id, self._cell_id, self._layer_id)

    def __repr__(self):
        return "VaryingThicknessDefect(cell_id={cell_id}, layer_id={layer_id}, param={param})".format(cell_id = self._cell_id, layer_id = self._layer_id, param = self._param)


class DefectInputDataSet(HasHDF):
    """Input data set for future simulation containing placed defects.

    Contains all info for a single simulation concerning deviations from the standard 3D model
    Information is sorted according to cell id of the 3D model first and then layer information of
    composite material markup second, individual defects are instances of an InputDefect subclass; 
    implements pyiron HasHDF interface for automatic HDF5 serialization during job execution for storage
    """
    def __init__(self, uniq_id: int=None, defects: list=None, conn: StoreConnection=None):
        """Initialize empty internal data storage object"""
        self._defect_data = {}
        self._id = uniq_id
        self._store_connection = conn
        if defects:
            for defect in defects:
                self.add_defect_to_cell(defect.get_cell_id(), defect.get_layer_id(), defect)

    def get_id(self):
        """Return id of input data set, can be None if object not saved to data storage yet"""
        return self._id
    
    def get_defects_of_cell(self, cell_id: int) -> dict:
        """Return all defects for specified cell

        Args:
            cell_id: unique cell id of associated 3D model

        Returns:
            dict of layer id to defect dicts (defect id => defect object) 
        """
        if cell_id not in self._defect_data:
            return {}
        else:
            return self._defect_data[cell_id]

    def add_defect_to_cell(self, cell_id: int, layer_id: int, defect: InputDefect):
        """Add 1 defect,overwriting any existing defect of this specific InputDefect subclass.

        Args:
            cell_id: unique cell id of associated 3D model
            layer_id: id of layer in material of defect
            defect: child class of InputDefect containing defect data
        """
        if cell_id not in self._defect_data:
            self._defect_data[cell_id] = {}
        if layer_id not in self._defect_data[cell_id]:
            self._defect_data[cell_id][layer_id] = {}
        self._defect_data[cell_id][layer_id][defect.type_id] = defect

    def clear_defects_of_cell(self, cell_id: int):
        """Remove all defects of specific cell"""
        if cell_id in self._defect_data:
            self._defect_data.pop(cell_id)

    def generate_input_file(self):
        print("generate the calculix input file i guess")

    def generate_defect_display_summary(self) -> dict:
        """Generate summary containing ids of all cells with defects and list of contained defect types, disregarding layer information.

        Returns:
            dictionary containing list of cells of with defects and list of defect types per cell
        """
        summary = {
            "defect_cells": list(self._defect_data.keys()),
            "cell_defect_types": {}
        }
        for vtk_id, layers in self._defect_data.items():
            summary["cell_defect_types"][vtk_id] = []
            for layer_id, layer_data in layers.items():
                summary["cell_defect_types"][vtk_id].extend(layer_data.keys())
            summary["cell_defect_types"][vtk_id] = list(set(summary["cell_defect_types"][vtk_id]))
        return summary

    def generate_input_set_display_label(self) -> str:
        """Return string display for object in list displays."""
        summ = self.get_defects()
        c = Counter([x.type_id for x in summ])
        occurence_string = ", ".join(["{}: {}".format(value, count) for value, count in c.most_common()])
        return "Total: {} | {}".format(c.total(), occurence_string)

    def get_defects(self) -> list:
        """Get all defect objects of input set
        
        Returns:
            list of InputDefect objects
        """
        defects = []
        for _, cell_data in self._defect_data.items():
            for _, layer_data in cell_data.items():
                for defect_id, defect in layer_data.items():
                    defects.append(defect)
        return defects

    @staticmethod
    def get_available_defect_types() -> list:
        """Return all subclasses of the class InputDefect"""
        return InputDefect.__subclasses__()

    def generate_ontology_label(self) -> str:
        """Generate ontology instance iri from object."""
        return "__DefectPlacementInputSet_{}".format(self._id)

    @staticmethod
    def get_id_from_ontology_iri(label: str) -> int:
        """Extract id from full ontology instance iri."""
        return int(label.split("_")[-1])

    def generate_store_insert_stub(self, uniq_id):
        return """
            senso:__DefectPlacementInputSet_{uniq_id} a senso:DefectPlacementInputSet .
        """.format(uniq_id=uniq_id)

    def save_entry_to_store(self, set_id: int):
        """Save object to store connection passed during initialization."""
        self._id = set_id
        label = self.generate_ontology_label()
        insert_stubs = [self.generate_store_insert_stub(self._id)]
        for i, defect in enumerate(self.get_defects()):
            new_id = "{}_{}".format(self._id, i)
            insert_stubs.append(defect.generate_store_insert_stub(new_id))  
        query = """
            INSERT {{
            {stubs}
            }} WHERE {{ }}
            """.format(stubs="\n".join(insert_stubs))
        self._store_connection.update_data(query)

    @staticmethod
    def get_all_entries_from_store(conn: StoreConnection) -> dict:
        """Get all instances of DefectInputDataSet from store connection passed during initialization.

        Args:
            conn: Any subclass instance of StoreConnection
        Returns:
            dict of int -> DefectInputDataSet
        """
        defects = {}
        for defect_type in DefectInputDataSet.get_available_defect_types():
            defects[defect_type.__name__] = defect_type.get_all_entries_from_store(conn)
        query = """ 
            SELECT DISTINCT ?x
            WHERE {
              ?x a senso:DefectPlacementInputSet .
            }
            """
        result = {}
        for x in conn.get_data(query):
            uniq_id = DefectInputDataSet.get_id_from_ontology_iri(x[0])
            result[uniq_id] = DefectInputDataSet(uniq_id=uniq_id, conn=conn)
        for defect_type, defect_dict in defects.items():
            for iri, defect in defect_dict.items():
                set_id = defect.get_set_id_from_ontology_iri(iri)
                result[set_id].add_defect_to_cell(defect.get_cell_id(), defect.get_layer_id(), defect)
        return result

    def __str__(self):
        summ = self.get_defects()
        c = Counter([x.display_name for x in summ])
        occurence_string = ", ".join(["{}: {}".format(value, count) for value, count in c.most_common()])
        return "DefectInputDataSet ID {} with {} defects: {}".format(self._id, c.total(), occurence_string)

    def __repr__(self):
        return "DefectInputDataSet(uniq_id={uniq_id}, defects={defects}, conn={conn})".format(uniq_id=self._id,defects=repr(self.get_defects()),conn=repr(self._store_connection))

    def _to_hdf(self, hdf=None, group_name=None):
        with hdf.open("defect_placement_input_data") as hdf_store:
            hdf_store["_id"] = self._id
            hdf_store["_defects"] = repr(self.get_defects())
            hdf_store["_store_connection"] = repr(self._store_connection)

    def _from_hdf(self, hdf=None, version=None):
        pass

    def from_hdf_args(hdf):
        with hdf.open("defect_placement_input_data") as hdf_store:
            arg_dict = {
                "uniq_id": hdf_store["_id"], 
                "defects": eval(hdf_store["_defects"]),
                "conn": eval(hdf_store["_store_connection"])
            }
        return arg_dict


class CellLayersDataSet:
    """Data set containing information about layer structure of all cells.

    reads data from .vtt file containing information about the different layers for each cell of the 3D model
    expects vtk table structure according to following definition:
    
    "element_label": int (id of associated cell), starting at 1
    "layer_id": int (id of layer, unique per cell)
    "layer_material": str (layer material name)
    "layer_thickness_mm": float (thickness of layer in mm)
    "layer_angle_deg": int (angle of fiber alignment in °, [-90 to 90])

    """
    NECESSARY_KEYS = ["element_label", "layer_id", "layer_material", "layer_thickness_mm", "layer_angle_deg"]
    def __init__(self, vtt_path: str):
        """Initialise data set from file and check if necessary columns are present.

        Args:
            vtt_path: full or relative path of .vtt file containing cell information table
        """
        self._file_path = vtt_path
        self._dataframe = read_vtt.get_dataframe(self._file_path)
        self.check_data_source()
        self._dataframe.layer_material.replace(to_replace={"SAERTEX-U-E-SKIN": "GLASS-U-E-SKIN","SAERTEX-U-E-WEB": "GLASS-U-E-WEB", "E-LT-5500(UD)": "GLASS-UD"}, inplace=True)

    def get_available_layers_for_cell(self, cell_id: int) -> pd.DataFrame:
        """Return dataframe containing the data subset of the passed cell_id """
        return self._dataframe.loc[self._dataframe.element_label == cell_id]

    def get_layer_of_cell(self, cell_id: int, layer_id: int) -> pd.DataFrame:
        """Return one specific entry to dataset with the correct combination of cell id and layer id"""
        return self._dataframe.loc[(self._dataframe.element_label == cell_id) & (self._dataframe.layer_id == layer_id)].iloc[0]

    def check_data_source(self):
        """Check if data source contains all necessary keys.
        
        Raises:
            KeyError: If any key as defined in NECESSARY_KEYS is not found
            ValueError: If serial ids in element_label do not start with 1
        """
        for key in self.NECESSARY_KEYS:
            if key not in self._dataframe:
                raise KeyError("'{}' not found in input data '{}'".format(key, self._file_path))
        if self._dataframe["element_label"].min() != 1:
            raise ValueError("Serial IDs in 'element_label' need to start at 1, minimum value found: {}".format(self._dataframe["element_label"].min()))

    def __str__(self):
        return "CellLayersDataSet with {} cells with up to {} layers per cell".format(len(self._dataframe["element_label"].unique()), len(self._dataframe["layer_id"].unique()))

    def __repr__(self):
        return self.__str__()


def get_material_color(name: str) -> str:
    """Get color for specific layer in matplotlib material layup display."""
    color_dict = defaultdict(lambda: 'white')
    color_dict['gelcoat'] = 'red'
    color_dict['GELCOAT'] = 'red'
    color_dict['SNL(Triax)'] = 'green'
    color_dict['saertex(DB)'] = 'blue'
    color_dict['foam'] = 'yellow'
    color_dict['FOAM'] = 'yellow'
    color_dict['Carbon(UD)'] = 'cyan'
    color_dict['CARBON(UD)'] = 'cyan'
    color_dict['E-LT-5500(UD)'] = 'salmon'
    color_dict["GLASS-UD"] = 'salmon'
    color_dict['E-LT-5500(UD)-SKIN'] = 'orange'
    color_dict['GLASS-TRIAX'] = 'orange'
    color_dict['E-LT-5500(UD)-WEB'] = 'peachpuff'
    color_dict['SAERTEX-U-E-SKIN'] = 'orange'
    color_dict['SAERTEX-U-E-WEB'] = 'peachpuff'
    color_dict['GLASS-U-E-SKIN'] = 'orange'
    color_dict['GLASS-U-E-WEB'] = 'peachpuff'
    return color_dict[name]


