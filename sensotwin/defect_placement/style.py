

local_css = """
.defect_place_simulation_row_container {
    width: 100% !important;
    display: flex !important;
    flex-direction: row !important;
    align-items: stretch !important;
    justify-content: space-around !important;
}
.defect_place_layer_render_container {
    width: 100% !important;
}
.defect_place_layer_render_container > div > .jupyter-matplotlib-figure {
    margin: 0 auto !important;
}
.defect_place_layer_input {
    width: 99% !important;
}
.defect_place_layer_input > label {
    width: 200px !important;
}
.defect_place_layup_headline {
    margin-left: 50px !important;
}
.defect_param_output_widget {
    margin: 0px !important;
}
.defect_param_input > label {
    width: 40% !important;
}
"""