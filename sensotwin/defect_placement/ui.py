import ipywidgets as widgets
from . import configuration
from . import style
from .. import global_style
from .. import store_connection
import functools
import pyvista as pv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as pltcolor
import copy
from .. import composite_layup
from traitlets.utils.bunch import Bunch
import pandas as pd


class DefectPlacementUIElements:
    """Contains all UI functionality of material curing process configuration UI (Step 2).

    In order to use, show the top level widget 'self.dashboard' in one cell of a notebook
    and the apply css styling calling 'self.apply_styling()' an another cell
    """
    MESH_FILE_PATH = 'Inputs/input_mesh_17052024.vtu'
    SHELL_DATA_KEY = 'WebShellAssignment'
    SECTION_DATA_KEY = 'SectionAssignment'
    MESH_DATA = {}

    LAYER_FILE_PATH = "Inputs/composite_layup_db.vtt"
    LAYER_ID_KEY = 'layer_id'
    LAYER_MATERIAL_KEY = 'layer_material'
    LAYER_THICKNESS_KEY = 'layer_thickness_mm'
    LAYER_ANGLE_KEY = 'layer_angle_deg'
    
    def __init__(self):        
        """Initialize UI objects and associated UI functionality."""
        self.init_global_data()
        self.dashboard = widgets.Tab().add_class("global_tab_container")
        self.dashboard.children = [
            self.init_data_source_box(),
            self.init_placement_box()
        ]
        tab_titles = ['Input', 'Simulation']
        for i in range(len(tab_titles)):
            self.dashboard.set_title(i, tab_titles[i])
        # manually trigger change event to properly render intial defect configuration display
        self.on_defect_type_changed()

    def init_global_data(self):
        """Set necessary global parameters for UI and load static data like 3D mesh and 3D mesh layer data."""
        self.available_defects = {}
        for defect in configuration.DefectInputDataSet().get_available_defect_types():
            self.available_defects[defect.type_id] = defect
        self.new_defect_to_place = self.available_defects[min(self.available_defects.keys())]
        self.MULTIPLE_DEFECTS_COLOR_ID = len(self.available_defects) + 1
        self.SELECTION_COLOR_ID = self.MULTIPLE_DEFECTS_COLOR_ID + 1
        self.COLOR_DEFINITIONS = self.create_scalar_color_definitions()
        self.selected_cells = {
            "center": None,
            "all": None,
            "grow_counter" : 0
        }
        self.active_scalar_display = 'state'
        self.layer_data = configuration.CellLayersDataSet(self.LAYER_FILE_PATH)
        self.MESH_DATA = self.create_display_mesh_data()

    def init_data_source_box(self) -> widgets.Box:
        """Initialize first tab of dashboard, choosing data source."""
        local_database_label = widgets.Label(value="Use local owlready2 database:").add_class("global_headline")
        use_local_data_button = widgets.Button(
            description='Use local database',
            disabled=False,
            tooltip='Use local database',
            icon='play',
        ).add_class("global_load_data_button").add_class("global_basic_button")
        use_local_data_button.on_click(self.load_local_data)
        
        remote_database_label = widgets.Label(value="Use remote Apache Jena Fuseki database:").add_class("global_headline")
        self.remote_database_input = widgets.Text(
            value=None,
            placeholder="insert SPARQL url here",
            description="SPARQL Endpoint:",
            disabled=False   
        ).add_class("global_url_input").add_class("global_basic_input")
        use_remote_data_button = widgets.Button(
            description='Use remote database',
            disabled=False,
            tooltip='Use remote database',
            icon='play',
        ).add_class("global_load_data_button").add_class("global_basic_button")
        use_remote_data_button.on_click(self.load_remote_data)
        local_data_box = widgets.VBox([
            local_database_label,
            use_local_data_button
        ])
        
        remote_data_box = widgets.VBox([
            remote_database_label,
            self.remote_database_input,
            use_remote_data_button
        ])
        data_source_box = widgets.VBox([
            local_data_box,
            remote_data_box
        ]).add_class("global_data_tab_container")
        return data_source_box

    def init_pyvista_render(self) -> widgets.Output:
        """Initialize Jupyter Output widget for rendering 3D mesh via PyVista."""
        OUTPUT_RENDER_HEIGHT = 500
        OUTPUT_RENDER_WIDTH= 1000
        self.plotter = pv.Plotter()
        for id, data in self.MESH_DATA.items():
            data["mesh"] = self.plotter.add_mesh(data["input"], show_edges=True, show_scalar_bar=False, clim=data["value_ranges"]['state'], cmap=self.COLOR_DEFINITIONS['state'], scalars="color")
        self.plotter.camera_position = [(-30.765771120379302, -28.608772602676154, 39.46235706090557),
             (0.9572034500000001, 0.0005481500000000805, -30.4),
             (-0.16976192468426815, -0.8825527410211623, -0.43849919982085056)]
        self.plotter.window_size = [OUTPUT_RENDER_WIDTH, OUTPUT_RENDER_HEIGHT]
        self.plotter.enable_element_picking(callback=self.cell_selected, show_message=False)
        legend_labels = [(defect.display_name, defect.display_color) for _, defect in self.available_defects.items()]
        legend_labels.append(["Multiple Defects", (1.0, 0.0, 0.0)])
        self.plotter.add_legend(labels=legend_labels, bcolor=None, size=(0.2,0.2), loc="upper right", face=None)
        render_widget = widgets.Output(layout={'height': '{}px'.format(OUTPUT_RENDER_HEIGHT+15), 
                                               'width': '{}px'.format(OUTPUT_RENDER_WIDTH+10)})
        with render_widget:
            self.plotter.show(jupyter_backend='trame')
        return render_widget

    def init_placement_box(self) -> widgets.Box:
        """Initialize second tab of dashboard, configuring construction defects."""
        self.selected_id_widget = widgets.IntText(
            value=None,
            description="Selected Cell",
            disabled=True
        ).add_class("global_basic_input").add_class("defect_place_layer_input")
        self.layer_dropdown_widget = widgets.Dropdown(
            options=['Select cell first'],
            value='Select cell first',
            description='Show Layer',
            disabled=False,
        ).add_class("global_basic_input").add_class("defect_place_layer_input")
        self.cell_layer_widget = widgets.FloatText(
            placeholder='select cell',
            description="Layer ID",
            disabled=True
        ).add_class("global_basic_input").add_class("defect_place_layer_input")
        self.layer_material_widget = widgets.Text(
            placeholder='select cell',
            description="Material",
            disabled=True
        ).add_class("global_basic_input").add_class("defect_place_layer_input")
        self.layer_thickness_widget = widgets.FloatText(
            placeholder='select cell',
            description="Thickness (mm)",
            disabled=True
        ).add_class("global_basic_input").add_class("defect_place_layer_input")
        self.layer_angle_widget = widgets.FloatText(
            placeholder='select cell',
            description="Angle (°)",
            disabled=True
        ).add_class("global_basic_input").add_class("defect_place_layer_input")
        
        rendering_toggle_buttons = []
        show_all_button = widgets.Button(
            description='Show all',
            disabled=False
        ).add_class("global_basic_button")
        show_all_button.on_click(self.show_mesh)
        rendering_toggle_buttons.append(show_all_button)
        for id, data in self.MESH_DATA.items():
            current_button = widgets.Button(
                description='Show ' + data["name"],
                disabled=False
            ).add_class("global_basic_button")
            current_button.on_click(functools.partial(self.show_mesh, selected_meshes=tuple([id])))
            rendering_toggle_buttons.append(current_button)
        toggle_button_box = widgets.HBox([x for x in rendering_toggle_buttons])
        show_defects_button = widgets.Button(
            description='Show Defects',
            disabled=False
        ).add_class("global_basic_button")
        show_defects_button.on_click(functools.partial(self.change_scalar_display_mode, scalar_name="state"))
        show_sections_button = widgets.Button(
            description='Show Sections',
            disabled=False
        ).add_class("global_basic_button")
        show_sections_button.on_click(functools.partial(self.change_scalar_display_mode, scalar_name="section"))
        scalar_button_box = widgets.HBox([show_defects_button, show_sections_button])

        place_defect_button = widgets.Button(
            description='Place Defect',
            disabled=False
        ).add_class("global_basic_button")
        place_defect_button.on_click(self.place_defect)
        remove_defects_button = widgets.Button(
            description='Remove Defects',
            disabled=False
        ).add_class("global_basic_button")
        remove_defects_button.on_click(self.remove_defects)
        grow_selection_button = widgets.Button(
            description='Grow Selection',
            disabled=False
        ).add_class("global_basic_button")
        grow_selection_button.on_click(self.grow_selection)
        shrink_selection_button = widgets.Button(
            description='Shrink Selection',
            disabled=False
        ).add_class("global_basic_button")
        shrink_selection_button.on_click(self.shrink_selection)
        self.defect_id_widget = widgets.Dropdown(
            options=[(defect.display_name, defect_id) for defect_id, defect in self.available_defects.items()],
            value=min(self.available_defects.keys()),
            description='Defect:',
            disabled=False,
        ).add_class("global_basic_input")
        self.defect_id_widget.observe(self.on_defect_type_changed, names=['value'])
        self.defect_param_output = widgets.Output().add_class("defect_param_output_widget")
        self.defect_param_widget = widgets.FloatText(
            value=0,
            description="Init",
            disabled=False
        ).add_class("defect_param_input").add_class("global_basic_input")
        
        self.fig = self.init_cell_layer_rendering()
        select_displayed_layer_headline = widgets.Label(value="Display Options:").add_class("global_headline")
        selected_cell_headline = widgets.Label(value="Cell Information:").add_class("global_headline")
        cell_select_info_box = widgets.VBox([
            select_displayed_layer_headline,
            toggle_button_box,
            scalar_button_box,
            selected_cell_headline,
            self.selected_id_widget,
            self.layer_dropdown_widget,
            self.cell_layer_widget,
            self.layer_material_widget,
            self.layer_thickness_widget,
            self.layer_angle_widget
        ])
        defect_configuration_box = widgets.VBox([
            widgets.HBox([
                place_defect_button,
                remove_defects_button
            ]),
            self.defect_id_widget, 
            self.defect_param_output
        ])
        defect_definition_box = widgets.VBox([
            widgets.Label(value="Cell selection:").add_class("global_headline"),
            widgets.HBox([
                grow_selection_button,
                shrink_selection_button
            ]),
            widgets.Label(value="Defect configuration:").add_class("global_headline"),
            defect_configuration_box
        ])
        layup_headline = widgets.Label(value="Cell Material Layup:").add_class("global_headline").add_class("defect_place_layup_headline")
        layer_box = widgets.VBox([
            layup_headline,
            self.fig.canvas
        ]).add_class("defect_place_layer_render_container")

        input_set_selection_label = widgets.Label(value="Select Input Set:").add_class("global_headline")
        self.input_set_selection = widgets.Select(
            options=['Load data first'],
            value='Load data first',
            rows=10,
            description='Input sets:',
            disabled=False
        ).add_class("global_input_set_selection").add_class("global_basic_input")
        display_input_set_button = widgets.Button(
            description='Load Input Set',
            disabled=False,
            tooltip='Load Input Set',
            icon='play',
        ).add_class("global_basic_button")
        display_input_set_button.on_click(self.display_input_set)
        input_set_box = widgets.VBox([
            input_set_selection_label,
            self.input_set_selection,
            display_input_set_button
        ])
        placed_defects_list_label = widgets.Label(value="Defects in cell:").add_class("global_headline")
        self.defect_in_cell_selection = widgets.Select(
            options=['Select cell first'],
            value='Select cell first',
            rows=10,
            description='',
            disabled=False
        ).add_class("global_input_set_selection").add_class("global_basic_input")
        defects_in_cell_box = widgets.VBox([
            placed_defects_list_label,
            self.defect_in_cell_selection
        ])
        save_input_set_button = widgets.Button(
            description='Save configured defects as new input set',
            disabled=False,
            tooltip='Save configured defects as new input set',
            icon='save',
        ).add_class("global_save_input_set_button").add_class("global_basic_button")
        save_input_set_button.on_click(self.save_new_input_set)
        
        simulation_display_box = widgets.HBox([
            self.init_pyvista_render(),
            layer_box
        ]).add_class("defect_place_simulation_row_container")
        simulation_input_box = widgets.HBox([
            input_set_box,
            cell_select_info_box,
            defects_in_cell_box,
            defect_definition_box
        ]).add_class("defect_place_simulation_row_container")
        simulation_box = widgets.VBox([
            simulation_display_box,
            simulation_input_box,
            save_input_set_button
        ]) 
        return simulation_box
    
    def cell_selected(self, cell: pv.UnstructuredGrid):
        """Update material layup render and layer display with new information.
        
        Args:
            cell: Single PyVista cell returned by callback
        """
        self.reset_multi_select()
        self.selected_cells["center"] = cell
        cell_id = cell['original_id'][0]
        available_layers = self.layer_data.get_available_layers_for_cell(cell_id)
        self.layer_dropdown_widget.options = [("Layer {}: {}".format(x[self.LAYER_ID_KEY], x[self.LAYER_MATERIAL_KEY]), x[self.LAYER_ID_KEY]) 
                                              for i, x in available_layers.iterrows()]

        self.update_defects_in_cell_display(cell_id)
        self.update_layers_in_cell_display(cell_id)
        self.render_cell_layer_structure(available_layers)

    def update_defects_in_cell_display(self, cell_id: int):
        """Update display of already placed faults in currently selected cell."""
        defects_in_cell = self.placed_defects.get_defects_of_cell(cell_id)
        if defects_in_cell:
            options = []
            for layer_id, defects in defects_in_cell.items():
                for defect_id, defect in defects.items():
                    options.append("Layer {}: '{}'".format(layer_id, defect.generate_display_label()))
            self.defect_in_cell_selection.options = options
        else:
            self.defect_in_cell_selection.options = ['No defects in selected cell']

    def update_layers_in_cell_display(self, cell_id: int):
        """Update display of available layers in currently selected cell."""
        first_layer = self.layer_data.get_available_layers_for_cell(cell_id).iloc[0]
        self.layer_dropdown_widget.value = first_layer[self.LAYER_ID_KEY]
        self.selected_id_widget.value = cell_id
        self.cell_layer_widget.value = first_layer[self.LAYER_ID_KEY]
        self.layer_material_widget.value = first_layer[self.LAYER_MATERIAL_KEY]
        self.layer_thickness_widget.value = first_layer[self.LAYER_THICKNESS_KEY]
        self.layer_angle_widget.value = first_layer[self.LAYER_ANGLE_KEY]
        self.layer_dropdown_widget.observe(self.layer_selected, names="value")
    
    def show_mesh(self, _, selected_meshes: list=[]):
        """Update visibility of all meshes in PyVista view.

        Args:
            selected_meshes: ids of meshes to display in case of partial render
        """
        if len(selected_meshes) == 0:
            selected_meshes = self.MESH_DATA.keys()
        for id, data in self.MESH_DATA.items():
            current_visibility = data["mesh"].GetVisibility()
            if current_visibility == False and id in selected_meshes:
                data["mesh"].SetVisibility(True)
            elif current_visibility == True and id not in selected_meshes:
                data["mesh"].SetVisibility(False)
        self.plotter.update()

    def create_display_mesh_data(self) -> dict:
        """Read 3D mesh data from file and analyse for display in UI.
        
        Returns:
            dictionary of mesh data divided into different shells according to SHELL_DATA_KEY
        """
        initial_mesh = pv.read(self.MESH_FILE_PATH).cast_to_unstructured_grid()
        available_shells = np.unique(initial_mesh[self.SHELL_DATA_KEY])
        # IDs in layer file start at 1
        initial_mesh['original_id'] = [x+1 for x in range(len(initial_mesh[self.SECTION_DATA_KEY]))]
        display_mesh_data = {}
        for shell in available_shells:
            cell_to_remove = np.argwhere(initial_mesh[self.SHELL_DATA_KEY] != shell)
            extracted_shell = initial_mesh.remove_cells(cell_to_remove)
            extracted_scalars = extracted_shell[self.SECTION_DATA_KEY]
            extracted_shell['subshell_id'] = [x for x in range(len(extracted_scalars))]
            extracted_shell['color'] = [0 for x in range(len(extracted_scalars))]
            extracted_shell['section'] = extracted_shell[self.SECTION_DATA_KEY]
            if shell == 0:
                display_name = "Outer Shell"
            elif shell == 1:
                display_name = "Webs"
            else:
                display_name = "Layer '{}'".format(shell)
            display_mesh_data[shell] = {
                "input": extracted_shell,
                "name": display_name,
                "mesh": None,
                "scalar_arrays": {
                    "section": extracted_scalars,
                    "state": [0 for x in range(len(extracted_scalars))],
                    "selection": [0 for x in range(len(extracted_scalars))]
                },
                "value_ranges": {
                    "section": [0, 68],
                    "state": [0, self.SELECTION_COLOR_ID],
                    "selection": [0, self.SELECTION_COLOR_ID]
                }
            }
        return display_mesh_data

    def layer_selected(self, change: Bunch):
        """Change displayed layer information on layer dropdown change."""
        selected_layer_data = self.layer_data.get_layer_of_cell(self.selected_cells["center"]['original_id'][0], change['new'])
        self.cell_layer_widget.value = selected_layer_data[self.LAYER_ID_KEY]
        self.layer_material_widget.value = selected_layer_data[self.LAYER_MATERIAL_KEY]
        self.layer_thickness_widget.value = selected_layer_data[self.LAYER_THICKNESS_KEY]
        self.layer_angle_widget.value = selected_layer_data[self.LAYER_ANGLE_KEY]

    def init_cell_layer_rendering(self) -> plt.Figure:
        """Initialize 3D matplot for rendering material layup and fill with placeholder."""
        with plt.ioff():
            fig = plt.figure()
            fig.set_figwidth(4)
            self.ax = fig.add_subplot(111, projection='3d', computed_zorder=False)
        fig.set_facecolor((0.0, 0.0, 0.0, 0.0))
        self.ax.set_facecolor((0.0, 0.0, 0.0, 0.0))
        self.ax.set_axis_off()
        self.ax.set_title("")
        fig.canvas.toolbar_visible = False
        fig.canvas.header_visible = False
        fig.canvas.footer_visible = False
        fig.canvas.resizable = False
        self.ax.bar3d(1, 1, 1, 1, 1, 1, label="Select cell first",color="white", shade=True)
        handles, labels = self.ax.get_legend_handles_labels()
        self.ax.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, 1.05), prop={'size': 12})
        self.ax.disable_mouse_rotation()
        return fig

    def render_cell_layer_structure(self, layers: pd.DataFrame):
        """Update rendered material layupt 3D matplot.
        
        Args:
            layers: Sorted layers to display with structure according to CellLayersDataSet
        """
        self.ax.clear()
        self.ax.set_axis_off()
        self.ax.set_title("Cell {}".format(self.selected_id_widget.value), y=-0.01)
        total_width = 0
        total_height = 0
        legend_colors = []
        graph_colors = []
        current_label = None
        previous_label = None
        graph_labels = []
        layer_counter = 1
        layer_counts = []
        layer_angles = []
        group_width = 0.5
        alignments = [[]]

        # count layer and material composition
        for i, (_, layer) in enumerate(layers.iterrows()):
            color = configuration.get_material_color(layer[self.LAYER_MATERIAL_KEY])
            current_label = layer[self.LAYER_MATERIAL_KEY]
            if current_label != previous_label:
                graph_labels.append(current_label)
                legend_colors.append(color)
                if i > 0:
                    layer_counts.append(layer_counter)
                    layer_counter = 1
            else:
                graph_labels.append("_")
                layer_counter += 1
            graph_colors.append(color)
            previous_label = layer[self.LAYER_MATERIAL_KEY]
            total_height += layer[self.LAYER_THICKNESS_KEY]
        layer_counts.append(layer_counter)
        for layer in layer_counts:
            total_width += group_width

        # add data to graph
        current_height = total_height + layers.iloc[0][self.LAYER_THICKNESS_KEY]
        current_width = 0
        current_layer_index = 0
        for i, (_, layer) in enumerate(layers.iterrows()):
            current_label = layer[self.LAYER_MATERIAL_KEY]
            if current_label != previous_label and i > 0:
                current_layer_index += 1
                alignments.append([])
            current_height -= layer[self.LAYER_THICKNESS_KEY]
            if layer_counts[current_layer_index] == 1:
                width_delta = group_width * 0.66
            else:
                width_delta = group_width / layer_counts[current_layer_index]
            current_width += width_delta
            p = self.ax.bar3d(1, 1, current_height, current_width, 3, layer[self.LAYER_THICKNESS_KEY], 
                              label=graph_labels[i],color=graph_colors[i],zorder=current_height, shade=True)
            previous_label = layer[self.LAYER_MATERIAL_KEY]
            alignments[current_layer_index].append(layer[self.LAYER_ANGLE_KEY])

        # adjust text and colors of legend
        handles, labels = self.ax.get_legend_handles_labels()
        for i, layer_count in enumerate(layer_counts):
            if layer_count > 1:
                labels[i] = "{} - {}".format(labels[i], composite_layup.generate_composite_layup_description(alignments[i]))
        self.ax.disable_mouse_rotation()
        self.ax.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, 1.30), prop={'size': 10})
        for i, color in enumerate(legend_colors):
            self.ax.get_legend().legend_handles[i].set_color(color)

        plt.show()
        # manually trigger redraw event - update cycles take several seconds to show up on frontend otherwise
        self.fig.canvas.draw()

    def create_scalar_color_definitions(self) -> dict:
        """Create custom color maps for defect and section displays in PyVista 3D render.
        
        Returns:
            dict with str key and matplotlib Colormap as value
        """
        color_maps = {}
        color_maps["section"] = "prism"
        
        default = np.array([189 / 256, 189 / 256, 189 / 256, 1.0])
        multiple_defects = np.array([1.0, 0.0, 0.0, 1.0])
        selection = np.array([255 / 256, 3 / 256, 213 / 256, 1.0])
        intmapping = np.linspace(0, self.SELECTION_COLOR_ID, 256)
        intcolors = np.empty((256, 4))
        intcolors[intmapping <= (self.SELECTION_COLOR_ID + 0.1)] = selection
        intcolors[intmapping <= (self.MULTIPLE_DEFECTS_COLOR_ID + 0.1)] = multiple_defects
        for defect_id, defect in reversed(self.available_defects.items()):
            defect_color = list(defect.display_color)
            defect_color.append(1.0)
            intcolors[intmapping <= (defect_id + 0.1)] = defect_color
        intcolors[intmapping <= 0.1] = default
        color_maps["state"] = pltcolor.ListedColormap(intcolors)

        return color_maps

    def change_scalar_display_mode(self, _, scalar_name: str=None):
        """Change displayed scalar for colors on 3D mesh."""
        self.reset_multi_select()
        for shell_id, data in self.MESH_DATA.items():
            scalars = data["scalar_arrays"][scalar_name]
            value_range = data["value_ranges"][scalar_name]
            data["input"].cell_data.set_scalars(scalars, "color")
            data["mesh"].mapper.lookup_table.cmap = self.COLOR_DEFINITIONS[scalar_name]
            data["mesh"].mapper.scalar_range = value_range[0], value_range[1]
        self.active_scalar_display = scalar_name
        self.plotter.update()

    def place_defect(self, ui_element=None):
        """Place new defect according to currently entered information in UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        cells = self.selected_cells["all"] if self.selected_cells["all"] else self.selected_cells["center"]
        subshell_ids = cells['subshell_id']
        vtk_ids = cells['original_id']
        params = self.get_new_defect_configuration()
        for i in range(len(subshell_ids)):
            new_defect = self.new_defect_to_place(vtk_ids[i], self.layer_dropdown_widget.value, **params)
            self.placed_defects.add_defect_to_cell(vtk_ids[i], self.layer_dropdown_widget.value, new_defect)
        self.update_defects_in_cell_display(self.selected_cells["center"]['original_id'][0])
        self.update_displayed_cell_state(cells)
        self.reset_multi_select()
        
    def remove_defects(self, ui_element=None):
        """Remove all defects from currently selected cell.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        cells = self.selected_cells["all"] if self.selected_cells["all"] else self.selected_cells["center"]
        subshell_ids = cells['subshell_id']
        vtk_ids = cells['original_id']
        for i in range(len(subshell_ids)):
            self.placed_defects.clear_defects_of_cell(vtk_ids[i])
        self.update_defects_in_cell_display(self.selected_cells["center"]['original_id'][0])
        self.update_displayed_cell_state(cells)
        self.reset_multi_select()
        
    def update_displayed_cell_state(self, cells: pv.UnstructuredGrid):
        """Update colors of selected cells in 3D mesh according to current defect state.
        
        Args:
            cells to refresh defect display information for
        """
        shell_id = cells[self.SHELL_DATA_KEY][0]
        data = self.MESH_DATA[shell_id]
        subshell_ids = cells['subshell_id']
        vtk_ids = cells['original_id']
        defect_display_data = self.placed_defects.generate_defect_display_summary()
        for i, vtk_id in enumerate(vtk_ids):
            if vtk_id not in defect_display_data["cell_defect_types"]:
                display_value = 0
            else:
                cell_defects = defect_display_data["cell_defect_types"][vtk_id]
                if len(cell_defects) > 1:
                    display_value = self.MULTIPLE_DEFECTS_COLOR_ID
                else:
                    display_value = cell_defects[0]
            data["scalar_arrays"]["state"][subshell_ids[i]] = display_value

    def grow_selection(self, ui_element=None):
        """Grow current selection of cells in all directions by 1 cell while respecting section borders.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        grow_counter = self.selected_cells["grow_counter"] + 1
        self.perform_multiselect(grow_counter)

    def shrink_selection(self, ui_element=None):
        """Shrink current selection of cells in all directions by 1 cell.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        grow_counter = self.selected_cells["grow_counter"] - 1
        self.perform_multiselect(grow_counter)
        
    def perform_multiselect(self, grow_counter: int):
        """Update selection area according to current multi select counter."""
        self.reset_multi_select(refresh=False)
        self.selected_cells["grow_counter"] = grow_counter
        cell = self.selected_cells["center"]
        shell_id = cell['WebShellAssignment'][0]
        search_id = cell['original_id'][0]
        cell_subshell_id = cell['subshell_id'][0]
        section_id = cell['section'][0]
        mesh_data = self.MESH_DATA[shell_id]["input"]
        mesh_data.cell_data.set_scalars(self.MESH_DATA[shell_id]["scalar_arrays"]["state"], "color")
        
        cells_found = [cell_subshell_id]
        for _ in range(grow_counter):
            new_cells = []
            for cell_id in cells_found:
                result = mesh_data.cell_neighbors(cell_id, "points")
                extracted_cells = mesh_data.extract_cells(result)
                section_ids = extracted_cells['section']
                subshell_ids = extracted_cells['subshell_id']
                for i, item in enumerate(section_ids):
                    if item == section_id:
                        new_cells.append(subshell_ids[i])
            cells_found.extend(new_cells)
            cells_found = list(set(cells_found))
    
        grow_cells = mesh_data.extract_cells(cells_found)
        self.selected_cells["all"] = grow_cells
        for i in grow_cells['subshell_id']:
            self.MESH_DATA[shell_id]["scalar_arrays"]["selection"][i] = self.SELECTION_COLOR_ID
        mesh_data.cell_data.set_scalars(self.MESH_DATA[shell_id]["scalar_arrays"]["selection"], "color")

        self.plotter.update()

    def reset_multi_select(self, refresh: bool=True):
        """Reset internal storage of currently selected cells and display state of 3D mesh back to baseline.
        
        Args:
            refresh: Trigger PyVista re-render or not
        """
        self.selected_cells["all"] = None
        self.selected_cells["grow_counter"] = 0
        for key, data in self.MESH_DATA.items():
            data["scalar_arrays"]["selection"] = copy.deepcopy(data["scalar_arrays"]["state"])
            if self.active_scalar_display == 'state':
                data["input"].cell_data.set_scalars(data["scalar_arrays"]["state"], "color")
        if refresh:
            self.plotter.update()

    def on_defect_type_changed(self, ui_element=None):
        """Update displayed UI elements for defect configuration after selected defect type has changed.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        selected_defect_type_id = self.defect_id_widget.value
        self.new_defect_to_place = self.available_defects[selected_defect_type_id]
        match selected_defect_type_id:
            case 1: # PlyWaviness
                self.defect_param_widget = widgets.FloatText(
                    value=0,
                    description="Waviness (?):"
                ).add_class("defect_param_input").add_class("global_basic_input")
            case 2: # PlyMisorientation
                self.defect_param_widget = widgets.FloatText(
                    value=0,
                    description="Offset Angle (°):"
                ).add_class("defect_param_input").add_class("global_basic_input")
            case 3: # MissingPly
                self.defect_param_widget = None
            case 4: # VaryingThickness
                self.defect_param_widget = widgets.FloatText(
                    value=0,
                    description="Variation (?):"
                ).add_class("defect_param_input").add_class("global_basic_input")
            case _:
                print("Unknown type of defect selected with ID {}".format(self.defect_id_widget.value))
        self.defect_param_output.clear_output()
        if self.defect_param_widget:
            with self.defect_param_output:
                display(self.defect_param_widget)

    def get_new_defect_configuration(self) -> dict:
        """Return all parameters of defect currently being defined.
        
        Returns:
            dictionary of all parameters currently entered in UI for defect configuration (can be empty)
        """
        parameters = {}
        if self.defect_param_widget:
            parameters["param"] = self.defect_param_widget.value
        return parameters

    def load_local_data(self, ui_element=None):
        """Use locally stored data for UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.conn = store_connection.LocalStoreConnection("sensotwin_world")
        self.placed_defects = configuration.DefectInputDataSet(conn=self.conn)
        self.load_input_set_data()

    def load_remote_data(self, ui_element=None):
        """Use remotely stored data for UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.conn = store_connection.FusekiConnection(self.remote_database_input.value)
        self.placed_defects = configuration.DefectInputDataSet(conn=self.conn)
        self.load_input_set_data()

    def save_new_input_set(self, ui_element=None):
        new_id = max(self.input_set_data.keys()) + 1

        self.placed_defects.save_entry_to_store(new_id)
        self.load_input_set_data()

    def load_input_set_data(self):
        """Load data from previously set input source and populate UI."""
        self.input_set_data = configuration.DefectInputDataSet.get_all_entries_from_store(self.conn)
        options = []
        for key, input_set in self.input_set_data.items():
            label = input_set.generate_input_set_display_label()
            options.append((label, key))
        self.input_set_selection.options = options
        self.input_set_selection.value = list(self.input_set_data.keys())[0]

    def display_input_set(self, ui_element=None):
        """Display data for currently selected input set in UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.placed_defects = self.input_set_data[self.input_set_selection.value]
        for id, data in self.MESH_DATA.items():
            self.update_displayed_cell_state(data["input"])
        self.reset_multi_select()

    def apply_styling(self):
        """Apply CSS hack to notebook for better styling than native Jupyter Widget styles."""
        css = """
            <style>
            {}
            {}
            </style>
        """.format(global_style.global_css, style.local_css)
        return widgets.HTML(css)
        
        