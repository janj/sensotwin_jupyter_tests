import vtk
from vtk.util import numpy_support
import numpy as np
import logging
logging.captureWarnings(True)
import pandas as pd
logging.captureWarnings(False)

def _create_vtktable(dataframe: pd.DataFrame) -> vtk.vtkTable:
    """Create vtkTable object from input data.

    Args:
        dataframe: pandas dataframe with table data to convert to vktTable

    Returns:
        vtk table equal to input pandas dataframe in structure and content
    """
    table = vtk.vtkTable()
    for column in list(dataframe):
        np_array = dataframe[column].to_numpy()
        if np_array.dtype == np.dtype(object):
            # string are not handled properly by numpy_to_vtk, create manually
            vtk_array = vtk.vtkStringArray()
            vtk_array.SetNumberOfComponents(1)
            vtk_array.SetNumberOfTuples(len(np_array))
            for i, value in enumerate(list(np_array)):
                vtk_array.SetValue(i, value)
        else:
            # for numbers, use numpy_to_vtk
            vtk_array = numpy_support.numpy_to_vtk(np_array, deep=True, array_type=None)
        vtk_array.SetName(column)
        table.AddColumn(vtk_array)
    return table


def _write_to_disk(filename: str, table: vtk.vtkTable):
    """Write input vtkTable to vtt file on disk.

    Args:
        filename: full or relative filepath with file name to save to
        table: vtkTable containing data to write
    """
    writer = vtk.vtkXMLTableWriter()
    writer.SetFileName(filename)
    writer.SetDataModeToAppended()
    writer.SetInputData(table)
    writer.Write()


def write_file(filename: str, dataframe: pd.DataFrame):
    """Write given pandas data to file in specified location in vtk table format.

    Args:
        filename: full or relative filepath with file name to save to
        dataframe: pandas dataframe with input table data
    """
    table = _create_vtktable(dataframe)
    writer = _write_to_disk(filename, table)


