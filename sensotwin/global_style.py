PRIMARY_COLOR = "#1a4273"
SECONDARY_COLOR = "#009fe3"
BACKGROUND_COLOR = "#f3f3f3"
PRIMARY_TEXT_COLOR = "white"

global_css = """
.global_headline {{
    color: {primary_color} !important;
}}
.global_basic_input > label {{
    background-color: {secondary_color} !important;
    color: {primary_text_color} !important;
}}
.global_basic_button {{
    background-color: {primary_color} !important;
    color: {primary_text_color} !important;
}}
.global_tab_container > .widget-tab-contents, 
.global_tab_container > div > ul > .lm-mod-current {{
    background-color: {background_color} !important;
}}
.global_tab_container > div > ul > li {{
    filter: brightness(70%) !important;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
}}
.global_tab_container > div > ul > .lm-mod-current {{
    filter: brightness(100%) !important;
}}
.global_tab_container > div > ul > .lm-mod-current:before {{
    display: none;
}}
.global_headline {{
    color: {primary_color} !important;
    font-size: 150% !important;
    font-weight: bold !important;
    margin-top: 25px !important;
}}
.global_basic_input,
.global_basic_button {{
    margin-top: 5px !important;
}}
.global_basic_input > label {{
    border-radius: 2px !important;
    text-align: center !important;
    display: block !important;
    font-weight: bold !important;
}}
.global_basic_button {{
    border-radius: 2px;
    text-align: center !important;
    font-weight: bold !important;
}}
.global_save_input_set_button {{
    width: 80% !important;
    margin: 0 auto !important;
    margin-top: 40px !important;
    height: 60px !important;
    font-size: 175% !important;
}}
.global_data_tab_container {{
    display: flex !important;
    flex-direction: column !important;
    justify-content: space-around !important;
    height: 300px !important;
}}
.global_url_input {{
    width: 500px !important;
}}
.global_url_input > label {{
    width: 200px !important;
}}
.global_load_data_button {{
    width: 300px !important;
}}
.global_input_set_selection > label {{
    display: none !important;
}}
""".format(primary_color=PRIMARY_COLOR, background_color=BACKGROUND_COLOR, secondary_color=SECONDARY_COLOR, primary_text_color=PRIMARY_TEXT_COLOR)