from pyiron_base import Project
from pyiron_base import TemplateJob, PythonTemplateJob
import os
import hashlib
from sensotwin.lifetime_extrapolation.simulation import remainingLifeTimePrediction 

class StructureSimulation(PythonTemplateJob):
    def __init__(self, project, job_name):
        super().__init__(project, job_name) 

    def run_static(self):
        wind_speed = self.input.stress_strain_input.get_wind_speed()
        duration = self.input.stress_strain_input.get_duration()
        remainingLifeTimePrediction.f_calculateRemainingLifeTime(wind_speed, duration,num_cpus=6)
        self.output.remaining_lifetime = "30"
        self.output.mesh = "Inputs/output_mesh.vtu"
        self.status.finished = True
        self.to_hdf()

    def generate_ontology_label(self):
        return "__LifetimeExtrapolationSimulationProcess_{material_hardening_input}_{defect_placement_input}_{stress_strain_input}".format(
            material_hardening_input = self.input.material_hardening_input.get_id(),
            defect_placement_input = self.input.defect_placement_input.get_id(),
            stress_strain_input = self.input.stress_strain_input.get_id()
        )

    def save_simulation_to_ontology(self, conn):
        label = self.generate_ontology_label()
        query = """
            INSERT {{
                senso:{label}_Result a senso:TotalLifeTime ;
                    co:value {remaining_lifetime} .
                senso:{label} a senso:LifetimeExtrapolationSimulationProcess ;
                    co:input senso:{material_hardening_input} ;
                    co:input senso:{defect_placement_input} ;
                    co:input senso:{stress_strain_input} ;
                    co:output senso:{label}_Result .
            }} WHERE {{ }}
            """.format(label = label, 
                       material_hardening_input = self.input.material_hardening_input.generate_ontology_label(),
                       defect_placement_input = self.input.defect_placement_input.generate_ontology_label(), 
                       stress_strain_input = self.input.stress_strain_input.generate_ontology_label(), 
                       remaining_lifetime = self.output.remaining_lifetime)
        conn.update_data(query)


class CalculixJob(TemplateJob):
    def __init__(self, project, job_name):
        super().__init__(project, job_name)
        self.input.message = "Ich bin ein Input File"
        self.executable = "ccx -v > ccx_output || true"

    def write_input(self):
        with open(self.working_directory + "/input.dat", "w") as f:
            f.write(self.input.message)
            f.write(str(self.input.material_hardening_input.get_id()))
            f.write(str(self.input.defect_placement_input.get_id()))
            f.write(str(self.input.stress_strain_input.get_id()))

    def collect_output(self):
        with open(self.working_directory + "/ccx_output", "r") as f:
            self.output.message = f.read()
            self.output.ccx_version = self.output.message.strip().split()[-1]
        self.output.mesh = "Inputs/output_mesh.vtu"
        self.output.remaining_lifetime = "30"
        self.to_hdf()


def _get_hash(x):
    return hashlib.md5(str(x).encode('utf-8')).hexdigest()[:40]

def execute_job(pr, material_hardening_input, defect_placement_input, stress_strain_input, conn=None):
    #name_hash = _get_hash(str(cells) + str(defect) + str(material))
    name_hash = "{}_{}_{}".format(material_hardening_input.get_id(),defect_placement_input.get_id(), stress_strain_input.get_id())
    
    curing_job = pr.create_job(CalculixJob, job_name=("curing_job_", name_hash), delete_existing_job=True)
    curing_job.input.material_hardening_input = material_hardening_input
    curing_job.input.defect_placement_input = defect_placement_input
    curing_job.input.stress_strain_input = stress_strain_input
    curing_job.run()

    structure_job = pr.create_job(StructureSimulation, job_name=("structure_job_", name_hash), delete_existing_job=True)
    structure_job.input.material_hardening_input = material_hardening_input
    structure_job.input.defect_placement_input = defect_placement_input
    structure_job.input.stress_strain_input = stress_strain_input
    structure_job.run()
    
    if conn:
        structure_job.save_simulation_to_ontology(conn)

def get_pyiron_project(project_path):
    return Project(path=project_path)

def get_job_table(pr):
    return pr.job_table(auto_refresh_job_status=False)


def pyiron_job():
    def get_hash(x):
        return hashlib.md5(str(x).encode('utf-8')).hexdigest()[:30]
    
    ccx_input_template_1 = """
    These are ccx input cells
    {cells}
    Some static Calculix parameters
    1 2 3 4 5
    Some other dynamic Calculix parameter
    Thingamagig: {param}
    Defect: {defect}
    """
    
    ccx_input_template_2 = """
    No parameters here to be found
    
    just testing multiple files per input
    """
    
    def write_input(input_dict, working_directory="."):
        with open(os.path.join(working_directory, "ccx_input_1"), "w") as f:
            f.write(ccx_input_template_1.format(cells="\t".join(map(str,input_dict["cells"])), param=input_dict["material"], defect=input_dict["defect"]))
        with open(os.path.join(working_directory, "ccx_input_2"), "w") as f:
            f.write(ccx_input_template_2)
    
    def collect_output(working_directory="."):
        with open(os.path.join(working_directory, "ccx_output"), "r") as f:
            file_content = f.read()
            return {
                "message": file_content,
                "version": file_content.strip().split()[-1]
            }
    
    pr = Project(path="test")
    pr.create_job_class(
        class_name="CalculixVersionJob",
        write_input_funct=write_input,
        collect_output_funct=collect_output,
        default_input_dict={"cells": [], "defect": "", "material": ""},
        executable_str="ccx -v > ccx_output || true",
    )
    
    cells = [1, 12, 123, 1234, 12345, 123456]
    defect = "Fault 500"
    material = "Unobtanium"
    
    name_hash = get_hash(str(cells) + str(defect) + str(material))
    
    external_job = pr.create.job.CalculixVersionJob(job_name=("ccx_version", name_hash), delete_existing_job=True)
    external_job.input["cells"] = cells
    external_job.input["defect"] = defect
    external_job.input["material"] = material
    external_job.run()
    
    return external_job.output

def pyiron_job2():
    from pyiron_base import Project
    import numpy as np
    import matplotlib.pyplot as plt
    from pyiron_base import PythonTemplateJob
    import hashlib
    
    def get_hash(x):
        return hashlib.md5(str(x).encode('utf-8')).hexdigest()[:30]
    
        class GraphJob(PythonTemplateJob):
            def __init__(self, project, job_name):
                super().__init__(project, job_name) 
                self.input.nr_of_points = 10
                self.input.max_value = 2
        
            def run_static(self):
                self.output.x = np.arange(0, self.input.nr_of_points)
                self.output.y = np.random.rand(self.input.nr_of_points)*self.input.max_value
                self.status.finished = True
                self.to_hdf()
        
        pr = Project("test")
        
        points = 20
        max_value = 15
        
        name_hash = get_hash(str(points) + str(max_value))
        
        python_job = pr.create_job(job_type=GraphJob, job_name=("Graph", name_hash))
        python_job.input.nr_of_points = points
        python_job.input.max_value = max_value
        python_job.run()
        
        output = python_job.output
        
        fig, ax = plt.subplots()
        ax.plot(output["x"], output["y"])
        
        ax.set(xlabel='time (s)', ylabel='something ()',
               title='Display data from ".wrap_python_function"')
        
        plt.show()
