

local_css = """
.life_extra_input_row_container {
    width: 100% !important;
    display: flex !important;
    flex-direction: row !important;
    align-items: stretch !important;
    justify-content: space-around !important;
}
.life_extra_input_column_container {
    width: 100% !important;
    display: flex !important;
    flex-direction: column !important;
    align-items: center !important;
    justify-content: space-around !important;
}
.fault_place_layer_render_container {
    width: 100% !important;
}
.fault_place_layer_render_container > div > .jupyter-matplotlib-figure {
    margin: 0 auto !important;
}
.life_extra_output_column_container,
.life_extra_cell_data_column_container{
    height: 1100px !important;
    display: flex !important;
    align-items: start !important;
    justify-content: center !important;
    flex-grow: 1 !important;
}
.life_extra_cell_data_column_container > .widget-html-content {
    align-self: center !important;
    white-space: pre-wrap !important;
    line-height: 15px !important;
}
"""