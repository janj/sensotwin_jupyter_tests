import ipywidgets as widgets
from . import configuration
from . import style
from .. import global_style
from .. import store_connection
from ..defect_placement import configuration as defect_config
from ..stress_selection import configuration as stress_config
from ..temperature_selection import configuration as temperature_config
import pyvista as pv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as pltcolor
from IPython.display import display
import copy
from traitlets.utils.bunch import Bunch
from vtkmodules.vtkRenderingCore import vtkActor2D


def create_d_cmap():
    """Create matplotlib color map for d plot in result.
    
    Returns:
        matplotlib Colormap
    """
    base_cmap = plt.get_cmap('jet')
    damage_cmap = pltcolor.LinearSegmentedColormap.from_list(
            'trunc({n},{a:.2f},{b:.2f})'.format(n=base_cmap.name, a=0.30, b=1.0),
            base_cmap(np.linspace(0.30, 1.0, 100)))
    return damage_cmap

def create_t_cmap():
    """Create matplotlib color map for t plot in result.
    
    Returns:
        matplotlib Colormap
    """
    base_cmap = plt.get_cmap('jet')
    damage_cmap = pltcolor.LinearSegmentedColormap.from_list(
            'trunc({n},{a:.2f},{b:.2f})'.format(n=base_cmap.name, a=0.30, b=1.0),
            base_cmap(np.linspace(0.30, 1.0, 100)))
    return "jet"


class LifetimeExtrapolationUIElements:
    """Contains all UI functionality for executing simulation and viewing results (Step 4)."""
    LAYER_ID_KEY = "LAYID"
    LAYER_THICKNESS_KEY = 'THICKMM'

    STRUCTURE_DISPLAY_DEFINITIONS = {
            "DFAT": {
                "clim": [0.000000001, 1],
                "cmap": create_d_cmap(),
                "log_scale": True
            },
            "TFAT": {
                "clim": [0.001, 100],
                "cmap": create_t_cmap(),
                "log_scale": True
            },
            "THICKNESS": {
                "cmap": "terrain",
                "log_scale": False
            }
        }

    structure_max_actor = None

    def __init__(self):
        """Initialize UI objects and associated UI functionality."""
        self.init_input_set_selection_UI()
        self.init_job_overview_UI()
        self.init_result_display_UI()

    def init_input_set_selection_UI(self):
        """Initialize first UI of notebook, choosing data source and selecting input data sets."""
        temperature_input_selection_label = widgets.Label(value="Material Hardening Input:").add_class("global_headline")
        self.temperature_input_selection = widgets.Select(
            options=['Load data first'],
            value='Load data first',
            rows=10,
            description='Input sets:',
            disabled=False
        ).add_class("global_input_set_selection").add_class("global_basic_input")
        defect_input_selection_label = widgets.Label(value="Defect Placement Input Set:").add_class("global_headline")
        self.defect_input_selection = widgets.Select(
            options=['Load data first'],
            value='Load data first',
            rows=10,
            description='Input sets:',
            disabled=False
        ).add_class("global_input_set_selection").add_class("global_basic_input")
        stress_input_selection_label = widgets.Label(value="Stress/Strain Input Set:").add_class("global_headline")
        self.stress_input_selection = widgets.Select(
            options=['Load data first'],
            value='Load data first',
            rows=10,
            description='Input sets:',
            disabled=False
        ).add_class("global_input_set_selection").add_class("global_basic_input")
        start_simulation_button = widgets.Button(
            description='Start simulation with selected input data sets',
            disabled=False,
            tooltip='Start simulation with selected input data sets',
            icon='play',
        ).add_class("global_save_input_set_button").add_class("global_basic_button")
        start_simulation_button.on_click(self.start_simulation)

        simulation_temperature_selection = widgets.VBox([
            temperature_input_selection_label,
            self.temperature_input_selection
        ]).add_class("life_extra_input_column_container")
        simulation_defect_placement = widgets.VBox([
            defect_input_selection_label,
            self.defect_input_selection
        ]).add_class("life_extra_input_column_container")
        simulation_stress_selection = widgets.VBox([
            stress_input_selection_label,
            self.stress_input_selection
        ]).add_class("life_extra_input_column_container")
        simulation_box = widgets.VBox([
            widgets.HBox([
                simulation_temperature_selection,
                simulation_defect_placement,
                simulation_stress_selection
            ]).add_class("life_extra_input_row_container"),
            start_simulation_button
        ])

        local_database_label = widgets.Label(value="Use local owlready2 database:").add_class("global_headline")
        use_local_data_button = widgets.Button(
            description='Use local database',
            disabled=False,
            tooltip='Use local database',
            icon='play',
        ).add_class("global_load_data_button").add_class("global_basic_button")
        use_local_data_button.on_click(self.load_local_data)
        remote_database_label = widgets.Label(value="Use remote Apache Jena Fuseki database:").add_class("global_headline")
        self.remote_database_input = widgets.Text(
            value=None,
            placeholder="insert SPARQL url here",
            description="SPARQL Endpoint:",
            disabled=False   
        ).add_class("global_url_input").add_class("global_basic_input")
        use_remote_data_button = widgets.Button(
            description='Use remote database',
            disabled=False,
            tooltip='Use remote database',
            icon='play',
        ).add_class("global_load_data_button").add_class("global_basic_button")
        use_remote_data_button.on_click(self.load_remote_data)
        local_data_box = widgets.VBox([
            local_database_label,
            use_local_data_button
        ])
        remote_data_box = widgets.VBox([
            remote_database_label,
            self.remote_database_input,
            use_remote_data_button
        ])
        data_source_box = widgets.VBox([
            local_data_box,
            remote_data_box
        ]).add_class("global_data_tab_container")

        self.input_dashboard = widgets.Tab().add_class("global_tab_container")
        self.input_dashboard.children = [
            data_source_box,
            simulation_box
        ]
        tab_titles = ['Input', 'Simulation']
        for i in range(len(tab_titles)):
            self.input_dashboard.set_title(i, tab_titles[i])

    def start_simulation(self, ui_element=None):
        """Start simulation with the 3 currently selected input sets.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        configuration.execute_job(
            self.pyiron_project,
            self.temperature_input_set_data[self.temperature_input_selection.value],
            self.defect_input_set_data[self.defect_input_selection.value],
            self.stress_input_set_data[self.stress_input_selection.value],
            self.conn
        )

    def init_job_overview_UI(self):
        """Initialize second UI of notebook, viewing the pyiron job table."""
        self.pyiron_project = configuration.get_pyiron_project("ccx_demonstration")
        self.job_dashboard = widgets.Output(layout={'border': '1px solid black'})
        self.update_job_list_display()

    def update_job_list_display(self):
        """Reload job table from pyiron project."""
        if self.job_dashboard:
            self.job_dashboard.clear_output()
            columns_to_hide = ["projectpath","chemicalformula","parentid","masterid","hamversion"]
            with self.job_dashboard:
                display(configuration.get_job_table(self.pyiron_project).drop(columns=columns_to_hide))

    def init_result_display_UI(self):
        """Initialize third UI of notebook, displaying result of a simulation."""
        result_selection_label = widgets.Label(value="Completed simulation:").add_class("global_headline")
        self.result_selection = widgets.Select(
            options=['Load data first'],
            value='Load data first',
            rows=10,
            description='Input sets:',
            disabled=False
        ).add_class("global_input_set_selection").add_class("global_basic_input")
        display_result_button = widgets.Button(
            description='Display Result',
            disabled=False,
            tooltip='Display Result',
            icon='play',
        ).add_class("global_basic_button")
        display_result_button.on_click(self.display_result)
        result_display_selection_box = widgets.VBox([
            result_selection_label,
            self.result_selection,
            display_result_button
        ])

        structure_render_label = widgets.Label(value="Structure simulation:").add_class("global_headline")
        self.structure_deformation_toggle = widgets.ToggleButton(
            value=False,
            description='Toggle Deformation',
            tooltip='Toggle Deformation',
        ).add_class("global_basic_button")
        self.structure_deformation_toggle.observe(self.on_structure_deformation_changed, names=['value'])
        self.structure_layer_select = widgets.Dropdown(
            options=['Load Result'],
            value='Load Result',
            description='Layer:',
            disabled=False,
        ).add_class("global_basic_input")
        self.structure_layer_select.observe(self.on_structure_layer_changed, names=['value'])
        self.structure_scalar_select = widgets.Dropdown(
            options=['Select Layer'],
            value='Select Layer',
            description='Value:',
            disabled=False,
        ).add_class("global_basic_input")
        self.structure_scalar_select.observe(self.on_structure_scalar_changed, names=['value'])
        self.structure_cell_info = widgets.HTML().add_class("life_extra_cell_data_column_container")
        
        curing_render_label = widgets.Label(value="Curing simulation:").add_class("global_headline")
        self.curing_deformation_toggle = widgets.ToggleButton(
            value=False,
            description='Toggle Deformation',
            tooltip='Toggle Deformation',
        ).add_class("global_basic_button")
        self.curing_layer_select = widgets.Dropdown(
            options=['Load Result'],
            value='Load Result',
            description='Layer:',
            disabled=False,
        ).add_class("global_basic_input")
        self.curing_cell_info = widgets.HTML().add_class("life_extra_cell_data_column_container")

        result_display_controls = widgets.VBox([
            widgets.VBox([
                curing_render_label,
                self.curing_deformation_toggle,
                self.curing_layer_select
            ]).add_class("life_extra_output_column_container"), 
            widgets.VBox([
                structure_render_label,
                self.structure_deformation_toggle,
                self.structure_layer_select,
                self.structure_scalar_select
            ]).add_class("life_extra_output_column_container")
        ]).add_class("life_extra_output_column_container")

        PYVISTA_OUTPUT_RENDER_HEIGHT = 1100
        PYVISTA_OUTPUT_RENDER_WIDTH = 900
        pyvista_render_widget = widgets.Output(layout={'height': '{}px'.format(PYVISTA_OUTPUT_RENDER_HEIGHT+15), 
                                                           'width': '{}px'.format(PYVISTA_OUTPUT_RENDER_WIDTH+10)})

        result_cell_info_box = widgets.VBox([
            self.curing_cell_info,
            self.structure_cell_info
        ]).add_class("life_extra_output_column_container")
        result_display_box = widgets.HBox([
            widgets.VBox([
                pyvista_render_widget
            ]),
            result_display_controls,
            result_cell_info_box
        ])

        self.plotter = pv.Plotter(shape=(2,1))
        self.plotter.window_size = [PYVISTA_OUTPUT_RENDER_WIDTH, PYVISTA_OUTPUT_RENDER_HEIGHT]
        self.plotter.enable_element_picking(callback=self.selection_callback, show_message=False)
        with pyvista_render_widget:
            self.plotter.show(jupyter_backend='trame')

        self.result_dashboard = widgets.Tab().add_class("global_tab_container")
        self.result_dashboard.children = [
            result_display_selection_box,
            result_display_box
        ]
        tab_titles = ['Select Result', 'Result Display']
        for i in range(len(tab_titles)):
            self.result_dashboard.set_title(i, tab_titles[i])

    def update_result_list_display(self):
        """Update list of completed jobs in result display selection list."""
        if self.result_dashboard:
            options = []
            for _, row in configuration.get_job_table(self.pyiron_project).iterrows():
                if row["hamilton"] == "StructureSimulation" and row["status"] == "finished":
                    options.append((row["job"], row["id"]))
            if options:
                self.result_selection.options = options
                self.result_selection.value = options[0][1]
            
    def display_result(self, ui_element=None):
        """Load currently selected result from list of finished jobs.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        all_jobs = configuration.get_job_table(self.pyiron_project)
        result = all_jobs.loc[all_jobs.id == self.result_selection.value].iloc[0]
        result_job = self.pyiron_project.load(result["job"])
        self.init_pyvista_render(result_job.output.mesh)

    def init_pyvista_render(self, mesh_path):
        """Load result mesh from job into PyVista display.
        
        Args:
            mesh_path: Relative file path to result mesh
        """
        initial_camera_position = [(-30.765771120379302, -28.608772602676154, 39.46235706090557),
             (0.9572034500000001, 0.0005481500000000805, -30.4),
             (-0.16976192468426815, -0.8825527410211623, -0.43849919982085056)]
        base_cmap = plt.get_cmap('jet')
        d_cmap = pltcolor.LinearSegmentedColormap.from_list(
                'trunc({n},{a:.2f},{b:.2f})'.format(n=base_cmap.name, a=0.30, b=1.0),
                base_cmap(np.linspace(0.30, 1.0, 100)))

        self.structure_mesh_data = self.create_structure_mesh_data("Inputs/STARTER_SNL61p5_13LY_S8R_29052024_15_0_FATIGUE.vtu")
        
        self.set_plotter_to_curing()
        self.plotter.camera_position = initial_camera_position
        curing_mesh = pv.read("Inputs/Fatigue_Results_Rotorblade_1_07162024_110031.vtu").cast_to_unstructured_grid()
        extracted_curing_mesh = curing_mesh.remove_cells(np.argwhere(curing_mesh["LAYID_SAERTEX-U-E-SKIN"] != 1))
        self.plotter.add_mesh(extracted_curing_mesh, show_edges=False, cmap=d_cmap, clim=[0.000000001,1], scalars="DFAT_SAERTEX-U-E-SKIN", log_scale=True, 
                              lighting=False, show_scalar_bar=True,scalar_bar_args={'title': 'Damage Example'})
        
        # render will be triggered by the scalar widget change event when setting new options
        self.set_plotter_to_structure()
        self.plotter.camera_position = initial_camera_position
        self.relevant_structure_cell_fields = self.get_relevant_structure_cell_fields()
        self.update_available_structure_layers()

        self.plotter.update()

    def create_structure_mesh_data(self, mesh_path):
        mesh = self.reduce_structure_mesh(pv.read(mesh_path).cast_to_unstructured_grid())
        
        layers = np.unique(mesh[self.LAYER_ID_KEY])
        mesh_data = {}
        for layer_id in layers:
            input_mesh = mesh.remove_cells(np.argwhere(mesh[self.LAYER_ID_KEY] != layer_id))
            input_mesh['color'] = [0 for x in range(len(input_mesh[self.LAYER_THICKNESS_KEY]))]
            # TODO: remove warp correction factor
            mesh_data[layer_id] = {
                "input": input_mesh,
                "warped": input_mesh.warp_by_vector(vectors="U_AEROCENT"),
                "scalar_bar_name": None,
                "output": None
            }
        return mesh_data

    def reduce_structure_mesh(self, mesh: pv.UnstructuredGrid) -> pv.UnstructuredGrid:
        """Reduces 3D cells of structure simulation result to 2D polygons for display purposes.
        
        Args:
            mesh: PyVista mesh read directly from simulation result file

        Returns:
            3D mesh with same cell layout and cell data content as input, but reduced in dimension (20 to 4 points per cell)
        """
        full_cell_data = mesh.cell_data
        full_point_data = mesh.point_data
        cell_ids = mesh.cells
        cell_ids = np.split(cell_ids, len(cell_ids) / 21)
        
        new_points = []
        new_ids = []
        point_scalar_slice = []
        for cell_id in range(mesh.n_cells):
            cell = mesh.get_cell(cell_id)
            new_ids.append(4)
            for i, point in enumerate(cell.points[:4]):
                new_points.append(point)
                point_scalar_slice.append(cell_ids[cell_id][i+1])
                new_ids.append(len(new_points) - 1)
        
        point_scalars = full_point_data["U_AEROCENT"][point_scalar_slice]
        reduced_mesh = pv.UnstructuredGrid(new_ids, [pv.CellType.POLYGON for _ in range(mesh.n_cells)], new_points)
        for prop in full_cell_data:
            reduced_mesh[prop] = full_cell_data[prop]
        reduced_mesh.point_data["U_AEROCENT"] = point_scalars

        return reduced_mesh

    def get_relevant_structure_cell_fields(self) -> list:
        """Get all fields that should ne filter-able for structure mesh.
        
        Returns:
            list of string names of scalar fields that should be toggleable for display
        """
        relevant_cells = ["THICKMM"]
        for x in self.structure_mesh_data[list(self.structure_mesh_data.keys())[0]]["input"].cell_data:
            if x.startswith("DFAT_") or x.startswith("TFAT_"):
                relevant_cells.append(x)
        return relevant_cells

    def update_available_structure_layers(self):
        """Update available layers for display in structure dropdown.
        
        This causes an update in PyVista rendering by triggering the onchange event of 
        the layer select widget.
        """
        self.structure_layer_select.options = self.structure_mesh_data.keys()
        self.structure_layer_select.value = list(self.structure_mesh_data.keys())[0]
        #self.update_available_structure_layer_scalars(self.structure_layer_select.value)

    def update_available_structure_layer_scalars(self, layer_id: int):
        """Update scalar select with available scalars for currently displayed layer."""
        options = []
        for field in self.relevant_structure_cell_fields:
            if not np.isnan(self.structure_mesh_data[layer_id]["input"][field]).all():
                options.append(field)
        if len(options) == 0:
            options.append("No layers with valid values")
        self.structure_scalar_select.options = options
        self.structure_scalar_select.value = options[0]
        
    def on_structure_layer_changed(self, value: Bunch):
        """Event to update UI after selected structure layer to display has changed."""
        self.update_available_structure_layer_scalars(value["new"])
        self.update_displayed_structure_scalar(self.structure_scalar_select.value)

    def on_structure_scalar_changed(self, value: Bunch):
        """Event to update UI after selected structure scalar to display has changed."""
        self.update_displayed_structure_scalar(value["new"])

    def on_structure_deformation_changed(self, value: Bunch):
        """Event to update UI after value of deformation display has changed."""
        self.update_displayed_structure_scalar(self.structure_scalar_select.value)

    def update_displayed_structure_scalar(self, value: str):
        """Update PyVista structure render.

        Args:
            value: Scalar name to display on 3D mesh
        """
        self.set_plotter_to_structure()
        if self.structure_deformation_toggle.value == True:
            mesh_name = "warped"
        else:
            mesh_name = "input"
        for layer_id, structure_layer in self.structure_mesh_data.items():
            if structure_layer["output"] != None:
                self.plotter.remove_actor(structure_layer["output"])
                self.plotter.remove_scalar_bar(structure_layer["scalar_bar_name"])
                structure_layer["scalar_bar_name"] = None
                structure_layer["output"] = None
        if self.structure_max_actor:
            self.plotter.remove_actor(self.structure_max_actor)
            self.structure_max_actor = None
        self.structure_cell_info.value = ""

        data = self.structure_mesh_data[self.structure_layer_select.value]
        data[mesh_name].cell_data.set_scalars(data[mesh_name][value], "color")
        if value.startswith("DFAT_"):
            cmap = self.STRUCTURE_DISPLAY_DEFINITIONS["DFAT"]["cmap"]
            clim = self.STRUCTURE_DISPLAY_DEFINITIONS["DFAT"]["clim"]
            log_scale = self.STRUCTURE_DISPLAY_DEFINITIONS["DFAT"]["log_scale"]
            title = "DFAT Layer {}".format(self.structure_layer_select.value)
            self.structure_max_actor = self.show_max_value(data[mesh_name], value)
        elif value.startswith("TFAT"):
            cmap = self.STRUCTURE_DISPLAY_DEFINITIONS["TFAT"]["cmap"]
            clim = self.STRUCTURE_DISPLAY_DEFINITIONS["TFAT"]["clim"]
            log_scale = self.STRUCTURE_DISPLAY_DEFINITIONS["TFAT"]["log_scale"]
            title = "TFAT Layer {}".format(self.structure_layer_select.value)
            self.structure_max_actor = self.show_max_value(data[mesh_name], value)
        elif value.startswith(self.LAYER_THICKNESS_KEY):
            cmap = self.STRUCTURE_DISPLAY_DEFINITIONS["THICKNESS"]["cmap"]
            clim = np.nanmin(data[mesh_name][self.LAYER_THICKNESS_KEY]), np.max(data[mesh_name][self.LAYER_THICKNESS_KEY])
            log_scale = self.STRUCTURE_DISPLAY_DEFINITIONS["THICKNESS"]["log_scale"]
            title = "Thickness(mm) Layer {}".format(self.structure_layer_select.value)
        else:
            raise ValueError("No colorscheme for chosen scalar")

        data["output"] = self.plotter.add_mesh(data[mesh_name], show_edges=False, lighting=False, show_scalar_bar=True, scalars="color", 
                                               cmap=cmap, clim=clim, log_scale=log_scale, scalar_bar_args={'title': title})
        data["scalar_bar_name"] = title
        self.plotter.update()

    def show_max_value(self, mesh: pv.UnstructuredGrid, scalar: str) -> vtkActor2D:
        """Render point with tooltip at place of maximum value in PyVista.
        
        Args:
            mesh: 3D mesh with cells to check for values
            scalar: Name of scalar to check for values

        Returns:
            vtk actor of added max value point
        """
        max_value = np.nanmax(mesh[scalar])
        max_cell = mesh.remove_cells(np.argwhere(mesh[scalar] != max_value))
        points = max_cell.points
        max_point = [x/len(points) for x in map(sum, zip(*points))]
        return self.plotter.add_point_scalar_labels(max_cell.points[0], [max_cell[scalar]], name="max_value_point", fmt="Max: %.5f", point_size=20, shape="rounded_rect", 
                                point_color="red", render_points_as_spheres=True, fill_shape=True, shape_color="pink", text_color="red",shadow=True, tolerance=1)
    
    def selection_callback(self, cell: pv.Cell):
        """Event to display detail information for selected cell."""
        display_html = '<p class="global_headline"> Selected cell: </p>'
        for prop in cell.cell_data:
            if not np.isnan(cell[prop]):
                display_html += "<b>{property_name}</b>: {property_value} \n".format(property_name=prop, property_value=cell[prop][0])
        self.structure_cell_info.value = display_html
    
    def set_plotter_to_curing(self):
        """Set active plot to curing simulation, redirecting call to plotter."""
        self.plotter.subplot(0,0)

    def set_plotter_to_structure(self):
        """Set active plot to structure simulation, redirecting call to plotter."""
        self.plotter.subplot(1,0)

    def load_local_data(self, ui_element=None):
        """Use locally stored data for UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.conn = store_connection.LocalStoreConnection("sensotwin_world")
        self.load_input_set_data()

    def load_remote_data(self, ui_element=None):
        """Use remotely stored data for UI.

        Args:
            ui_element: Override for ipywidgets to not pass the UI element that triggered the event
        """
        self.conn = store_connection.FusekiConnection(self.remote_database_input.value)
        self.load_input_set_data()

    def load_input_set_data(self):
        """Load data from previously set input source and populate UI."""
        self.temperature_input_set_data = temperature_config.MaterialHardeningInputDataSet.get_all_entries_from_store(self.conn)
        options = []
        for key, input_set in self.temperature_input_set_data.items():
            label = input_set.generate_input_set_display_label()
            options.append((label, key))
        self.temperature_input_selection.options = options
        self.temperature_input_selection.value = list(self.temperature_input_set_data.keys())[0]

        self.defect_input_set_data = defect_config.DefectInputDataSet.get_all_entries_from_store(self.conn)
        options = []
        for key, input_set in self.defect_input_set_data.items():
            label = input_set.generate_input_set_display_label()
            options.append((label, key))
        self.defect_input_selection.options = options
        self.defect_input_selection.value = list(self.defect_input_set_data.keys())[0]

        self.stress_input_set_data = stress_config.StressStrainInputDataSet.get_all_entries_from_store(self.conn)
        options = []
        for key, input_set in self.stress_input_set_data.items():
            label = input_set.generate_input_set_display_label()
            options.append((label, key))
        self.stress_input_selection.options = options
        self.stress_input_selection.value = list(self.stress_input_set_data.keys())[0]

    def apply_styling(self):
        css = """
            <style>
            {}
            {}
            </style>
        """.format(global_style.global_css, style.local_css)
        return widgets.HTML(css)