'''
-------------------- Information --------------------
                                                     
             Chair of Carbon Composites              
           Technical University of Munich            
               Alexander Seidel, M.Sc.               
                                                      
-----------------------------------------------------
                                                     
This script is only intended for internal use at the 
     Chair of Carbon Composites at the Technical     
 University of Munich. Any distribution outside the  
    Chair of Carbon Composites is not permitted.       
                                                     
-----------------------------------------------------
'''

import logging
from ccx2paraview import ccx2paraview
import os

def f_convertFRD2VTU():
    '''
    Converts all native CCX results in the .frd format
    in the current working directory to the .vtu format
    using ccx2paraview.

    Parameters:
    - None

    Returns:
    - None
    '''
    logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')

    # get all frd files in the current directory
    current_dir = os.getcwd()
    all_files = os.listdir(current_dir)
    all_frd_files = []

    print('Working Directory: ' + current_dir)

    for file in all_files:
        if file.endswith('.frd'):
            all_frd_files.append(file)

    # convert file with ccx2paraview
    for file in all_frd_files:
        c = ccx2paraview.Converter(file, ['vtu'])
        c.run()

def main():
    f_convertFRD2VTU()

if __name__ == '__main__':
    main()
