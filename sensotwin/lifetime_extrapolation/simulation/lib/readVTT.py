'''
-------------------- Information --------------------
                                                     
             Chair of Carbon Composites              
           Technical University of Munich            
               Alexander Seidel, M.Sc.               
                                                      
-----------------------------------------------------
                                                     
This script is only intended for internal use at the 
     Chair of Carbon Composites at the Technical     
 University of Munich. Any distribution outside the  
    Chair of Carbon Composites is not permitted.       
                                                     
-----------------------------------------------------
'''

import vtk
from vtk.util import numpy_support as vtknp
import numpy as np
import pandas as pd

def f_readVTTFile(filename):
    '''
    Create a reader to read in a .vtt file.

    Parameters:
    - filename(string):
        Name of the file to read in.
    
    Returns:
    - reader (vtkXMLTableReader):
        Reader handle.
    '''
    reader = vtk.vtkXMLTableReader()
    reader.SetFileName(filename)
    reader.Update()
    
    return reader

def f_convertVtkTableToPandasDF(vtk_table):
    '''
    Converts a .vtt table to a pandas dataframe.
    
    Parameters:
    - vtk_table (vtkTable):
        Table to convert to a pandas dataframe.

    Returns:
    - dataframe (DataFrame):
        Pandas dataframe.
    '''
    n_columns = vtk_table.GetRowData().GetNumberOfArrays()
    n_rows = vtk_table.GetColumn(0).GetSize()
    column_names = [vtk_table.GetColumnName(x) for x in range(n_columns)]
    column_data = []
    for i in range(n_columns):
        current_column = vtk_table.GetColumn(i)
        if isinstance(current_column, vtk.vtkStringArray):
            column_data.append([vtk_table.GetColumn(i).GetValue(x) for x in range(n_rows)])
        elif isinstance(current_column, vtk.vtkDoubleArray) or isinstance(current_column, vtk.vtkIntArray):
            column_data.append(vtknp.vtk_to_numpy(vtk_table.GetColumn(i)))
    column_data = list(map(list, zip(*column_data))) # transpose
    dataframe = pd.DataFrame(column_data)
    dataframe.columns = column_names
    
    return dataframe

def f_readVTT2DF(filename):
    '''
    Reads in a .vtt file and returns a pandas dataframe.

    - filename(string):
        Name of the file to read in.
    
    Returns:
    - dataframe (DataFrame):
        Pandas dataframe.
    '''
    reader = f_readVTTFile(filename)
    table = vtk.vtkTable()
    table.DeepCopy(reader.GetOutput())
    dataframe = f_convertVtkTableToPandasDF(table)

    return dataframe

def main():
    # ---------- user input ---------- #

    filename = 'adjacent_elements_db.vtt'

    # -------------------------------- #

    reader = f_readVTTFile(filename)
    table = vtk.vtkTable()
    table.DeepCopy(reader.GetOutput())
    dataframe = f_convertVtkTableToPandasDF(table)
    print(dataframe)
    
if __name__ == '__main__':
    main()