'''
-------------------- Information --------------------
                                                     
             Chair of Carbon Composites              
           Technical University of Munich            
               Alexander Seidel, M.Sc.               
                                                      
-----------------------------------------------------
                                                     
This script is only intended for internal use at the 
     Chair of Carbon Composites at the Technical     
 University of Munich. Any distribution outside the  
    Chair of Carbon Composites is not permitted.       
                                                     
-----------------------------------------------------
'''

import pandas as pd
import numpy as np
import os
from . import readVTT
import vtk
from vtk.util import numpy_support as vtknp
import sys

def f_calculateDamageAndFatigueLife(wind_speed,simulation_time):
    '''
    Calculates the damage and the fatigue life of every element
    based on constant uniform wind, shifted linear Goodman diagrams
    and Miner's sum.
    
    This follows the calculation method by Germanischer Lloyd for 
    fibre-reinforced polymers under fatigue loads, p. 5-22 f.:
        Germanischer Lloyd: Rules and Regulations, IV-Non-marine
        Technology, Part 1-Wind Energy In: Guidelines for the 
        Certification of Wind Turbines. Guidelines for the
        Certification of Wind Turbines, 2010.

    Parameters:
    - wind_speed (float):
        Average wind speed acting on the entire rotor disc in m/s.
    - simulation_time (float):
        Total time the rotor blade is exposed to wind_speed in years.
    
    Returns:
    - None
    '''

    # read in material data from table
    current_dir = os.getcwd()
    material_data_df = readVTT.f_readVTT2DF(os.path.join(current_dir, 'INPUT', 'material_fatigue_data.vtt'))

    # Compute gamma_Ma and gamma_Mb
    material_data_df["gamma_Ma"] = material_data_df.gamma_M0*material_data_df.C1a*material_data_df.C2a*material_data_df.C3a*material_data_df.C4a;
    material_data_df["gamma_Mb"] = material_data_df.gamma_M0*material_data_df.C2b*material_data_df.C3b*material_data_df.C4b*material_data_df.C5b;

    # define the number of azimuthal positions 
    AzimuthalPositions = 4; 
    StepSize = 360/AzimuthalPositions;

    # import RPM Data
    rpm_df = readVTT.f_readVTT2DF(os.path.join(current_dir,'INPUT', 'rpm_windspeed_0p01.vtt'))

    vwind_values = rpm_df["windspeed_mpers"]
    rpm_values = rpm_df["rpm"]

    rpm_at_wind_speed_interp = np.interp(wind_speed,vwind_values,rpm_values)
    total_rotations = int(round((simulation_time*365*24*60)*rpm_at_wind_speed_interp,0));

    # import stress data

    # for aerodynamics and centrifugal forces
    file_path = '../01_StaticAnalysis/OUTPUT/AerodynamicsCentrifugal/'
    all_files = os.listdir(file_path)
    all_processed_vtu_files = [x for x in all_files if x.endswith('_processed.vtu')]
    filename = all_processed_vtu_files[0]

    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(os.path.join(file_path,filename))
    reader.Update()
    input_data_aerocent = reader.GetOutput()

    # aerocent_stresses = [x[0] for x in vtknp.vtk_to_numpy(input_data_aerocent.GetCellData().GetArray('S'))] # get first component of stress vector (S11)

    # for gravity forces
    all_files = os.listdir('../01_StaticAnalysis/OUTPUT/')
    all_azimuth_folders = [x for x in all_files if x.startswith('Gravity')]

    input_data_gravity = []
    gravity_stresses = []

    for i,folder in enumerate(all_azimuth_folders):
        file_path = '../01_StaticAnalysis/OUTPUT/' + folder
        all_files = os.listdir(file_path)
        all_processed_vtu_files = [x for x in all_files if x.endswith('_processed.vtu')]
        filename = all_processed_vtu_files[0]

        reader = vtk.vtkXMLUnstructuredGridReader()
        reader.SetFileName(os.path.join(file_path,filename))
        reader.Update()
        input_data_gravity.append(reader.GetOutput())

        gravity_stresses.append([x[0] for x in vtknp.vtk_to_numpy(input_data_gravity[i].GetCellData().GetArray('S'))]) # get first component of stress vector (S11)

    # get LAYIDS for element assignment
    material_names = list(material_data_df["Material"])
    layer_ids = []

    for material in material_names:
        current_array = input_data_aerocent.GetCellData().GetArray('LAYID_' + material)
        if current_array is None:
            print('WARNING: Material ' + material + ' not found in results. Ignoring it.')
        else:
            layer_ids.append([material, vtknp.vtk_to_numpy(current_array)])

    # compute fatigue for each material
    miner_sum_materials = []
    fatigue_life_materials = []

    for i,material in enumerate(material_data_df["Material"]):
        m = float(material_data_df.iloc[i]['m'])
        UTS = float(material_data_df.iloc[i]['UTS'])
        UCS = float(material_data_df.iloc[i]['UCS'])
        gamma_Ma = float(material_data_df.iloc[i]['gamma_Ma'])
        gamma_Mb = float(material_data_df.iloc[i]['gamma_Mb'])
        # print('m: ' + str(m) + ', UTS: ' + str(UTS)  + ', UCS: ' + str(UCS)  + ', gamma_Ma: ' + str(gamma_Ma)  + ', gamma_Mb: ' + str(gamma_Mb))

        # get stresses for that material
        # find all stresses where the field LAYIDS_<current_material> is not NaN
        current_aerocent_stresses = []
        for j,x in enumerate(vtknp.vtk_to_numpy(input_data_aerocent.GetCellData().GetArray('S'))):
            if not np.isnan(layer_ids[i][1][j]):
                current_aerocent_stresses.append(x[0])
            else:
                current_aerocent_stresses.append(np.nan)
        
        current_total_stresses = []

        # for every azimuth angle
        for input_data in input_data_gravity:
            current_gravity_stresses_azimuth = []
            for j,x in enumerate(vtknp.vtk_to_numpy(input_data.GetCellData().GetArray('S'))):
                if not np.isnan(layer_ids[i][1][j]):
                    current_gravity_stresses_azimuth.append(x[0])
                else:
                    current_gravity_stresses_azimuth.append(np.nan)

            current_total_stresses_azimuth = []

            for j in range(len(current_gravity_stresses_azimuth)): # for every element
                # compute total stress as superposition of aerodynamical, centrifugal and gravity stresses for every azimuth angle
                current_total_stresses_azimuth.append(current_aerocent_stresses[j] + current_gravity_stresses_azimuth[j])
            
            current_total_stresses.append(current_total_stresses_azimuth) # list of lists [[value_per_element, value_per_element, ...], <-- azimuth_angle_1
                                                                          #                [value_per_element, value_per_element, ...], ...] <-- azimuth_angle_2

        current_miner_sum = []
        current_fatigue_life = []

        for j in range(len(current_total_stresses[0])): # for every element
            sigma_max = max([x[j] for x in current_total_stresses]);
            sigma_min = min([x[j] for x in current_total_stresses]);
            sigma_mean = (sigma_max+sigma_min)/2;
            sigma_amplitude = sigma_max-sigma_mean;

            # compute allowable number and Miner's sum
            N_allowable = np.power(((UTS + abs(UCS) - abs(2*gamma_Ma*sigma_mean - UTS + abs(UCS)))/(2*gamma_Mb*sigma_amplitude)),m)
            miner_sum = total_rotations/N_allowable
            
            current_miner_sum.append(miner_sum)
            current_fatigue_life.append(simulation_time/miner_sum)

        miner_sum_materials.append(current_miner_sum)
        fatigue_life_materials.append(current_fatigue_life)

    # write results
    file_path = '../01_StaticAnalysis/OUTPUT/AerodynamicsCentrifugal/'
    all_files = os.listdir(file_path)
    all_processed_vtu_files = [x for x in all_files if x.endswith('_processed.vtu')]
    filename = all_processed_vtu_files[0]

    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(os.path.join(file_path,filename))
    reader.Update()
    input_data = reader.GetOutput()

    # delete S, E, U and ERROR cell data array
    dataset = input_data.GetCellData()
    dataset.RemoveArray('S')
    dataset.RemoveArray('S_Mises')
    dataset.RemoveArray('S_Principal')
    dataset.RemoveArray('E')
    dataset.RemoveArray('E_Mises')
    dataset.RemoveArray('E_Principal')
    dataset.RemoveArray('U')
    dataset.RemoveArray('ERROR')
    dataset.Modified()

    # rename U point data to U_AEROCENT as it only shows the deformation due to aerodynamical and centrifugal forces
    input_data.GetPointData().GetArray('U').SetName('U_AEROCENT')
    dataset.Modified()

    # create new array with damage and fatigue life for each material

    for i,material in enumerate(material_data_df["Material"]):
        array = vtknp.numpy_to_vtk(miner_sum_materials[i])
        array.SetName('DFAT_' + material)
        dataset = input_data.GetCellData()
        dataset.AddArray(array)
        dataset.Modified()

        array = vtknp.numpy_to_vtk(fatigue_life_materials[i])
        array.SetName('TFAT_' + material)
        dataset = input_data.GetCellData()
        dataset.AddArray(array)
        dataset.Modified()

    # write to new .vtu file
    current_dir = os.getcwd()
    if not os.path.exists(os.path.join(current_dir, 'OUTPUT')):
        os.mkdir(os.path.join(current_dir, 'OUTPUT'))

    writer = vtk.vtkXMLUnstructuredGridWriter()
    writer.SetFileName(os.path.join(current_dir, 'OUTPUT', filename.strip('_processed.vtu') + '_FATIGUE.vtu'))
    writer.CompressorType = 'NONE'
    writer.SetInputData(input_data)
    writer.Write()

def main():
    wind_speed = 4
    simulation_time = 20
    f_calculateDamageAndFatigueLife(wind_speed, simulation_time)

if __name__ == '__main__':
    main()
