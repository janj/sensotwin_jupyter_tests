'''
-------------------- Information --------------------
                                                     
             Chair of Carbon Composites              
           Technical University of Munich            
               Alexander Seidel, M.Sc.               
                                                      
-----------------------------------------------------
                                                     
This script is only intended for internal use at the 
     Chair of Carbon Composites at the Technical     
 University of Munich. Any distribution outside the  
    Chair of Carbon Composites is not permitted.       
                                                     
-----------------------------------------------------
'''

import os
import numpy as np
from . import readVTT

def f_setupStaticGravitySimulations(template_name, wind_speed, save_additional_fields = False):
    '''
    Sets up a static CCX simulation with the influence
    of gravity forces for the given azimuthal positions
    of the rotor blade.

    Parameters:
    - template_name (string):
        Name of the template file in .inp format to adapt for
        the static simulation. The file specifies the geometry
        of the rotor blade. Element and Node sets have to be
        predefined. Boundary conditions, steps and outputs
        have to be predefined.
    - wind_speed (float):
        Average wind speed acting on the entire rotor disc in m/s.
    - save_additional_fields (bool):
        Flag to save S values for the gravity based simulations in
        the .dat file. This slows down the conversion from the .frd
        to .vtu file.

    Returns:
    - None
    '''
    
    # define azimuthal positions
    numberPositions = 4
    stepSize = 360/numberPositions
    azimuth_angles = np.arange(0,360,stepSize)
    setwork_dir = os.getcwd()

    # open input file to add loads
    with open(os.path.join(setwork_dir,'INPUT', template_name + '.inp')) as input_file:
        input_lines = input_file.readlines()

    nosave_input_lines = []
    line_idx = 0
    
    if not save_additional_fields:
        while line_idx < len(input_lines):
            current_line = input_lines[line_idx]
            if current_line.find('*EL PRINT, ELSET=ALL_ELMS, GLOBAL=YES') != -1:
                line_idx +=2
            else:
                nosave_input_lines.append(current_line)
                line_idx +=1
        
        line_idx = 0
        input_lines = nosave_input_lines
        nosave_input_lines = []

        while line_idx < len(input_lines):
            current_line = input_lines[line_idx]
            if current_line.find('*NODE FILE, GLOBAL=NO') != -1:
                line_idx +=2
            else:
                nosave_input_lines.append(current_line)
                line_idx +=1

    input_lines = nosave_input_lines

    for i,azimuth_angle in enumerate(azimuth_angles):

        step_block_inp = []

        # set working directory
        os.chdir(setwork_dir)
        folder_name = "Gravity-" + str(int(azimuth_angle))
        main_dir = os.path.join(os.getcwd(),"OUTPUT")
        save_dir = os.path.join(main_dir, folder_name)
        # print("Generierter Pfad:", save_dir)
        if os.path.exists(save_dir) == False:
            os.mkdir(save_dir)
        
        # change working directory to save files in desired folder
        os.chdir(save_dir)

        # import gravity 

        loads_data_df = readVTT.f_readVTT2DF('../../INPUT/gravity_loads_transformed.vtt')
        is_current_az = loads_data_df.iloc[0] == azimuth_angle
        is_current_az['WindSpeed'] = True
        is_current_az = is_current_az.values.tolist()

        loads_data_current_df = loads_data_df.iloc[1:,is_current_az]
        # print(loads_data_current_df)
        loads_data = np.array(loads_data_current_df.values.tolist())

        # loads_data = np.genfromtxt("../../INPUT/LoadsTransformed_" + str(int(azimuth_angle)) + ".txt", delimiter =",")
        windspeed = loads_data[:,0]
        windspeed_float = [float(x) for x in windspeed]
        gravity = loads_data[:,1:4]

        # find closest wind speed in gravity_loads_transformed.vtt
        vwind_diff = []
        for vwind in windspeed_float:
            vwind_diff.append(abs(wind_speed - vwind))

        windspeed_idx = np.argmin(vwind_diff)

        if i == 0:
            print('Requested wind speed: ' + str(wind_speed))
            print('Closest matching wind speed: ' + str(windspeed_float[windspeed_idx]))
            print('Relative difference: ' + str(1-(wind_speed/windspeed_float[windspeed_idx])))
        
        # # only compute if windspeed > 11.2
        
        # if windspeed_float[windspeed_idx] <= 11.2:
        #     calculate_windspeed = 11.2
        # else:
        #     calculate_windspeed = windspeed_float[windspeed_idx]

        load_gravity = 'Load-Gravity'

        # length of centrifugal vector
        norm_gravity = np.sqrt(np.power(gravity[windspeed_idx,0],2)
                            + np.power(gravity[windspeed_idx,1],2)
                            + np.power(gravity[windspeed_idx,2],2))

        step_block_inp.append('** NAME: ' + load_gravity + '   Type: Gravity' + '\n')
        step_block_inp.append('*DLOAD' + '\n')
        step_block_inp.append('ALL_ELMS, GRAV,  ' + str(norm_gravity) + ', '
                            + str((1/norm_gravity)*gravity[windspeed_idx,0]) + ', '
                            + str((1/norm_gravity)*gravity[windspeed_idx,1]) + ', '
                            + str((1/norm_gravity)*gravity[windspeed_idx,2]) + '\n')

        # create new input file
        new_input_lines = []
        for line in input_lines:
            current_line = line
            if line.find('<enter wind speed dependent loads here>') != -1:
                current_line = ''.join(step_block_inp)
            new_input_lines.append(current_line)

        # write new input file
        with open(template_name.strip('_TEMPLATE') + '_' + str(windspeed_float[windspeed_idx]).replace('.','_') + '.inp', 'w') as output_file:
            for line in new_input_lines:
                output_file.write(line)

def main():
    template_name = 'STARTER_SNL61p5_13LY_S8R_29052024_TEMPLATE' # without .inp
    wind_speed = 10
    f_setupStaticGravitySimulations(template_name, wind_speed)

if __name__ == '__main__':
    main()
