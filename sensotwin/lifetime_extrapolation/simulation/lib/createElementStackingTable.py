'''
-------------------- Information --------------------
                                                     
             Chair of Carbon Composites              
           Technical University of Munich            
               Alexander Seidel, M.Sc.               
                                                      
-----------------------------------------------------
                                                     
This script is only intended for internal use at the 
     Chair of Carbon Composites at the Technical     
 University of Munich. Any distribution outside the  
    Chair of Carbon Composites is not permitted.       
                                                     
-----------------------------------------------------
'''

import os
import pandas as pd

def f_createElementStackingTable():
    '''
    Uses the .dat file of a CCX result to create 
    a temporary element stacking table summarising 
    the mapping of previous shell elements (S8R) to
    now solid elements (C3D20R). Always operates on
    the first .dat file found in the current working
    directory.

    Parameters:
    - None

    Returns:
    - None
    '''

    # get dat file
    current_dir = os.getcwd()
    all_files = os.listdir(current_dir)
    dat_file = [x for x in all_files if x.endswith('.dat')][0]

    # find start and ending line index for stress section
    with open(dat_file, 'r') as raw_input:
        input_lines = raw_input.readlines()
        # find first and last line of interesting 'S'-block
        for i,line in enumerate(input_lines):
            if line.find('stresses') != -1:
                starter_idx = i + 2
            if line.find('strains') != -1:
                end_idx = i - 1
        end_idx = len(input_lines)

    # read in data from dat file
    stress_data = pd.read_csv(dat_file, sep="\s+", skiprows=starter_idx, nrows=end_idx-starter_idx, header=None)
    element_layer_data = stress_data.iloc[:,0:2]
    element_layer_data.columns = ['element','integration_point']

    # get number of layers per element
    layers_per_element = []
    for i in range(max(element_layer_data.element)):
        n_elements = int(element_layer_data.loc[element_layer_data['element'] == i+1].size/16) # 16 integration points per element
        layers_per_element.append(n_elements)

    element_ids = []
    for i,element in enumerate(layers_per_element):
        for j in range(element):
            element_ids.append(i)

    with open('element_layer_assignment.out.txt','w') as output_file:
        for element in element_ids:
            output_file.write(str(element) + '\n')

def main():
    f_createElementStackingTable()

if __name__ == '__main__':
    main()
