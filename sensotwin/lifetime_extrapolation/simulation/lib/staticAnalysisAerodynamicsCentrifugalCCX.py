'''
-------------------- Information --------------------
                                                     
             Chair of Carbon Composites              
           Technical University of Munich            
               Alexander Seidel, M.Sc.               
                                                      
-----------------------------------------------------
                                                     
This script is only intended for internal use at the 
     Chair of Carbon Composites at the Technical     
 University of Munich. Any distribution outside the  
    Chair of Carbon Composites is not permitted.       
                                                     
-----------------------------------------------------
'''

import os
import sys
import numpy as np
from . import readVTT

def f_setupStaticAerodynamicsCentrifugalSimulations(template_name, wind_speed):
    '''
    Sets up a static CCX simulation with the influence
    of aerodynamic (only lift) and centrifugal forces.

    Parameters:
    - template_name (string):
        Name of the template file in .inp format to adapt for
        the static simulation. The file specifies the geometry
        of the rotor blade. Element and Node sets have to be
        predefined. Boundary conditions, steps and outputs
        have to be predefined.
    - wind_speed (float):
        Average wind speed acting on the entire rotor disc in m/s.

    Returns:
    - None
    '''

    # set working directory
    current_dir = os.getcwd()
    folder_name = "AerodynamicsCentrifugal" 
    main_dir = os.path.join(os.getcwd(),"OUTPUT")
    save_dir = os.path.join(main_dir, folder_name)
    if os.path.exists(save_dir) == False:
        os.mkdir(save_dir)

    # import aerodynamic loads
    aero_normal_df = readVTT.f_readVTT2DF('./INPUT/aero_loads_N_transformed.vtt')
    aero_normal = np.array(aero_normal_df.values.tolist())

    # # import coordinates for application of aerodynamic loads
    # coord_normal_1_df = readVTT.f_readVTT2DF('./INPUT/coords_N_web_1.vtt')
    # coord_normal_1 = np.array(coord_normal_1_df.values.tolist())

    # coord_normal_2_df = readVTT.f_readVTT2DF('./INPUT/coords_N_web_2.vtt')
    # coord_normal_2 = np.array(coord_normal_2_df.values.tolist())

    # Import centrifugal loads
    loads_data_df = readVTT.f_readVTT2DF('./INPUT/gravity_loads_transformed.vtt')
    is_current_az = loads_data_df.iloc[0] == 0
    is_current_az['WindSpeed'] = True
    is_current_az = is_current_az.values.tolist()

    loads_data_0_df = loads_data_df.iloc[1:,is_current_az]
    loads_data = np.array(loads_data_0_df.values.tolist())

    windspeed = loads_data[:,0]
    windspeed_float = [float(x) for x in windspeed]

    angular_velocity = loads_data[:,4]
    coord_point_1 = loads_data[:,5:8]
    coord_point_2 = loads_data[:,8:11]

    # Sets are already defined
                                                                                                                                                                                                        
    # create loads based on wind speed

    # open input file to add loads
    with open(os.path.join(current_dir,'INPUT', template_name + '.inp')) as input_file:
        input_lines = input_file.readlines()

    step_block_inp = []

    # Define aerodynamic loads at each blade section

    # find closest wind speed in gravity_loads_transformed.vtt
    vwind_diff = []
    for vwind in windspeed_float:
        vwind_diff.append(abs(wind_speed - vwind))

    windspeed_idx = np.argmin(vwind_diff)

    print('Requested wind speed: ' + str(wind_speed))
    print('Closest matching wind speed: ' + str(windspeed_float[windspeed_idx]))
    print('Relative difference: ' + str(1-(wind_speed/windspeed_float[windspeed_idx])))

    for i in range(len(aero_normal[:,0:])):
        # region_tangential = mdb.models['Airfoil Project'].rootAssembly.surfaces['Surface-' + str(aero_tangential[i,0]).replace(".","_")]
        # region_normal_1 = mdb.models['Airfoil Project'].rootAssembly.sets['BASE_PLANE-1.Set-N1-' + str(aero_normal[i,0]).replace(".","_")]
        # region_normal_2 = mdb.models['Airfoil Project'].rootAssembly.sets['BASE_PLANE-1.Set-N2-' + str(aero_normal[i,0]).replace(".","_")]
        
        # load_tangential = 'Load-T-' + str(aero_tangential[i,0]).replace(".","_") 
        load_normal_1 = 'Load-N1-' + str(aero_normal[i,0]).replace(".","_")
        load_normal_2 = 'Load-N2-' + str(aero_normal[i,0]).replace(".","_")

        step_block_inp.append('** NAME: ' + load_normal_1 + '   Type: Concentrated force' + '\n')
        step_block_inp.append('*CLOAD' + '\n')
        step_block_inp.append('Set-N1-' + str(aero_normal[i,0]).replace(".","_") + ', 2, ' + str(0.5*aero_normal[i,windspeed_idx+1]) + '\n')

        step_block_inp.append('** NAME: ' + load_normal_2 + '   Type: Concentrated force' + '\n')
        step_block_inp.append('*CLOAD' + '\n')
        step_block_inp.append('Set-N2-' + str(aero_normal[i,0]).replace(".","_") + ', 2, ' + str(0.5*aero_normal[i,windspeed_idx+1]) + '\n')

        # mdb.models['Airfoil Project'].SurfaceTraction(name=load_tangential, createStepName=myStep, region=region_tangential, magnitude=surface_scaling_factor[i]*aero_tangential[i,j+1], directionVector=((0.0, 0.0, 0.0), (1.0, 0.0, 0.0)), distributionType=UNIFORM, field='', localCsys=None)
        # mdb.models['Airfoil Project'].ConcentratedForce(name=load_normal_1,createStepName=myStep, region=region_normal_1, cf2=0.5*aero_normal[i,j+1],distributionType=UNIFORM, field='', localCsys=None)    
        # mdb.models['Airfoil Project'].ConcentratedForce(name=load_normal_2,createStepName=myStep, region=region_normal_2, cf2=0.5*aero_normal[i,j+1],distributionType=UNIFORM, field='', localCsys=None)

    # Define centrifugal loads
    load_centrifugal = 'Load-Centrifugal'
    step_block_inp.append('** NAME: ' + load_centrifugal + '   Type: Rotational body force' + '\n')
    step_block_inp.append('*DLOAD' + '\n')

    # length of centrifugal vector
    norm_centrif = np.sqrt(np.power(coord_point_2[windspeed_idx,0]-coord_point_1[windspeed_idx,0],2)
                        + np.power(coord_point_2[windspeed_idx,1]-coord_point_1[windspeed_idx,1],2)
                        + np.power(coord_point_2[windspeed_idx,2]-coord_point_1[windspeed_idx,2],2))

    step_block_inp.append('Set-Centrifugal, CENTRIF, ' 
                        + str(np.power(angular_velocity[windspeed_idx],2)) + ', '
                        + str(coord_point_1[windspeed_idx,0]) + ', ' 
                        + str(coord_point_1[windspeed_idx,1]) + ', ' 
                        + str(coord_point_1[windspeed_idx,2]) + ', ' 
                        + str((1/norm_centrif)*(coord_point_2[windspeed_idx,0]-coord_point_1[windspeed_idx,0])) + ', ' 
                        + str((1/norm_centrif)*(coord_point_2[windspeed_idx,1]-coord_point_1[windspeed_idx,1])) + ', ' 
                        + str((1/norm_centrif)*(coord_point_2[windspeed_idx,2]-coord_point_1[windspeed_idx,2])) + '\n')

    # Create new input file

    new_input_lines = []
    for line in input_lines:
        current_line = line
        if line.find('<enter wind speed dependent loads here>') != -1:
            current_line = ''.join(step_block_inp)
        new_input_lines.append(current_line)

    # write new input file
    with open(os.path.join(save_dir, template_name.strip('_TEMPLATE') + '_' + str(windspeed_float[windspeed_idx]).replace('.','_') + '.inp'), 'w') as output_file:
        for line in new_input_lines:
            output_file.write(line)

def main():
    template_name = 'STARTER_SNL61p5_13LY_S8R_29052024_TEMPLATE' # without .inp
    wind_speed = 10
    f_setupStaticAerodynamicsCentrifugalSimulations(template_name, wind_speed)

if __name__ == '__main__':
    main()
