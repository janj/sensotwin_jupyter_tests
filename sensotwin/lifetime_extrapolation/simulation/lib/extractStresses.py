'''
-------------------- Information --------------------
                                                     
             Chair of Carbon Composites              
           Technical University of Munich            
               Alexander Seidel, M.Sc.               
                                                      
-----------------------------------------------------
                                                     
This script is only intended for internal use at the 
     Chair of Carbon Composites at the Technical     
 University of Munich. Any distribution outside the  
    Chair of Carbon Composites is not permitted.       
                                                     
-----------------------------------------------------
'''

from . import readVTT
import numpy as np
import vtk
from vtk.util import numpy_support as vtknp
import os

def f_extractStresses():
    '''
    Converts point data results from convertFRDtoVTK.py
    to cell data results in a new .vtu file. Copies
    the U field as point data for usage of warpByVector.
    Uses element_layer_assignment.out.txt from 
    createElementStackingTable.py to add the following
    fields:
    
    - ID: cell ID
    - ELID: element IDs of shell model
    - LAYID: layer id starting from the outer-most layer
    - ANGLEDEG: angle of layer in deg
    - THICKMM: thickness of layer in mm
    - LAYID_<material>: LAYID for every material

    Always operates on the first .vtu file found in the
    current working directory.

    Parameters:
    - None

    Returns:
    - None
    '''
    current_dir = os.getcwd()
    all_files = os.listdir(current_dir)
    all_vtu_files = [x for x in all_files if x.endswith('.vtu')]
    filename = all_vtu_files[0]
    print('Evaluating ' + filename + '...')

    # read in composite_layup_db.vtt
    reader = readVTT.f_readVTTFile('../../INPUT/composite_layup_db.vtt')
    table = vtk.vtkTable()
    table.DeepCopy(reader.GetOutput())
    layup_df = readVTT.f_convertVtkTableToPandasDF(table)
    # print(layup_df)

    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(filename)
    reader.Update()

    point_data = reader.GetOutput()

    # convert point data to cell data to operate on elements
    p2c_converter = vtk.vtkPointDataToCellData()
    p2c_converter.SetInputData(point_data)
    p2c_converter.Update()
    cell_data = p2c_converter.GetOutput()

    # add U as point data for "warp-by-vector" filter
    array = point_data.GetPointData().GetArray('U')
    dataset = cell_data.GetPointData()
    dataset.AddArray(array)
    dataset.Modified()

    # create new array with cell ids
    n_cells = cell_data.GetNumberOfCells()
    cell_ids = np.array([x for x in range(n_cells)])

    array = vtknp.numpy_to_vtk(cell_ids)
    array.SetName("ID")
    dataset = cell_data.GetCellData()
    dataset.AddArray(array)
    dataset.Modified()

    # create new array with element ids and layer ids from shell model
    with open('element_layer_assignment.out.txt','r') as input_file:
        input_lines = input_file.readlines()
    element_ids = [int(x.strip('\n')) for x in input_lines]

    layer_ids = []
    for i in range(max(element_ids)+1):
        n_layers = element_ids.count(i)
        for j in range(n_layers):
            layer_ids.append(j)

    array = vtknp.numpy_to_vtk(element_ids)
    array.SetName("ELID")
    dataset = cell_data.GetCellData()
    dataset.AddArray(array)
    dataset.Modified()

    array = vtknp.numpy_to_vtk(layer_ids)
    array.SetName("LAYID")
    dataset = cell_data.GetCellData()
    dataset.AddArray(array)
    dataset.Modified()

    # create new array with orientation
    orientations = list(layup_df['layer_angle_deg'])
    array = vtknp.numpy_to_vtk(orientations)
    array.SetName("ANGLEDEG")
    dataset = cell_data.GetCellData()
    dataset.AddArray(array)
    dataset.Modified()

    # create new array with thickness
    thicknesses = list(layup_df['layer_thickness_mm'])
    array = vtknp.numpy_to_vtk(thicknesses)
    array.SetName("THICKMM")
    dataset = cell_data.GetCellData()
    dataset.AddArray(array)
    dataset.Modified()

    # create new array for material with layer id
    unique_materials = list(set(layup_df['layer_material']))
    # print(unique_materials)

    for material in unique_materials:
        array_name = 'LAYID_' + material
        element_labels_with_material = layup_df.loc[layup_df['layer_material'] == material]
        current_material_assignment = []
        for element_id in range(len(layup_df.index)):
            if element_id in element_labels_with_material.index:
                current_material_assignment.append(layer_ids[element_id])
            else:
                current_material_assignment.append(np.nan)
        
        array = vtknp.numpy_to_vtk(current_material_assignment)
        array.SetName(array_name)
        dataset = cell_data.GetCellData()
        dataset.AddArray(array)
        dataset.Modified()

    # write to new .vtu file
    writer = vtk.vtkXMLUnstructuredGridWriter()
    writer.SetFileName(filename.strip('.vtu') + '_processed.vtu')
    writer.CompressorType = 'NONE'
    writer.SetInputData(cell_data)
    writer.Write()

def main():
    f_extractStresses()

if __name__ == '__main__':
    main()
