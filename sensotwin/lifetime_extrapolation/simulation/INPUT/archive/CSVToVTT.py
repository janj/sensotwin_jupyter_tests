'''
-------------------- Information --------------------
                                                     
             Chair of Carbon Composites              
           Technical University of Munich            
               Alexander Seidel, M.Sc.               
                                                      
-----------------------------------------------------
                                                     
This script is only intended for internal use at the 
     Chair of Carbon Composites at the Technical     
 University of Munich. Any distribution outside the  
    Chair of Carbon Composites is not permitted.       
                                                     
-----------------------------------------------------
'''

import vtk

# ---------- user input ---------- #

filename = 'MaterialData.csv'

# -------------------------------- #

def f_readCSVFile(filename):
    '''
    Create a reader to read in delimited text. 
    '''
    reader = vtk.vtkDelimitedTextReader()
    reader.DetectNumericColumnsOn()
    reader.SetHaveHeaders(True)
    reader.SetFileName(filename)
    reader.Update()
    
    return reader

def f_writeTable(filename, table):
    '''
    Write VTK table to VTT file. 
    '''
    writer = vtk.vtkXMLTableWriter()
    writer.SetFileName(filename)
    writer.SetDataModeToAppended()
    writer.SetInputData(table)
    writer.Write()
    
    return writer

def main():
    reader = f_readCSVFile(filename)
    table = vtk.vtkTable()
    table.DeepCopy(reader.GetOutput())
    writer = f_writeTable(filename.replace('.csv','.vtt'), table)

if __name__ == '__main__':
    main()