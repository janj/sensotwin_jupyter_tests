import os

current_dir = os.getcwd()
all_files = os.listdir(current_dir)
all_load_files = [x for x in all_files if (x.startswith('LoadsTransformed') and x.endswith('.txt'))]

input_data = []
for file in all_load_files:
    with open(file,'r') as input_file:
        input_data.append([x.strip('\n').split(',') for x in input_file.readlines()])

azimuth_angles = [x.strip('.txt').split('_')[-1] for x in all_load_files]

with open('LoadsTransformed_combined.csv', 'w') as output_file:
    for i in range(len(input_data[0])):
        if i == 0:
            header_line = input_data[0][0][0] + ',' + ','.join([','.join([','.join([y + '_az_' + str(j) for y in x[0][1:]]) for j,x in enumerate(input_data)])])
            output_file.write(header_line + '\n')
            output_file.write('NaN,' + ','.join([azimuth_angle for azimuth_angle in azimuth_angles for i in range(len(input_data[0][0])-1)]) + '\n')
        else:
            output_file.write(input_data[0][i][0] + ',' + ','.join([','.join([','.join(y[i][1:]) for y in input_data])]) + '\n')


