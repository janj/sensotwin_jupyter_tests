import numpy as np

with open('Performance_Metric.out.txt', 'r') as input_file:
    input_lines = input_file.readlines()

sum_parts = []
for line in input_lines[:-1]:
    print(line.split('\t')[-1].strip('\t').strip(' ').strip('\n'))
    sum_part = float(line.split('\t')[-1].strip('\t').strip(' ').strip('\n'))
    sum_parts.append(sum_part)

total_sum = np.sum(sum_parts)
print(total_sum)