'''
-------------------- Information --------------------
                                                     
             Chair of Carbon Composites              
           Technical University of Munich            
               Alexander Seidel, M.Sc.               
                                                      
-----------------------------------------------------
                                                     
This script is only intended for internal use at the 
     Chair of Carbon Composites at the Technical     
 University of Munich. Any distribution outside the  
    Chair of Carbon Composites is not permitted.       
                                                     
-----------------------------------------------------

'''

import os
import shutil
import time
from datetime import datetime

from .lib import staticAnalysisAerodynamicsCentrifugalCCX
from .lib import staticAnalysisGravityCCX
from .lib import convertFRDtoVTK
from .lib import createElementStackingTable
from .lib import extractStresses
from .lib import fatigueAnalysisCCX

def f_calculateRemainingLifeTime(wind_speed, simulation_time, num_cpus=1, test_performance=False):
    '''
    Top level script to set up the simulations, run them and
    create the results in form of a .vtu file.

    Parameters:
    - wind_speed (float):
        Average wind speed acting on the entire rotor disc in m/s.
    - simulation_time (float):
        Total time the rotor blade is exposed to wind_speed in years.
    - num_cpus (int):
        Number of CPUs to run the CCX solver on. Defaults to 1.
    - test_performance (bool):
        Flag to create a performance metric file in the current working 
        directory showing how much time was spent for each step.

    Returns:
    - None
    '''
    
    # change number of processors to run CCX with
    os.environ['OMP_NUM_THREADS'] = str(num_cpus)

    if test_performance:
        performance_matrix = []
        global_start_time = time.time()

    current_dir = os.path.abspath(os.path.dirname(__file__))

    # create folder structure
    # |- INPUT
    # |- OUTPUT
    # |
    # |- lib
    #    |- 02_convertFRDtoVTK_PyModule.py
    #    |- 03_createElementStackingTable.py
    #    |- 04_extractStresses.py
    #    |- readVTT.py
    # |
    # |- main
    #    |- 01_StaticAnalysis
    #       |- INPUT
    #       |- OUTPUT
    #    |- 02_FatigueAnalysis
    #       |- INPUT
    #       |- OUTPUT

    print('Current Step: Setting up the folder structure...')

    root_directories = ['INPUT','OUTPUT','main']
    main_directories = ['01_StaticAnalysis','02_FatigueAnalysis']
    analysis_directories = ['INPUT','OUTPUT']

    for directory in root_directories:
        if not os.path.exists(os.path.join(current_dir,directory)):
            os.mkdir(os.path.join(current_dir,directory))

    for directory in main_directories:
        if not os.path.exists(os.path.join(current_dir,'main',directory)):
            os.mkdir(os.path.join(current_dir,'main',directory))

    for analysis_directory in main_directories:
        for directory in analysis_directories:
            if not os.path.exists(os.path.join(current_dir,'main',analysis_directory,directory)):
                os.mkdir(os.path.join(current_dir,'main',analysis_directory,directory))

    # copy necessary files to folder structure
    shutil.copy(os.path.join(current_dir,'INPUT','aero_loads_N_transformed.vtt'),
                os.path.join(current_dir,'main','01_StaticAnalysis','INPUT','aero_loads_N_transformed.vtt'))
    shutil.copy(os.path.join(current_dir,'INPUT','gravity_loads_transformed.vtt'),
                os.path.join(current_dir,'main','01_StaticAnalysis','INPUT','gravity_loads_transformed.vtt'))
    shutil.copy(os.path.join(current_dir,'INPUT','coords_N_web_1.vtt'),
                os.path.join(current_dir,'main','01_StaticAnalysis','INPUT','coords_N_web_1.vtt'))
    shutil.copy(os.path.join(current_dir,'INPUT','coords_N_web_2.vtt'),
                os.path.join(current_dir,'main','01_StaticAnalysis','INPUT','coords_N_web_2.vtt'))
    shutil.copy(os.path.join(current_dir,'INPUT','composite_layup_db.vtt'),
                os.path.join(current_dir,'main','01_StaticAnalysis','INPUT','composite_layup_db.vtt'))
    shutil.copy(os.path.join(current_dir,'INPUT','material_fatigue_data.vtt'),
                os.path.join(current_dir,'main','02_FatigueAnalysis','INPUT','material_fatigue_data.vtt'))
    shutil.copy(os.path.join(current_dir,'INPUT','rpm_windspeed_0p01.vtt'),
                os.path.join(current_dir,'main','02_FatigueAnalysis','INPUT','rpm_windspeed_0p01.vtt'))
    shutil.copy(os.path.join(current_dir,'INPUT','STARTER_SNL61p5_13LY_S8R_29052024_TEMPLATE.inp'),
                os.path.join(current_dir,'main','01_StaticAnalysis','INPUT','STARTER_SNL61p5_13LY_S8R_29052024_TEMPLATE.inp'))

    # set up static analysis
    static_dir = os.path.join(current_dir,'main','01_StaticAnalysis')
    os.chdir(static_dir)

    if test_performance:
        print('Current Step: Setting up the static analysis with aerodynamic and centrifugal loads...')
        start_time = time.time()
        staticAnalysisAerodynamicsCentrifugalCCX.f_setupStaticAerodynamicsCentrifugalSimulations('STARTER_SNL61p5_13LY_S8R_29052024_TEMPLATE', wind_speed)
        time_delta = time.time() - start_time
        performance_matrix.append(['Setting up static analysis aerodynamic, centrifugal', time_delta])
        
        start_time = time.time()
        print('Current Step: Setting up the static analysis with gravity loads...')
        staticAnalysisGravityCCX.f_setupStaticGravitySimulations('STARTER_SNL61p5_13LY_S8R_29052024_TEMPLATE', wind_speed)
        time_delta = time.time() - start_time
        performance_matrix.append(['Setting up static analysis gravity', time_delta])
    else:
        staticAnalysisAerodynamicsCentrifugalCCX.f_setupStaticAerodynamicsCentrifugalSimulations('STARTER_SNL61p5_13LY_S8R_29052024_TEMPLATE', wind_speed)
        staticAnalysisGravityCCX.f_setupStaticGravitySimulations('STARTER_SNL61p5_13LY_S8R_29052024_TEMPLATE', wind_speed)

    # run static simulations
    all_files = os.listdir(os.path.join(static_dir,'OUTPUT'))
    all_dirs = [x for x in all_files if os.path.isdir(os.path.join(static_dir,'OUTPUT',x))]

    all_dirs.sort()

    for i,direc in enumerate(all_dirs):
        current_working_dir = os.path.join(static_dir,'OUTPUT',direc)
        print('---------- ' + current_working_dir + ' ----------')

        os.chdir(current_working_dir)
        all_current_files = os.listdir(current_working_dir)
        all_current_inp_files = [x for x in all_current_files if x.endswith('.inp')]
        for file in all_current_inp_files:
            if test_performance:
                start_time = time.time()
                if i == 0:
                    print('Current Step: Running the static analysis with aerodynamic and centrifugal loads...')
                else:
                    print('Current Step: Running the static analysis with gravity loads for position = ' + str(i*90) + '°...')
                os.system('ccx ' + file.strip('.inp'))
                time_delta = time.time() - start_time
                performance_matrix.append(['Running simulation for ' + file.strip('.inp'), time_delta])

                start_time = time.time()
                print('Current Step: Converting to .vtu...')
                convertFRDtoVTK.f_convertFRD2VTU()
                time_delta = time.time() - start_time
                performance_matrix.append(['Converting to .vtu for ' + file.strip('.inp'), time_delta])

                if i == 0: # create mapping just once
                    start_time = time.time()
                    print('Current Step: Generating element stacking table...')
                    createElementStackingTable.f_createElementStackingTable()
                    time_delta = time.time() - start_time
                    performance_matrix.append(['Creating element stacking table for ' + file.strip('.inp'), time_delta])
                else:
                    shutil.copy(os.path.join(static_dir,'OUTPUT',all_dirs[0],'element_layer_assignment.out.txt'),
                                os.path.join(current_working_dir,'element_layer_assignment.out.txt')) 

                start_time = time.time()
                print('Current Step: Extracting stresses...')
                extractStresses.f_extractStresses()
                time_delta = time.time() - start_time
                performance_matrix.append(['Extracting stresses for ' + file.strip('.inp'), time_delta])
            else:
                if i == 0:
                    print('Current Step: Running the static analysis with aerodynamic and centrifugal loads...')
                else:
                    print('Current Step: Running the static analysis with gravity loads for position = ' + str(i*90) + '°...')
                os.system('ccx ' + file.strip('.inp'))
                print('Current Step: Converting to .vtu...')
                convertFRDtoVTK.f_convertFRD2VTU()
                if i == 0:
                    print('Current Step: Generating element stacking table...')
                    createElementStackingTable.f_createElementStackingTable()
                else:
                    shutil.copy(os.path.join(static_dir,'OUTPUT',all_dirs[0],'element_layer_assignment.out.txt'),
                                os.path.join(current_working_dir,'element_layer_assignment.out.txt')) 
                print('Current Step: Extracting stresses...')
                extractStresses.f_extractStresses()

    # run fatigue calculation
    fatigue_dir = os.path.join(current_dir,'main','02_FatigueAnalysis')
    os.chdir(fatigue_dir)

    print('Current Step: Calculating fatigue results...')
    if test_performance:
        start_time = time.time()
        fatigueAnalysisCCX.f_calculateDamageAndFatigueLife(wind_speed,simulation_time)
        time_delta = time.time() - start_time
        performance_matrix.append(['Calculating fatigue', time_delta])
    else:
        fatigueAnalysisCCX.f_calculateDamageAndFatigueLife(wind_speed,simulation_time)

    os.chdir(current_dir)

    if test_performance:
        global_time_delta = time.time() - global_start_time
        performance_matrix.append(['Total Runtime', global_time_delta])

        with open('Performance_Metric.out.txt', 'w') as metric_file:
            for line in performance_matrix:
                output_line = line[0] + ':\t\t' + str(line[1])
                metric_file.write(output_line + '\n')
                print(output_line)
    
    # copy final result file to ./OUTPUT
    fatigue_output_dir = os.path.join(current_dir,'main','02_FatigueAnalysis','OUTPUT')
    all_files = os.listdir(fatigue_output_dir)
    all_result_files = [x for x in all_files if x.endswith('.vtu')]
    result_file = all_result_files[0]
    output_file = 'Fatigue_Results_Rotorblade_1_' + datetime.now().strftime('%m%d%Y_%H%M%S') + '.vtu'
    shutil.copy(os.path.join(fatigue_output_dir,result_file),os.path.join(current_dir,'OUTPUT',output_file))

def main():
    # ------------- user input -------------- #

    test_performance = True    # write performance metrics
    num_cpus = 4               # number of cpus to use for FE simulations

    wind_speed = 15             # wind speed in m/s
    simulation_time = 20       # simulation time in years

    # ---------- end of user input ---------- #

    f_calculateRemainingLifeTime(wind_speed,simulation_time,num_cpus,test_performance)

if __name__ == '__main__':
    main()
