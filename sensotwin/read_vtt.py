import vtk
from vtk.util import numpy_support as vtknp
import numpy as np
import logging
logging.captureWarnings(True)
import pandas as pd
logging.captureWarnings(False)

def _get_file_data(file_path: str) -> vtk.vtkTable:
    """Read input vtt file into vtkTable object.

    Args:
        vtkTable: table data to convert to dataframe

    Returns:
        pandas dataframe equal to input vtk table in structure
    """
    reader = vtk.vtkXMLTableReader()
    reader.SetFileName(file_path)
    reader.Update()
    table = vtk.vtkTable()
    table.DeepCopy(reader.GetOutput())
    return table


def _vtk_to_pandas(vtkTable: vtk.vtkTable) -> pd.DataFrame:
    """Convert vtk table to pandas dataframe.

    Args:
        vtkTable: table data to convert to dataframe

    Returns:
        pandas dataframe equal to input vtk table in structure
    """
    n_columns = vtkTable.GetRowData().GetNumberOfArrays()
    n_rows = vtkTable.GetColumn(0).GetSize()
    column_names = [vtkTable.GetColumnName(x) for x in range(n_columns)]
    column_data = []
    for i in range(n_columns):
        current_column = vtkTable.GetColumn(i)
        if isinstance(current_column, vtk.vtkStringArray):
            column_data.append([vtkTable.GetColumn(i).GetValue(x) for x in range(n_rows)])
        elif isinstance(current_column, vtk.vtkDoubleArray) or isinstance(current_column, vtk.vtkIntArray):
            column_data.append(vtknp.vtk_to_numpy(vtkTable.GetColumn(i)))
        else:
            column_data.append([vtkTable.GetColumn(i).GetValue(x) for x in range(n_rows)])
    column_data = list(map(list, zip(*column_data))) # transpose
    dataframe = pd.DataFrame(column_data)
    dataframe.columns = column_names
    return dataframe


def get_dataframe(file_path: str) -> pd.DataFrame:
    """Get pandas dataframe from file at passed input path.

    Args:
        file_path: full or relative path to input .vtt file

    Returns:
        pandas dataframe with structure equal to input file
    """
    table = _get_file_data(file_path)
    dataframe = _vtk_to_pandas(table)
    return dataframe
